@extends('backend.layouts.main_admin')
@section('title', 'Login')
@section('content')
    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf
        <div class="login-box">
            <div class="login-logo">
                <a>Login<b></b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg"></p>
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback"
                              role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input id="password" type="password"
                           class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                           placeholder="Password" name="password"
                           required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback"
                              role="alert"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row ">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign in</button>
                    </div>
                </div>
                <hr>
                <a href="{{ route('register') }}">{{ __('Create new account') }}</a>
                <br>
                <a href="{{ route('forgot-password.index') }}">{{ __('Forgot Your Password?') }}</a>
            </div>
        </div>
    </form>

@endsection
