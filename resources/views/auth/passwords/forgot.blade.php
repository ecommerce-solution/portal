@extends('backend.layouts.main_admin')
@section('title', 'Forgot Password')
@section('content')
    <form action="{{ route('forgot-password.send') }}" method="post">
        @csrf
        <div class="login-box">
            <div class="login-logo">
                <a>Forgot Password</a>
            </div>
            <div class="login-box-body">
                <div class="form-group has-feedback ">
                    <input type="email" class="form-control" placeholder="Please enter you email" name="email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
                    </div>
                </div>
                <hr>
                <a href="{{ route('login') }}">Back</a><br>
            </div>
        </div>
    </form>

@endsection
