@extends('backend.layouts.main_admin')
@section('title', 'Reset Password')
@section('content')
    <form action="{{ route('reset-password.update') }}" method="post">
        @csrf
        <div class="login-box">
            <div class="login-logo">
                <a>New Password</a>
            </div>
            <div class="login-box-body">
                <div class="form-group has-feedback ">
                    <input id="password" type="password" class="form-control" placeholder="New password"
                           name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback ">
                    <input id="password-confirm" type="password" class="form-control" placeholder="Re-new password"
                           name="password_confirmation">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Confirm</button>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <input type="hidden" name="token" value=" {{ $token }}">
        <input type="hidden" name="email" value=" {{ $email }}">
    </form>

@endsection
