@if(!$ourServices->isEmpty())
    <section class="section-3">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title title-black">OUR SERVICE</div>
                    <div class="caption caption-black text-center">บริการของเรา</div>
                </div>
                <div class="service">
                    <div class="container ">
                        <div class="row">
                            @foreach($ourServices as $key =>$ourService)
                                @if($key%2==0)
                                    <div class="col-sm-6 column-xs">
                                        <div class="row">
                                            <div class="item col-sm-12">
                                                <div class="thumbnail">
                                                    <div class="item-image">
                                                        <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}">
                                                            <div class="image image-1"><img
                                                                    src="{{ asset('uploads/'.$ourService->image) }}"
                                                                    alt="image"></div>
                                                        </a>
                                                    </div>
                                                    <div class="caption">
                                                        <div class="title">
                                                            {{  $ourService->title }}
                                                        </div>
                                                        <div class="item-description">
                                                            {{ $ourService->short_description }}
                                                        </div>
                                                        <div class="item-readmore">
                                                            <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}">view
                                                                more</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-sm-6 column-xs">
                                        <div class="row">
                                            <div class="item col-sm-12">
                                                <div class="thumbnail">
                                                    <div class="item-image">
                                                        <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}">
                                                            <div class="image image-1"><img
                                                                    src="{{ asset('uploads/'.$ourService->image) }}"
                                                                    alt="image"></div>
                                                        </a>
                                                    </div>
                                                    <div class="caption">
                                                        <div class="title">
                                                            {{ $ourService->title }}
                                                        </div>
                                                        <div class="item-description">
                                                            {{ $ourService->short_description }}
                                                        </div>
                                                        <div class="item-readmore">
                                                            <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}">view
                                                                more</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
