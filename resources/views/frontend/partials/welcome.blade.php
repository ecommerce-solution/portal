<section class="section-1">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 animatedParent animateOnce">
                <div class="title title-black animated growIn">WELCOME TITLE</div>
                <div class="caption caption-black text-center animated fadeInUp">"  A welcome message on your website could easily be the make or break when it comes to triggering
                    conversions. Without conversions your site will never grow - simply put. A good thank you message is
                    not something that should be taken lightly because it is the entry point to gaining new
                    clients/customers.
                </div>
            </div>
        </div>
    </div>
    <div class="container about-us">
        <div class="row animatedParent animateOnce" data-sequence='500' data-appear-top-offset='-200'>
            <div class="col-lg-3 col-sm-6 animated bounceInUp" data-id="1">
                <div class="item text-center align-items-center">
                    <div class="item-image">
                        <img src="{{ asset('img/item1.png') }}" alt="">
                    </div>
                    <div class="item-title">
                        API SERVICE
                    </div>
                    <div class="item-caption">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                    </div>
                    <div class="item-readmore">
                        <a href="#">readmore</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 animated bounceInUp" data-id="2">
                <div class="item text-center align-items-center">
                    <div class="item-image">
                        <img src="{{ asset('img/item2.png') }}" alt="">
                    </div>
                    <div class="item-title">
                        CLOUD SERVICE
                    </div>
                    <div class="item-caption ">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                    </div>
                    <div class="item-readmore">
                        <a href="#">readmore</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 animated bounceInUp" data-id="3">
                <div class="item text-center align-items-center">
                    <div class="item-image">
                        <img src="{{ asset('img/item3.png') }}" alt="">
                    </div>
                    <div class="item-title">
                        PLATFORM
                    </div>
                    <div class="item-caption">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                    </div>
                    <div class="item-readmore">
                        <a href="#">readmore</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 animated bounceInUp" data-id="4">
                <div class="item text-center align-items-center">
                    <div class="item-image">
                        <img src="{{ asset('img/item4.png') }}" alt="">
                    </div>
                    <div class="item-title">
                        CRM
                    </div>
                    <div class="item-caption ">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                    </div>
                    <div class="item-readmore">
                        <a href="#">readmore</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>