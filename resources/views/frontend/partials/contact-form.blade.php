@if(!$contactUses->isEmpty())
    <section class="section-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                    @foreach($contactUses as $contactUs)
                        <div class="title title-white">{{ ($contactUs->title) }}</div>
                        <div class="caption caption-white text-left">
                            {!! $contactUs->short_description !!}
                        </div>
                        <div>
                            @if(!$contactUs->time==null)
                            <i class="fas fa-clock icon-color"></i> <span
                                    class="caption caption-white"> {{ $contactUs->time }} </span>
                                @else
                                @endif
                        </div>
                    @endforeach
            </div>
            <div class="col-sm-8">
                <form action="{{ route('ecommerce.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row form-message">
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" placeholder="Name" name="name">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" placeholder="Email" name="email">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="text"  id="phone_number" class="form-control" placeholder="Phone" name="tel">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" placeholder="Company" name="company">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="comment">Message:</label>
                            <textarea class="form-control" rows="4" name="message"></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-style">Send Message</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</section>
@else
@endif
@push('script')
    <script>

        new Cleave('#phone_number', {
            phone: true,
            delimiter: ' ',
            phoneRegionCode: 'TH'
        });

    </script>
@endpush
