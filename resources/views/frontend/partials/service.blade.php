<section class="section-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title title-black">OUR SERVICE</div>
                <div class="caption caption-black text-center ">"Neque porro quisquam est qui dolorem ipsum quia dolor
                    sit amet, consectetur, adipisci...."<br/> There is no one who loves pain itself, who seeks after it
                    and wants to have it, simply because it is who seeks after it and wants to have
                    it pain...
                </div>
            </div>
            <div class="service">
                <div class="container ">
                    <div class="row">
                        <div class="col-sm-6 column-xs">
                            <div class="row">
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-1"><img src="{{ asset('img/s-1.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                igetweb.com
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-2"><img src="{{ asset('img/s-2.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                Domain registration
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-3"><img src="{{ asset('img/s-3.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                E-mail server
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 column-xs">
                            <div class="row">
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-1"><img src="{{ asset('img/s-4.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                Hosting
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-2"><img src="{{ asset('img/s-5.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                Cloud
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-sm-12">
                                    <div class="thumbnail">
                                        <div class="item-image">
                                            <div class="image image-3"><img src="{{ asset('img/s-6.png') }}"
                                                                            alt="image"></div>
                                        </div>
                                        <div class="caption">
                                            <div class="title">
                                                Server
                                            </div>
                                            <div class="item-description">
                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
                                                sequi nesciunt.
                                            </div>
                                            <div class="item-readmore">
                                                <a href="">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>