@if($aboutUses !=null)
    <section class="section-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 box-bg ">
                    <div class="box-left animatedParent animateOnce" data-sequence='500' data-appear-top-offset='-300'>
                        <div class="title title-white text-left">{{ $aboutUses->title }}</div>
                        <div class="caption caption-white text-left">
                            {!! $aboutUses->short_description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
@endif
