@if(!$contactUses->isEmpty())
    <footer class="footer">
        <div class="container footer-content">
            <div class="row">
                @foreach($contactUses as $contactUs)
                    <div class="col-sm-4">
                        <div class="footer-logo"><img src="{{asset('img/logo.png') }}"/></div>
                        <div class="link-social">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="https://www.facebook.com/pages/E-commerce-Solution-CoLtd/162925647095320?ref=br_tf" class="item item-fb"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="http://line.me/ti/p/%40igetweb.com" class="item item-line"><img src="{{ asset('img/icon-line.png') }}"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="list-address">
                            <div class="list-item">
                                @if(!$contactUs->address==null)
                                    <div class="list-icon"><i class="fas fa-map-marker-alt"></i></div>
                                    <div class="list-content"> {{ $contactUs->address }} </div>
                                @else
                                    <div class="list-icon"><i class="fas fa-map-marker-alt"></i></div>
                                    <div class="list-content"> 6/49 Kamphaeng Phet 6 Road, Lat Yao, Chatuchuk District,
                                        Bankok 10900
                                    </div>
                                @endif
                            </div>
                            <div class="list-item">
                                @if(!$contactUs->email==null)
                                    <div class="list-icon"><i class="fas fa-envelope"></i></div>
                                    <div class="list-content"><a>{{ $contactUs->email }}</a></div>
                                @else
                                    <div class="list-icon"><i class="fas fa-envelope"></i></div>
                                    <div class="list-content"><a> ecommerce-solution@email.com</a></div>
                                @endif
                            </div>
                            <div class="list-item">
                                @if(!$contactUs->tel==null)
                                    <div class="list-icon"><i class="fas fa-phone"></i></div>
                                    <div class="list-content"><a>{{ $contactUs->tel }}</a></div>
                                @else
                                    <div class="list-icon"><i class="fas fa-envelope"></i></div>
                                    <div class="list-content"><a> 02-832-3222</a></div>
                                @endif
                            </div>
                            <div class="list-item">
                                @if(!$contactUs->fax==null)
                                    <div class="list-icon"><i class="fas fa-fax"></i></div>
                                    <div class="list-content"><a>{{ $contactUs->fax }}</a></div>
                                @else
                                    <div class="list-icon"><i class="fas fa-fax"></i></div>
                                    <div class="list-content"><a> 02-832-3223</a></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="google-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3874.0284350517563!2d100.55172811520623!3d13.837331099032966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ced6f45578d%3A0x20e9db097422aa36!2sE-Commerce+Solution+Co%2C.+Ltd.!5e0!3m2!1sth!2sth!4v1531373869374"
                                    width="100%" height="170" frameborder="0" style="border-radius: 10px;"
                                    allowfullscreen></iframe>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container footer-copyright">
            © Copyright E-commerce Solution Co., Ltd. All rights reserved.
        </div>
        <!-- /.container -->
    </footer>
@else
@endif
