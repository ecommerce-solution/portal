<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        @if(!$banners->isEmpty())
            <ol class="carousel-indicators">
                @foreach($banners as $index => $banner)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $index }}"
                        class="{{ ($index === 0 ? "active" : "" ) }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach($banners as $index => $banner)
                    <div class="{{ $index === 0  ? "carousel-item active" : "carousel-item" }}"
                         style="background-image:url({{ asset('uploads/'. $banner->image) }})">
                        <div class="carousel-caption h-100 align-items-center justify-content-center only-lg">
                            <h1>{{ $banner -> title }}</h1>
                            <p class="d-block">{!! $banner->short_description !!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        @else
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <!-- Slide One - Set the background image for this slide in the line below -->
                    <div class="carousel-item active"
                         style="background-image: url({{ asset('img/cover-default.jpg') }})">
                        <div class="carousel-caption h-100 align-items-center justify-content-center only-lg">
                            <h1>"BEST SOLUTION |ONLINE SOLUTION"</h1>
                            <p class="d-block">@ E-COMMERCE SOLUTION</p>
                        </div>
                        <div class="carousel-caption only-sm">
                            <h1>BEST SOLUTION</h1>
                            <h1>ONLINE SOLUTION</h1>
                            <p class="d-block">@ E-COMMERCE SOLUTION</p>
                        </div>
                    </div>
                    <div class="carousel-item" style="background-image: url({{ asset('img/cover-default.jpg') }})">
                        <div class="carousel-caption h-100 align-items-center justify-content-center only-lg">
                            <h1>"BEST SOLUTION |ONLINE SOLUTION"</h1>
                            <p class="d-block">Slide 2</p>
                        </div>
                        <div class="carousel-caption only-sm">
                            <h1>BEST SOLUTION</h1>
                            <h1>ONLINE SOLUTION</h1>
                            <p class="d-block">SLide 2</p>
                        </div>
                    </div>
                    <div class="carousel-item" style="background-image: url({{ asset('img/cover-default.jpg') }})">
                        <div class="carousel-caption h-100 align-items-center justify-content-center only-lg">
                            <h1>"BEST SOLUTION |ONLINE SOLUTION"</h1>
                            <p class="d-block">Slide 3</p>
                        </div>
                        <div class="carousel-caption only-sm">
                            <h1>BEST SOLUTION</h1>
                            <h1>ONLINE SOLUTION</h1>
                            <p class="d-block">Slide 3</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        @endif
    </div>
</header>
