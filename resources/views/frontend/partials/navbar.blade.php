<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="{{ route('ecommerce.list') }}"><img src="{{ asset('img/logo.png') }}"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('ecommerce.list') }}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ecommerce.about-us') }}">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ecommerce.article') }}">Article</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" id="navbar-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Our Businesses</a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-dropdown">
                        <a class="dropdown-item" href="{{ route('ecommerce.our-business.index') }}">Our Business</a>
                        <a class="dropdown-item" href="{{ route('ecommerce.our-service.index') }}">Our Service</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ecommerce.career.index') }}">Career</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ecommerce.holiday') }}">Holiday</a>
                </li>
                <li class="nav-item language">
                    <a class="nav-link" href=""><img src="{{ asset('img/flag-th.png') }}">
                        <label>ไทย</label></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
