{{--<section class="breadscrumb">--}}
{{--<div class="container">--}}
{{--<nav aria-label="breadcrumb">--}}
{{--<ol class="breadcrumb">--}}
{{--<li class="breadcrumb-item"><a href="#">Home</a></li>--}}
{{--<li class="breadcrumb-item active" aria-current="page">Library</li>--}}
{{--</ol>--}}
{{--</nav>--}}
{{--</div>--}}
{{--</section>--}}

@if(request()->session()->has('_old_input.company'))
    @php $x=count(request()->session()->get('_old_input.company')); @endphp
@else
    @php $x=1; @endphp
@endif

<section class="section-applyform">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="title title-black">Application Form</div>
                <div class="caption caption-black text-center">Fill the form below accurately indicating your potentials
                    and suitability to job applying for.
                </div>
            </div>
            <div class="col-sm-10 ">
                <form class="form-apply" action="{{ route('ecommerce.save') }}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="title-content">
                        Personal Information
                    </div>
                    <div class="form-row">
                        <div class="col-sm-6">
                            <label>First name</label>
                            <input type="text" class="form-control" placeholder="" id="first-name-input"
                                   name="firstname" value="{{ old("firstname") }}">
                        </div>
                        <div class="col-sm-6">
                            <label>Last name</label>
                            <input type="text" class="form-control" placeholder="" id="last-name-input" name="lastname"
                                   value="{{ old("lastname") }}">
                        </div>
                        <div class="col-sm-4">
                            <label>Birthdate</label>
                            <div class="input-group">
                                <input type="text" class="form-control " id="date-input" readonly
                                       placeholder="วว/ดด/ปปปป" value="{{ old("date") }}"
                                       name="date">
                                <div class="input-group-append">
                                    <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>Contact Number</label>
                            <input type="tel" class="form-control" placeholder="" id="tel-input" name="phone"
                                   value="{{ old("phone") }}">
                        </div>
                        <div class="col-sm-4">
                            <label>Email</label>
                            <input type="text" class="form-control" placeholder="" id="email-input"
                                   name="email" value="{{ old("email") }}">
                        </div>
                    </div>

                    <div class="title-content">
                        Work Experience
                    </div>
                    <div class="checkbox-inline ">
                        <label>No Experience :</label>
                        <input type="checkbox" style="height: 20px !important;width: 40px;" placeholder="" id="trigger"
                               name="checkbox"
                               @if(request()->session()->hasOldInput('checkbox')) checked @endif >
                    </div>
                    <div class="hidden_fields">
                        <div class="form-row after-add-more">
                            <div class="col-sm-12">
                                <hr>
                                <label>Company</label>
                                <input type="text" class="form-control" placeholder=""
                                       value="{{ old('company.0') }}" name="company[]">
                            </div>
                            <div class="col-sm-12">
                                <label>Job Title</label>
                                <input type="text" class="form-control" placeholder="" id="job-title-input"
                                       name="job_title[]" value="{{ old("job_title.0") }}">
                            </div>
                            <div class="col-sm-6">
                                <label>Date Start</label>
                                <div class="input-group">
                                    <input type="text" class="form-control date-start" readonly
                                           placeholder="วว/ดด/ปปปป" name="start_date[]" id="date-start0"
                                           value="{{ old("start_date.0") }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Date End</label>
                                <div class="input-group">
                                    <input type="text" class="form-control date-end" readonly id="date-end0"
                                           placeholder="วว/ดด/ปปปป" name="end_date[]" value="{{ old("end_date.0") }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label>Responsibilities</label>
                                <textarea class="form-control" rows="3" id="responsibilities"
                                          name="responsibilities[]">{{ old("responsibilities.0") }}</textarea>
                            </div>
                            <div class="col-sm-12 form-row">
                                <button style="background:red" type="button" class="btn btn-add remove"><i
                                            class="fas fa-minus-circle"></i> Remove
                                </button>
                            </div>
                        </div>

                        @if(request()->session()->has('_old_input'))
                            @for ($i = 1; $i < $x; $i++)
                                <div class="form-row after-add-more ">
                                    <div class="col-sm-12">
                                        <hr>
                                        <label>Company</label>
                                        <input type="text" class="form-control" placeholder="" id="company{{ $i }}"
                                               name="company[]" value="{{ old("company.$i") }}">
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Job Title</label>
                                        <input type="text" class="form-control" placeholder="" id="jobtitle{{ $i }}"
                                               name="job_title[]" value="{{ old("job_title.$i") }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Date Start</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control date-start" id="ate-start{{ $i }}"
                                                   readonly="" placeholder="วว/ดด/ปปปป" name="start_date[]"
                                                   value="{{ old("start_date.$i") }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"> <i
                                                            class="far fa-calendar-alt"></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Date End</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control date-end " readonly=""
                                                   placeholder="วว/ดด/ปปปป" name="end_date[]" id="date-end{{ $i }}"
                                                   value="{{ old("end_date.$i") }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"> <i
                                                            class="far fa-calendar-alt"></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Responsibilities</label>
                                        <textarea class="form-control" rows="3" id="certifications{{ $i }}"
                                                  name="responsibilities[]">{{ old("responsibilities.$i") }}</textarea>
                                    </div>
                                    <div class="col-sm-12 form-row">
                                        <button style="background:red" type="button" class="btn btn-add remove"><i
                                                    class="fas fa-minus-circle"></i> Remove
                                        </button>
                                    </div>
                                </div>

                            @endfor
                        @endif

                        <div class=" form-row ">
                            <div class="col-sm-12 ">
                                <button class="btn btn-add add-more"><i class="fas fa-plus-circle"></i> Add more
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="title-content">
                        Highest Education
                    </div>
                    <div class="form-row">
                        <div class="col-sm-12">
                            <label>University/School</label>
                            <input type="text" class="form-control" name="university" placeholder=""
                                   id=university-name" value="{{ old("university") }}">
                        </div>
                        <div class="col-sm-4">
                            <label>Degree</label>
                            <input type="text" class="form-control" name="degree" placeholder=""
                                   id="degree" value="{{ old("degree") }}">
                        </div>
                        <div class="col-sm-4">
                            <label>Major/Subject</label>
                            <input type="text" class="form-control" name="major" placeholder=""
                                   id="major" value="{{ old("major") }}">
                        </div>
                        <div class="col-sm-4">
                            <label>GPA</label>
                            <input type="text" class="form-control" name="gpa" placeholder=""
                                   id="gpa" value="{{ old("gpa") }}">
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-sm-6">
                            <label>Position Applying For</label>
                            <select class="form-control" name="position" id="position">
                                <option selected disabled value="">
                                    {{--style="{{ (session()->hasOldInput('position')) ? 'display: none' : '' }}">--}}
                                    Choose position...
                                </option>
                                @foreach($careers as  $career)
                                    <option value="{{ $career->id }}"
                                            {{ (old("position") == $career->id) ? 'selected' :
                                               (request()->get('career') == $career->role_name) ? 'selected' : ''}}>
                                        {{ $career->role_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Expected Salary</label>
                            <input type="text" value="{{ old("salaryShow") }}" id="salaryShow" class="form-control"
                                   name="salaryShow">
                            <input type="hidden" value="{{ old("salary") }}" id="salary" name="salary">
                        </div>
                        <div class="col-sm-6">
                            <label>Resume Attachment
                                <small>(Only PDF Format)</small>
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" accept="application/pdf" id="attach"
                                       name="attach">
                                <label class="custom-file-label">Choose file...</label>
                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-apply" type="submit"><i class="fas fa-paper-plane"></i> Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>

@push('script')

    <script language="JavaScript" type="text/javascript">

        $(document).ready(function () {

            /* Without prefix */
            var input = document.getElementById('salaryShow');
            input.addEventListener('keyup', function (e) {
                input.value = format_number(this.value);
                $('#salary').val(input.value.replace(/,/g, ''));
            });

            /* Function */
            function format_number(number, prefix, thousand_separator, decimal_separator) {
                var thousand_separator = thousand_separator || ',',
                    decimal_separator = decimal_separator,
                    regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
                    number_string = number.replace(regex, '').toString(),
                    split = number_string.split(decimal_separator),
                    rest = split[0].length % 3,
                    result = split[0].substr(0, rest),
                    thousands = split[0].substr(rest).match(/\d{3}/g);

                if (thousands) {
                    separator = rest ? thousand_separator : '';
                    result += separator + thousands.join(thousand_separator);
                }
                result = split[1] != undefined ? result + decimal_separator + split[1] : result;
                return prefix == undefined ? result : (result ? prefix + result : '');
            };


            //Start Date - End Date
            datetimepicker();
            $('.hidden_fields').on("dp.change", ".date-start", function (e) {
                $(this).closest('.col-sm-6').next().find('.date-end').val('');
                $(this).closest('.col-sm-6').next().find('.date-end').data("DateTimePicker").minDate(e.date);
                console.log($(this));
            });

            //Remove Validate
            $(".hidden_fields").on("click", ".remove", function (e) {
                $(this).parents(".after-add-more").remove();
                x--;
                if (x == 0) {
                    $('#trigger').prop('checked', 'checked');
                    hidden.hide();
                }
            });

            //Birthdate
            $('#date-input').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'glyphicon glyphicon-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times',
                },
                maxDate: 'now',
            });

            //Checkbox
            var checkbox = $("#trigger");
            var hidden = $(".hidden_fields");

            if (checkbox.is(':checked')) {
                hidden.hide();
            } else {
                hidden.show();
            }

            checkbox.change(function () {
                if (checkbox.is(':checked')) {
                    hidden.hide();
                } else {
                    if (x == 0) {
                        $(".hidden_fields").prepend("         <div class=\"form-row after-add-more\">\n" +
                            "                            <div class=\"col-sm-12\">\n" +
                            "                                <hr>\n" +
                            "                                <label>Company</label>\n" +
                            "                                <input type=\"text\" class=\"form-control\" placeholder=\"\"\n" +
                            "                                       value=\"{{ old('company.0') }}\" name=\"company[]\">\n" +
                            "                            </div>\n" +
                            "                            <div class=\"col-sm-12\">\n" +
                            "                                <label>Job Title</label>\n" +
                            "                                <input type=\"text\" class=\"form-control\" placeholder=\"\"\n" +
                            "                                       name=\"job_title[]\" value=\"{{ old("job_title.0") }}\">\n" +
                            "                            </div>\n" +
                            "                            <div class=\"col-sm-6\">\n" +
                            "                                <label>Date Start</label>\n" +
                            "                                <div class=\"input-group\">\n" +
                            "                                    <input type=\"text\" class=\"form-control date-start\" readonly\n" +
                            "                                           placeholder=\"วว/ดด/ปปปป\" name=\"start_date[]\" id=\"date-start0\"\n" +
                            "                                           value=\"{{ old("start_date.0") }}\">\n" +
                            "                                    <div class=\"input-group-append\">\n" +
                            "                                        <span class=\"input-group-text\"> <i class=\"far fa-calendar-alt\"></i> </span>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                            <div class=\"col-sm-6\">\n" +
                            "                                <label>Date End</label>\n" +
                            "                                <div class=\"input-group\">\n" +
                            "                                    <input type=\"text\" class=\"form-control date-end\" readonly\n" +
                            "                                           placeholder=\"วว/ดด/ปปปป\" name=\"end_date[]\" value=\"{{ old("end_date.0") }}\">\n" +
                            "                                    <div class=\"input-group-append\">\n" +
                            "                                        <span class=\"input-group-text\"> <i class=\"far fa-calendar-alt\"></i> </span>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                            <div class=\"col-sm-12\">\n" +
                            "                                <label>Responsibilities</label>\n" +
                            "                                <textarea class=\"form-control\" rows=\"3\" id=\"responsibilities\"\n" +
                            "                                          name=\"responsibilities[]\">{{ old("responsibilities.0") }}</textarea>\n" +
                            "                            </div>\n" +
                            "                            <div class=\"col-sm-12 form-row\">\n" +
                            "                                <button style=\"background:red\" type=\"button\" class=\"btn btn-add remove\"><i\n" +
                            "                                            class=\"fas fa-minus-circle\"></i> Remove\n" +
                            "                                </button>\n" +
                            "                            </div>\n" +
                            "                        </div>");
                        x++;
                        hidden.show();
                        datetimepicker();
                    }
                    else {
                        hidden.show();
                    }
                }


            });

            //PDF
            $('.custom-file-input').on('change', function () {
                let fileName = $(this).val().split('\\').pop();
                $(this).next('.custom-file-label').addClass("selected").html(fileName);
            });

            //ADD MORE CLONE
            var max_fields_limit = 10;
            var x = parseInt("{{ $x }}");

            $(".add-more").click(function (e) {
                e.preventDefault();
                if (x < max_fields_limit) {
                    x++;
                    var html = $(".after-add-more").first().clone();
                    html.find('.date-start').attr('id', 'date-start' + x);
                    html.find('input,textarea').val('');
                    $(".after-add-more").last().after(html);

                    datetimepicker();


                }
            });

            function datetimepicker() {
                $('.date-start').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    useCurrent: false,
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'glyphicon glyphicon-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times',
                    },
                    maxDate: 'now',
                });
                $('.date-start').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    useCurrent: false,
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'glyphicon glyphicon-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times',
                    },
                    maxDate: 'now',
                });
                $('.date-end').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    useCurrent: false,
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'glyphicon glyphicon-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times',
                    },
                    maxDate: 'now',
                });

            }

        });


    </script>

@endpush
