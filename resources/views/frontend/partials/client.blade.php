@if(!$clients->isEmpty())
    <section class="section-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title title-black">OUR CLIENTS</div>
                    <div class="caption caption-black text-center">ลูกค้าที่ใช้บริการของเรา</div>
                    <div class="item-slide">
                        @foreach($clients as $client)
                            @if($client->url != null)
                                <div class="item-image">
                                    <a href="{{ $client->url }}" target="_blank"><img
                                                src="{{ asset('uploads/'.$client->image) }}">
                                    </a>
                                </div>
                            @else
                                <div class="item-image"><img src="{{ asset('uploads/'.$client->image) }}"></div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
@endif
