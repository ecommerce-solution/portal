<section class="section-benefits">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title title-white">Benefits</div>
                <div class="row item-benefits justify-content-center">
                    <div class="col-sm-2">
                        <div class="item text-center align-items-center">
                            <div class="item-image">
                                <img src="{{ asset('img/b1.png') }}" alt="">
                            </div>
                            <div class="item-title">ประกันสังคม<br/>ประกันสุขภาพ</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="item text-center align-items-center">
                            <div class="item-image">
                                <img src="{{ asset('img/b2.png') }}" alt="">
                            </div>
                            <div class="item-title">
                                โบนัสประจำปี
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="item text-center align-items-center">
                            <div class="item-image">
                                <img src="{{ asset('img/b3.png') }}" alt="">
                            </div>
                            <div class="item-title">
                                ท่องเที่ยวประจำปี
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="item text-center align-items-center">
                            <div class="item-image">
                                <img src="{{ asset('img/b4.png') }}" alt="">
                            </div>
                            <div class="item-title">
                                มินิมาร์ท<br/> ขนม&เครื่องดื่ม
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="item text-center align-items-center">
                            <div class="item-image">
                                <img src="{{ asset('img/b5.png') }}" alt="">
                            </div>
                            <div class="item-title">
                                เล่นกีฬา<br/>แบตมินตัน ฟุตบอล
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>