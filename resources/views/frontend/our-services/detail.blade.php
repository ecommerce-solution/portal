@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.our-service.index') }}">Our Service</a></li>
            </ol>
        </nav>
    </div>
    <section class="section-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title title-post"> {{ $ourService->title }}</h1>
                    <hr>
                    <br>
                    <div class="item-image" align="center">
                        <img src="{{ asset('uploads/'.$ourService->image) }}" alt=""
                             style="width: 350px;height: 200px;
background: -webkit-gradient(linear, left top, right top, from(#6ad8fa), to(#457ed2));
  background: -webkit-linear-gradient(left, #6ad8fa 0%, #457ed2 100%);
  background: -o-linear-gradient(left, #6ad8fa 0%, #457ed2 100%);
  background: linear-gradient(to right, #6ad8fa 0%, #457ed2 100%);
  border-radius: 10px;
  position: relative;
  min-height: 178px; ">
                    </div>
                    <br><br>
                    <div class="item-post">
                        <p class="lead">
                            {{ $ourService->short_description }}
                        </p>
                        {!! $ourService->description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
