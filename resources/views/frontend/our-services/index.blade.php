@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <!-- Header Page -->
    <header class="header-page header-career" style="background-image: url('{{asset('img/page-bg.jpg')}}');">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="title-page">
                        Our Service
                    </div>
                    <div class="title-caption">บริการของเรา</div>
                </div>
                <div class="col-sm-5 position-relative b-img">
                    <div class="item-image">
                        <img src="img/w1.png" alt="" id="item-image-1">
                        <img src="img/w2.png" alt="" id="item-image-2">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Our Service</li>
            </ol>
        </nav>
    </div>

    <!-- Page Content -->
    @if(!$ourServices->isEmpty())
        <section class="section-news">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 title-group">
                        <div class="title title-black mb-4 float-left" data-id="1">
                            Service
                        </div>
                    </div>
                </div>
                <!-- Blog Post -->
                <div class="container">
                    @foreach($ourServices as $ourService)
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="item col-sm-12">
                                        <div class="thumbnail">
                                            <div class="item-image">
                                                <div class="image image-1" align="center">
                                                    <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}">
                                                        <img
                                                            src="{{ asset('uploads/'.$ourService->image) }}"
                                                            alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="caption">
                                                <h2 class="card-title">{{ $ourService->title }}</h2>
                                                <hr/>
                                                <p class="card-text">
                                                    {{ $ourService->short_description }}
                                                </p>
                                                <a href="{{ route('ecommerce.our-service.detail', [$ourService->id]) }}"
                                                   class="btn btn-readmore">
                                                    Read More</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{--<!-- Pagination -->--}}
                    <div class="col-lg-12">
                        <ul class="pagination justify-content-center">
                            {{ $ourServices->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    @endif
@endsection
