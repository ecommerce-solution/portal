@extends('frontend.layouts.main')

@section('title', 'E-Commerce Solution')

@section('content')

    <section class="section-position">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <div class="title title-black" data-id="1">Send Success</div>
                    <div class="box-footer" style="text-align: center">
                        <br>
                        <a class="btn btn-apply" href="{{ route('ecommerce.list') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
