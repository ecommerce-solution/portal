<div class="col-md-5">
    <div class="box" style="border-top-color:grey;">
        <form role="form">
            <div class="box-body">
                <h4 style=" background:lightblue; font-size: 18px; text-align: center; padding: 12px 10px; margin-top: 0;">
                    รายการสั่งซื้อ </h4>
                <div class="panel-body ">
                    <div class="col-xs-3" style="padding-left: 25px;font-size: 15px"><span>สินค้า</span></div>
                    <div class="col-xs-2" style="padding-left: 14px;font-size: 15px"><span>ราคา</span></div>
                    <div class="col-xs-4" style="padding-left: 45px;font-size: 15px"><span>จำนวน</span></div>
                    <div class="col-xs-3" style="padding-left: 20px;font-size: 15px"><span>รวมเป็นเงิน</span></div>
                </div>
                <div class="order" id="order"
                     style="margin-bottom: 5px; overflow: auto; overflow-x: hidden; width: auto; height: 658px;background-color:lightgrey">
                    <div class="panel panel-default empty-list" style="margin-bottom: 5px">
                        <div class="panel-body">
                            <div class="col-xs-12" style="text-align: center;font-size: 17px;">
                                ยังไม่มีสินค้าในตะกร้า
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="table-responsive col-sm-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="active" style="background-color: lightpink" width="50%"> จำนวนทั้งสิ้น (ชิ้น)</td>
                            <td width="50%" style="background-color: lightgreen">
                                <span class="float-right"><b><span id="total-order">0</span></b></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="active" style="background-color: lightpink">รวมเป็นเงิน (บาท)</td>
                            <td style="background-color: lightgreen"><span class="float-right"><b><span
                                            id="total-price">0.00 บาท</span></b></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class=" col-sm-12">
                    <div class="col-sm-6">
                        <button type="button" onclick="deleteOrder()" class="col-sm-6 btn btn-block btn-danger btn-lg">
                            ลบทั้งหมด
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" id="payment" class="col-sm-6 btn btn-block btn-success btn-lg"
                                data-toggle="modal" data-target="#modal-default" disabled>
                            สั่งซื้อ
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <center><h2 class="modal-title"><i class="fa fa-shopping-cart"></i>
                                                รายการสั่งซื้อ</h2></center>
                                    </div>
                                    <div class="modal-body" style="margin-bottom: 10px;">
                                        <div class="step-1">
                                            <div class="form-group">
                                                <label>เลือกชื่อผู้ทำการสั่งซื้อ</label>
                                                <select class="form-control select2" name="user" id="user"
                                                        style="width: 100%;">
                                                    <option selected value="NULL" disabled>กรุณาเลือก</option>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}"
                                                                id="{{ $user->id }}">{{ $user->nickname }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="step-2 hide-modal">
                                            <div>
                                                <div><span class="float-left" id="bill-name"></span>
                                                    <div>
                                                        <table class="table" cellspacing="0" border="0">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>สินค้า</th>
                                                                <th>จำนวน</th>
                                                                <th>รวมเป็นเงิน</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="table-tbody">
                                                            </tbody>
                                                        </table>
                                                        <table class="table" cellspacing="0" border="0"
                                                               style="margin-bottom:8px;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="text-align:left;">สินค้าทั้งหมด
                                                                </td>
                                                                <td id="table-total-quantity"
                                                                    style="text-align:right; padding-right:1.5%;">
                                                                </td>
                                                                <td style="text-align:left; padding-left:1.5%;">
                                                                    ชิ้น
                                                                </td>
                                                                <td id="table-total-price"
                                                                    style="text-align:right;font-weight:bold;">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger pull-left"
                                                data-dismiss="modal" id="close">ปิด
                                        </button>
                                        <button type="button" class="step-1 btn btn-success" id="btn-step-1" disabled>
                                            ถัดไป
                                        </button>
                                        <button type="button" class="step-2 btn btn-success hide-modal"
                                                data-dismiss="modal" id="btn-step-2">
                                            ยืนยันคำสั่งซื้อ
                                        </button>
                                        <button type="button" class="back-1 btn btn-danger pull-left hide-modal"
                                                id="back-step-1">กลับ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

