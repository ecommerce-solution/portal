<!DOCTYPE html>
<html>
<head>
    <title>POS</title>
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
    <style>


        .btn:hover, div, button {
            outline: none !important;
        }

        .float-right {
            float: right;
        }

        input[type=text]:disabled {
            background: white;
        }

        .category {
            cursor: pointer;
            text-align: center;
            margin: 5px 0;
            padding: 10px 20px;
            background-color: lightyellow;
            border-radius: 3px;
            color: #6B6B6B;
            text-transform: uppercase;
            display: inline-block;
            font-weight: 500;
            padding-bottom: 7px;
            border-bottom: 3px solid lightsteelblue;
        }

        .scroll-pos {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 20px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: lightgoldenrodyellow;
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: lightblue;
        }

        body {
            background: white;
        }

        .modal {
            text-align: center;
            padding: 0 !important;
        }

        .modal:before {
            content: '';
            display: inline-block;
            height: 60%;
            vertical-align: middle;
            margin-right: -4px;
        }

        .modal-dialog {
            width: 700px;
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }

        .noselect {
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
            /* Non-prefixed version, currently upported by Chrome and Opera */
        }

        .hide-modal {
            display: none;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            padding: 6px 0px;
            border-radius: 15px;
            text-align: center;
            font-size: 12px;
            line-height: 1.42857;
        }

        .inline-label {
            white-space: normal;
            /*white-space: nowrap;*/
            /*max-width: 100px;*/
            /*overflow: hidden;*/
            /*text-overflow: ellipsis;*/
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            line-height: 16px; /* fallback */
            -webkit-line-clamp: 2; /* number of lines to show */
            -webkit-box-orient: vertical;
            height: 31px;
        }

        .text {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            line-height: 16px; /* fallback */
            -webkit-line-clamp: 4; /* number of lines to show */
            -webkit-box-orient: vertical;
        }
        .input-order { text-align:center; }

    </style>
</head>
<body class="content">
<section class="content noselect">
    <div class="row">
        @include('frontend.merchant.orders-list')
        @include('frontend.merchant.products-list')
    </div>
</section>

<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@include('frontend.merchant.script-pos')
<!-- Sweet Alert -->
<script src="{{ asset('sweetalert/dist/sweetalert.min.js') }}"></script>

</body>
</html>

