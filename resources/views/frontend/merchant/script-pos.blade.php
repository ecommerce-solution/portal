<script>
    document.addEventListener('contextmenu', event => event.preventDefault());

    function update() {
        var quantity = 0;
        var total = 0;
        var $price = $('[id^="order-list"]>[id^="order-"]>[class*="total"]>[id^="total-"]');
        var $items = $('[id^="order-list"]>[id^="order-"]>[class*="quantity"]>[id^="quantity-"]');

        $($items).each(function () {
            quantity += parseFloat($(this).val());
        });
        $($price).each(function () {
            total += parseFloat($(this).text());
        });

        if (quantity == 0 && total == 0) {
            $(".order").append("<div class=\"panel panel-default empty-list\" style=\"margin-bottom: 5px\">\n" +
                "            <div class=\"panel-body\">\n" +
                "            <div class=\"col-xs-12\" style=\"text-align: center;font-size: large;\">ยังไม่มีสินค้าในตะกร้า</div>\n" +
                "            </div>\n" +
                "            </div>");
            $('#total-order').text(0);
            $('#total-price').text("00.00 บาท");
            $('#payment').prop("disabled", true);
        }
        else {
            $('#total-order').text(quantity);
            $('#total-price').text(total.toFixed(2) + " บาท");
            $(".empty-list").remove();
            $('#payment').prop("disabled", false);
        }
    }

    function getProduct(x) {
        var total = 0;
        var quantity = 1;
        var product_name = x.getAttribute("data-product-name");
        var product_id = x.getAttribute("id");
        var product_price = x.getAttribute("data-product-price");
        var product_amount = x.getAttribute("data-product-amount");
        var check = ($("#order-" + product_id + "").length);
        total = parseFloat(total + product_price).toFixed(2) + " บาท";
        if (check == 0) {
            $(".order").append("      <div class=\"panel panel-default order-list\" style=\"margin-bottom: 0px;\" id=order-list" + product_id + ">\n" +
                "                             <div class=\"panel-body row \" id=order-" + product_id + " tabindex=\"0\" style=\"margin-bottom: 3px\">\n" +
                "                                <div class=\"col-xs-1\">\n" +
                "                                    <button class=\"clear btn btn-danger btn-circle\"  type=\"button\"  id=\"clear-" + product_id + "\" onclick=\"clearProduct(this," + product_id + ")\"><i class=\"fa fa-fw fa-close\"></i></button>\n" +
                "                                </div>\n" +
                "                                <div class=\"col-xs-2 order-name text\" style=\"margin-top: 5px;\" >" + product_name + "</div>\n" +
                "                                <div class=\"col-xs-2 order-price\" style=\"margin-top: 5px;\"><span id=\"price-" + product_id + "\">" + product_price + "</span></div>\n" +
                "                                <div class=\"col-xs-4 order-quantity\">\n" +
                "                                    <button class=\"minus btn btn-info btn-lg btn-circle\" type=\"button\" id=\"minus-" + product_id + "\" onclick=\"minusProduct(this," + product_id + ")\"><i class=\"fa fa-fw fa-minus\"></i></button>\n" +
                "\n" +
                "                                    <input type=\"text\" class=\"input-order form-control quantity \" value=\" " + quantity + "\n" +
                "                                          \" placeholder=\"0\" disabled maxlength=\"2\"\n" +
                "                                           style=\"width: 41px;display: inline;\" id=\"quantity-" + product_id + "\">\n" +
                "                                    <button class=\"plus btn btn-info btn-circle\" type=\"button\" id=\"plus-" + product_id + "\" onclick=\"plusProduct(this," + product_id + " ,"+product_amount+")\"><i class=\"fa fa-fw fa-plus\"></i></button>\n" +
                "                                </div>\n" +
                "                                <div class=\"order-amount \" id=\"order-amount" + product_id + "\" data-amount=\"" + product_amount + "\" style=\"display: none;\">" + product_amount + "</div>\n" +
                "                                <div class=\"col-xs-2 order-total\" style=\"margin-top: 5px;\"><span id=\"total-" + product_id + "\" >" + total + "</span></div>\n" +
                "                            </div> \n" +
                "                      </div>");
            $("#order-" + product_id).focus();
            update();
        }
        else {
            plusProduct(x, product_id,product_amount);
            $("#order-" + product_id).focus();
        }
    }

    function plusProduct(x, id, qtys) {
        var qty = qtys ;
        var productQuantity = parseFloat($('#quantity-' + id + '').val(), 10);
        var productPrice = ($('#price-' + id + '').text());

        if (productQuantity < qty) {
            $('#quantity-' + id + '').val(productQuantity + 1);
            parseFloat(sum = productPrice * (productQuantity + 1));
            $('#total-' + id + '').text(sum);
            document.getElementById('total-' + id + '').innerHTML = parseFloat(sum).toFixed(2) + " บาท";
            update();
        }
    }

    function minusProduct(x, id) {
        var productQuantity = parseFloat($('#quantity-' + id + '').val(), 10);
        var productPrice = ($('#price-' + id + '').text());

        if (productQuantity > 1 && productQuantity <= 99) {
            $('#quantity-' + id + '').val(productQuantity - 1);
            parseFloat(sum = productPrice * (productQuantity - 1));
            $('#total-' + id + '').text(sum);
            document.getElementById('total-' + id + '').innerHTML = parseFloat(sum).toFixed(2) + " บาท";
            update();
        }

    }

    function clearProduct(x, id) {
        $("#order-list" + id + "").remove();
        update();
    }

    var $items = $('[class^="item"]');
    var $btns = $('.category').click(function () {

        if (this.id == 'category-all') {
            $items.show();
        } else {
            var el = $('.' + this.id).show();
            $items.not(el).hide();
        }
        $btns.removeClass('active');
        $(this).addClass('active');
    });

    $('.select2').select2();

    $('select[name=user]').change(function () {
        $('#btn-step-1').prop("disabled", false);
    });

    var index = 1;

    $("#btn-step-1").click(function () {
        var selValue = $('select[name=user]').val();
        if (selValue != null) {
            $(".step-1").addClass('hide-modal');
            $(".step-2").removeClass('hide-modal');
            $("#back-step-1").removeClass('hide-modal');
            $("#close").addClass('hide-modal');

            var optionText = $('select[name=user] option:selected').text();
            $("#bill-name").html('<b>ชื่อผู้สั่งซื้อ : <h4 style=" display:inline-block;">' + optionText + '</h4></b>');

            var $items = $('[id^="order-list"]>[id^="order-"]');
            $($items).each(function () {
                var name = $(this).find(".order-name").text();
                var quantity = $(this).find(".quantity").val();
                var total = $(this).find(".order-total").text();
                $("#table-tbody").append("  <tr id=\"table-tr-index-" + index + "\">\n" +
                    "                                                                <td id=\"table-index-" + index + "\" style=\"text-align:center; width:30px;\">\n" +
                    "                                                                   " + index + " \n" +
                    "                                                                </td>\n" +
                    "                                                                <td id=\"table-name-" + index + "\" style=\"text-align:left; width:180px;\">\n" +
                    "                                                                    " + name + "\n" +
                    "                                                                </td>\n" +
                    "                                                                <td id=\"table-quantity-" + index + "\" style=\"text-align:center; width:50px;\">\n" +
                    "                                                                    " + quantity + "\n" +
                    "                                                                </td>\n" +
                    "                                                                <td id=\"table-price-" + index + "\" style=\"text-align:right; width:70px;\">\n" +
                    "                                                                    " + total + "\n" +
                    "                                                                </td>\n" +
                    "                                                            </tr>");

                index++;
            });

            $('#table-total-price').text($('#total-price').text());
            $('#table-total-quantity').text($('#total-order').text());

        }
    });

    $("#back-step-1").click(function () {
        $(".step-1").removeClass('hide-modal');
        $(".step-2").addClass('hide-modal');
        $("#back-step-1").addClass('hide-modal');
        $("#close").removeClass('hide-modal');
        var i;
        for (i = index; i > 1; i--) {
            $('#table-tr-index-' + (i - 1)).remove();
        }
        index = 1;
    });

    $("#btn-step-2").click(function () {

        $("#back-step-1").addClass('hide-modal');
        $("#close").removeClass('hide-modal');

        var data_user_id = $('select[name=user]').val();
        var optionText = $('select[name=user] option:selected').text();
        var data_total_price = $('#total-price').text().replace(" บาท", "");
        var data_product_id = [];
        var data_price = [];
        var data_product_name = [];
        var data_quantity = [];
        var data_product_total = [];
        var data_product_amount = [];
        var $items = $('[id^="order-list"]>[id^="order-"]');
        $($items).each(function () {
            data_product_id.push($(this).attr('id').replace("order-", ""));
            data_price.push($(this).find(".order-price").text());
            data_product_name.push($(this).find(".order-name").text());
            data_quantity.push($(this).find(".quantity").val());
            data_product_total.push($(this).find(".order-total").text().replace(" บาท", ""));
            data_product_amount.push(parseInt(($(this).find(".order-amount").text())));
        });
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            dataType: 'json',
            url: 'shop/payment',
            type: 'POST',
            _token: '{{ csrf_field() }}',
            data: {
                _token: $('input[name=_token]').val(),
                user_id: data_user_id,
                user_name: optionText,
                product_name: data_product_name,
                total_price: data_total_price,
                product_id: data_product_id,
                price_per_unit: data_price,
                product_qty: data_quantity,
                product_total: data_product_total,
                product_amount: data_product_amount,
            },
            success: function () {
                swal("สั่งซื้อ", "สินค้าสำเร็จ", "success")
                    .then((value) => {
                        deleteOrder();
                        window.location.reload();
                    });
            }

        });
    });

    $(".modal").on("hidden.bs.modal", function () {
        $("#back-step-1").addClass('hide-modal');
        $("#close").removeClass('hide-modal');
        $(".step-1").removeClass('hide-modal');
        $(".step-2").addClass('hide-modal');
        $('#user').val(null).trigger("change");
        $('#user').val('NULL').trigger('change');
        $('#btn-step-1').prop("disabled", true);
        $("#bill-name").html('ชื่อผู้ทำการสั่งซื้อ');
        var i;
        for (i = index; i > 1; i--) {
            $('#table-tr-index-' + (i - 1)).remove();
        }
        index = 1;
    });

    function deleteOrder() {
        var $order = $('[id^="order"]');
        $($order).each(function () {
            $order.children().remove();
        });
        update();
    }

</script>
