<div class="col-md-7">
    <div class="box box-danger" style="border-top-color:grey;">
        <div class="box-body">
            <h4 style="background:lightblue; font-size: 18px; text-align: center; padding: 12px 10px; margin-top: 0;">
                รายการสินค้า
            </h4>
            <div class="row">
                <div class="col-lg-10">
                        <button type="button" class="btn btn-default category active" id="category-all"> ทั้งหมด</button>
                        @foreach($categories as $category)
                            <button type="button" class="btn btn-default category"
                                    id="category-{{ $category->id }}">{{ $category->title }}</button>
                        @endforeach
                </div>
                <div class="col-lg-2">
                    <button type="button" class="btn btn-default category pull-right" onClick="window.location.reload()"
                            style="background-color: lavender; border-bottom: 3px solid lightyellow;">Refresh
                    </button>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 5px;">
                <div
                        style="position: relative; overflow: auto; width: auto; height: 800px; overflow-x: hidden; background-color:lightgrey;">
                    @foreach($products as $product)
                        <div class="item col-lg-2 col-md-3 col-xs-6 category-{{$product->product_category_id }}"
                             style="padding-left: 0px;padding-right: 0px; margin-top: 10px;margin-bottom: 10px;">
                            <center>
                                @if( $product->qty <= 0 )
                                    <button type="button" class="btn btn-default" id="{{ $product->id }}"
                                            data-product-name="{{ $product->name }}"
                                            data-product-price="{{ $product->price }}"
                                            data-product-amount="{{ $product->qty }}"
                                            onclick="getProduct(this)" style="background-color: white;" disabled>
                                        <div class="inline-label text-center"
                                             style="width: 100px;font-size: medium">{{ $product->name }}</div>
                                        <div style="width: 100px; height: 100px;">
                                            <img
                                                    src="{{ ($product->image != 'NULL') ? asset('uploads/'.$product->image) : "https://via.placeholder.com/180x120.png?text=No%20Image" }}"
                                                    style="height: 100px;width: 100px; "></div>
                                        <div class="text-center"
                                             style="margin-top: 4px;font-size: medium">{{ $product->price }} บาท
                                        </div>
                                        <div class="col" style="">
                                            {{ $product->qty }} ชิ้น
                                        </div>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-default" id="{{ $product->id }}"
                                            data-product-name="{{ $product->name }}"
                                            data-product-price="{{ $product->price }}"
                                            data-product-amount="{{ $product->qty }}"
                                            onclick="getProduct(this)" style="background-color: white;">
                                        <div class="inline-label text-center"
                                             style="width: 100px;font-size: medium">{{ $product->name }}</div>
                                        <div style="width: 100px; height: 100px;">
                                            <img
                                                    src="{{ ($product->image != 'NULL') ? asset('uploads/'.$product->image) : "https://via.placeholder.com/180x120.png?text=No%20Image" }}"
                                                    style="height: 100px;width: 100px;"></div>
                                        <div class="text-center"
                                             style="margin-top: 4px;font-size: medium">{{ $product->price }} บาท
                                        </div>
                                        <div class="col" style="">
                                            {{ $product->qty }} ชิ้น
                                        </div>
                                    </button>
                                @endif
                            </center>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

