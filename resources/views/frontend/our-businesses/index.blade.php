@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <header class="header-page header-career" style="background-image: url({{asset('../../img/page-bg.jpg')}});">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="title-page">
                        Our Business
                    </div>
                    <div class="title-caption">ลูกค้าของเรา</div>
                </div>
                <div class="col-sm-5 position-relative b-img">
                    <div class="item-image">
                        <img src="img/w1.png" alt="" id="item-image-1">
                        <img src="img/w2.png" alt="" id="item-image-2">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Our Business</li>
            </ol>
        </nav>
    </div>

    @if(!$ourBusinesses->isEmpty())

        <section class="section-portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title title-black mb-4">Our Business</div>
                    </div>
                    <br>

                    @foreach($ourBusinesses as $ourBusiness)
                        <div class="col-lg-4 col-sm-6 portfolio-item">
                            <div class="card h-100">
                                <a href="{{ route('ecommerce.our-business.detail', [$ourBusiness->id]) }}"><img
                                        class="card-img-top previewImageOurBusiness "
                                        src="{{ asset('uploads/'.$ourBusiness->thumbnail_image) }}" alt=""></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="{{ route('ecommerce.our-business.detail', [$ourBusiness->id]) }}"
                                           class="title-small title-hover">{{ $ourBusiness->title }}</a>
                                    </h4>
                                    <p class="card-text">{{ $ourBusiness->short_description }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-lg-12">
                        <!-- Pagination -->
                        <ul class="pagination justify-content-center">
                            {{ $ourBusinesses->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection
