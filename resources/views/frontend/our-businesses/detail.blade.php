@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.our-business.index') }}">Our Business</a></li>
            </ol>
        </nav>
    </div>
    <section class="section-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title title-post"> {{ $ourBusiness->title }}</h1>
                    <hr>
                    <br>
                    <div class="item-image" align="center">
                        <img src="{{ asset('uploads/'.$ourBusiness->cover_image) }}" alt=""
                             style="width: 750px;height: 500px">
                    </div>
                    <br>
                    <div class="item-post">
                        <p class="lead">
                            {{ $ourBusiness->short_description }}
                        </p>
                        {!! $ourBusiness->description !!}
                        <a href="{{ $ourBusiness->url }}">
                            {{ $ourBusiness->url }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
