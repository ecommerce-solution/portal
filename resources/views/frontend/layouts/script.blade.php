<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src='{{ asset('js/design/animate.js') }}'></script>
<script type="text/javascript" src="{{ asset('slick/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/design/custom.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datepicker/moment/min/moment.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('datepicker/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- Cleave.js -->
<script src="{{ asset('cleave.js/dist/cleave.min.js') }}"></script>
<script src="{{ asset('cleave.js/dist/addons/cleave-phone.th.js') }}"></script>