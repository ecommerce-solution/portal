<!DOCTYPE html>
<html class="home" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> @yield('title') </title>
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/design/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/design/custom.scss') }}" rel="stylesheet">
    <link href="{{ asset('css/design/_animations.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}"/>
    <link rel="stylesheet" type="fontawesome/css" href="{{ asset('font/all.css') }}" />
    <link rel="stylesheet"
          href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet"
          href="{{ asset('datepicker/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('css/data-tables/jquery.dataTables.min.css') }}">

    <link href="{{ asset('calendar/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('calendar/fullcalendar.print.min.css" rel="stylesheet" media="print') }}">
    <script src="{{ asset('calendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('calendar/lib/jquery.min.js') }}"></script>
    <script src="{{ asset('calendar/fullcalendar.min.js') }}"></script>

</head>

<body>

@include('frontend.partials.navbar')
@yield('content')
@include('frontend.partials.footer')
@include('frontend.layouts.script')
@stack('script')

</body>
</html>
