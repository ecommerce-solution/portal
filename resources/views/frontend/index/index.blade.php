@extends('frontend.layouts.main')

@section('title', 'E-Commerce Solution')

@section('content')
    @include('backend.partials.alert_error')
    @include('frontend.partials.header-silder')
    @include('frontend.partials.welcome')
    @include('frontend.partials.about-us')
    @include('frontend.partials.our-service')
    @include('frontend.partials.client')
    @include('frontend.partials.contact-form')


@endsection

