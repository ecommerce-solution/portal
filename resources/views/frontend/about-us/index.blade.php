@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <!-- Header Page -->
    <header class="header-page header-career" style="background-image: url('{{asset('img/page-bg.jpg')}}');">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="title-page">About Us</div>
                    <div class="title-caption">เกี่ยวกับเรา</div>
                </div>
                <div class="col-sm-5 position-relative b-img">
                    <div class="item-image">
                        <img src="img/w1.png" alt="" id="item-image-1">
                        <img src="img/w2.png" alt="" id="item-image-2">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">About us</li>
            </ol>
        </nav>
    </div>

    <!-- Page Content -->
    @if($aboutUses !=null)
        <section class="section-applyform">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="title title-black">About Us</div>
                        <br>
                        {{--<div data-id="1" style="text-align: center"><h4>{{ $aboutUses->title }}</h4></div>--}}
                        <div class="caption caption-black text-center">{!! $aboutUses->description !!}
                        </div>
                        <br>
                        <div class="item-image" style="text-align: center">
                            <img src="{{ asset('uploads/'.$aboutUses->image) }}" alt=""></div>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection
