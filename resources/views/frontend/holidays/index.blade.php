@extends('frontend.layouts.main')

@section('title', 'E-Commerce Solution')

@section('content')

    <section class="section-applyform">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="title title-black" data-id="1">Holiday</div>
                    <div id="calendar" style="max-width: 1200px">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            var events = JSON.parse('{!! json_encode($holidays) !!}')
            events = events.map(function (item) {
                return {
                    title: item.title,
                    backgroundColor:'#ff0',
                    color:'#ff0',
                    start: item.start_date,
                }
            })
            console.log(events)

            $('#calendar').fullCalendar({
                defaultDate: '{{ \Carbon\Carbon::now() }}',
                editable: false,
                eventLimit: true,
                events: events,
            });
        });
    </script>
@endsection
