@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <!-- Header Page -->
    <header class="header-page header-career" style="background-image:url({{ url('img/page-bg.jpg')}}) ;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="title-page">
                        Career
                    </div>
                    <div class="title-caption">ตำแหน่งงาน</div>
                </div>
                <div class="col-sm-6 position-relative b-img only-lg">
                    <div class="item-image">
                        <img src="{{ asset('img/w1.png') }}" alt="" id="item-image-1">
                        <img src="{{ asset('img/w2.png') }}" alt="" id="item-image-2">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Career</li>
            </ol>
        </nav>
    </div>

    <!-- Page Content -->
    @if(!$careers->isEmpty())
        <section class="section-position">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="title title-black">
                            Position
                        </div>
                    </div>
                    @foreach($careers as $career)
                        @if($career->status=="Open")
                            <div class="col-sm-10 list-view">
                                <div class="item d-flex ">
                                    <a href="{{ route('ecommerce.career.detail',[$career->id]) }}">
                                        <div class="item-image"><img src="{{ asset('uploads/'.$career->icon) }}" alt="">
                                        </div>
                                    </a>
                                    <div class="caption">
                                        <div class="title">{{ $career->role_name }}
                                            <a href="{{ route('ecommerce.career.detail',[$career->id]) }}">
                                                <div class="p-amount float-right">{{ $career->position }} position
                                                    <i class="fas fa-angle-right"></i>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="item-description">{{ $career->short_description }}</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div>
                    <button class="btn btn-apply"><a href="{{ route('ecommerce.career-form') }}">
                            <i class="fas fa-user-edit"></i> Apply now</a></button>
                </div>

            </div>
        </section>
        @include('frontend.partials.benefit-career')
    @endif

@endsection
