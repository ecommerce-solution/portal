@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.career.index') }}">Career</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $career->role_name }}</li>
            </ol>
        </nav>
    </div>

    <section class="section-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h1 class="title title-post" style="display: inline;"><img
                                src="{{ asset('uploads/'.$career->icon) }}"> {{ $career->role_name }} </h1>
                    </div>
                    <hr>
                    <br>
                    <div class="item-post">
                        <p class="lead">{{ $career->short_description }}</p>
                        {!! $career->description !!}
                    </div>
                    <hr>
                    <div>
                        <button class="btn btn-apply"><a
                                href="{{ route('ecommerce.career-form') }}?career={{ $career->role_name }}">
                                <i class="fas fa-user-edit"></i> Apply now</a></button>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
