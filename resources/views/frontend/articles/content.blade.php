@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('ecommerce.article') }}">Article</a></li>
                <li class="breadcrumb-item"><a
                        href="{{ route("ecommerce.article",[$article->category->slug]) }}">{{ $article->category->name }}</a></li>
            </ol>
        </nav>
    </div>

    <section class="section-news">
        <div class="container">
            <div class="row">
                <!-- Post Content Column -->
                <div class="col-lg-12">
                    <h1 class="title title-post"> {{ $article->title }}</h1>
                    <!-- Date/Time -->
                    <div class="item-meta text-muted">
                        <i class="fas fa-calendar-alt"> </i><span> {{ date('วันที่ d/m/Y  เวลา H:iน.',strtotime($article->publish_date)) }}</span>
                    </div>
                    <hr>
                    <br>
                    <div class="item-image" align="center">
                        <img src="{{ asset('uploads/'.$article->cover_image) }}" alt=""
                             style="width: 750px;height: 500px" href="{{ route('ecommerce.content',[$article]) }}">
                    </div>
                    <br><br>
                    <!-- Post Content -->
                    <div class="item-post">
                        <p class="lead">
                            {{ $article->short_description }}
                        </p>
                        <p>
                            {!! $article->description !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
