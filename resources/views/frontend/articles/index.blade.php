@extends('frontend.layouts.main')
@section('title', 'E-Commerce Solution')
@section('content')

    <!-- Header Page -->
    <header class="header-page header-career" style="background-image: url('{{asset('img/page-bg.jpg')}}');">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="title-page">
                        ข่าวสารและบทความ
                    </div>
                    <div class="title-caption">
                        รายละเอียดข่าวสาร
                    </div>
                </div>
                <div class="col-sm-5 position-relative b-img">
                    <div class="item-image">
                        <img src="{{ asset('img/w1.png') }}" alt="" id="item-image-1">
                        <img src="{{ asset('img/w2.png') }}" alt="" id="item-image-2">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Breadcrumb -->
    <div class="container">
        @if(request()->segment(2) === null)
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('ecommerce.article') }}">Article</a></li>
                </ol>
            </nav>
        @else
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('ecommerce.list') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('ecommerce.article') }}">Article</a></li>
                    @if(request()->segment(2) != null)
                        <li class="breadcrumb-item active">
                            <?php
                            $category = $categories->where('slug', request()->segment(2))->first();
                            ?>
                            {{ $category->name }}
                        </li>
                    @endif
                </ol>
            </nav>
        @endif
    </div>

    <!-- Page Content -->
    <section class="section-news">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 title-group">
                    <div class="title title-black mb-4 float-left" data-id="1">
                        News
                    </div>
                    <div class="seach-category d-flex float-right">
                        Category:
                        <div class="text-category text-muted">
                        </div>
                        <div class="dropdown">
                            <button type="button" class="btn btn-category dropdown-toggle" data-toggle="dropdown">
                                @if(request()->segment(2) != null)
                                    <?php
                                    $category = $categories->where('slug', request()->segment(2))->first();
                                    ?>
                                    {{ $category->name }}
                                @else
                                    all
                                @endif
                            </button>

                            <div class="dropdown-menu">
                                <a class="dropdown-item"
                                   href="{{ route('ecommerce.article',['slug' => null ]) }}">
                                    all
                                </a>
                                @foreach($categories as $category)
                                    <a class="dropdown-item"
                                       href="{{ route('ecommerce.article',['slug' => $category->slug]) }}">
                                        {{ $category->name }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Blog Post -->
                <div class="container">
                    @foreach($articles as $article)
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <a href="{{ route('ecommerce.content',[$article]) }}" class="item-image">
                                            <img class="img-fluid rounded"
                                                 src="{{ asset('uploads/'.$article->thumbnail_image) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="col-lg-7">
                                        <h2 class="card-title">{{ $article->title }}</h2>
                                        <div class="item-meta text-muted">
                                            <i class="fas fa-calendar-alt"> </i><span>
                                                {{ date('วันที่ d/m/Y  เวลา H:iน.',strtotime($article->publish_date)) }}</span>
                                        </div>
                                        <hr/>
                                        <p class="card-text">
                                            {{ $article->short_description }}
                                        </p>
                                        <a href="{{ route('ecommerce.content',[$article]) }}" class="btn btn-readmore">
                                            Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- Pagination -->
                <div class="col-lg-12">
                    <ul class="pagination justify-content-center" href="#">
                        {{ $articles->links() }}
                    </ul>
                </div>

                <!-- Other News -->
                @if($relateArticles->isNotEmpty())
                    <div class="other-news mb-3">
                        <div class="container">
                            <div class="row">
                                <!-- Static -->
                                <div class="col-lg-12">
                                    <div class="title-h2 mb-3">Other News</div>
                                </div>
                                <!-- Static -->
                                @foreach($relateArticles as $relateArticle)
                                    <div class="col-lg-3 col-sm-6">
                                        <a href="{{ route('ecommerce.content',[$relateArticle]) }}" class="item-image">
                                            <img class="img-fluid rounded"
                                                 src="{{ asset('uploads/'.$relateArticle->thumbnail_image) }}" alt="">
                                        </a>
                                        <a href="{{ route('ecommerce.content',[$relateArticle]) }}"
                                           class="title-small title-hover mt-2">{{ $relateArticle->title }}</a>
                                        <div class="item-meta text-muted">
                                            <i class="fas fa-calendar-alt"> </i><span> {{ date('วันที่ d/m/Y  เวลา H:iน.',strtotime($relateArticle->publish_date)) }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

@endsection
