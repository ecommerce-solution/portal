@extends('backend.layouts.main_dashboard')
@section('title', 'Job Skill Training List')
@section('title_header' , 'Job Skill Training List')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="float-md-right">
                    </div>
                    <br>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Index</th>
                            <th scope="col">Form id</th>
                            <th scope="col">Company</th>
                            <th scope="col">Date start</th>
                            <th scope="col">Date end</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Updated at</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($jobskill as $key => $jobskills)
                            <tr>
                                <td>{{ ($jobskill->currentPage()-1) * $jobskill->perPage() + $key+1 }}</td>
                                <td>{{ $jobskills->career_form_id }}</td>
                                <td>{{ $jobskills->company_name }}</td>
                                <td>{{ Carbon\Carbon::parse($jobskills->date_start)->format('d/m/Y')}}</td>
                                <td>{{ Carbon\Carbon::parse($jobskills->date_end)->format('d/m/Y')}}</td>
                                <td>{{ $jobskills->created_at }}</td>
                                <td>{{ $jobskills->updated_at }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target=".bd-example-modal-lg-{{$jobskills->id}}">Detail
                                    </button>
                                </td>
                            </tr>
                            <div class="modal fade in bd-example-modal-lg-{{$jobskills->id}}" tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h1 class="modal-title">Detail Job Skill & Training Form</h1>
                                            <h4 class="modal-title">Career id : {{ $jobskills->career_form_id }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="box-body">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Career id</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ $jobskills->career_form_id }}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">First name </label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{object_get($jobskills,'careerForm.firstname','-')}}</label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Last name </label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{object_get($jobskills,'careerForm.lastname','-')}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Company name</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ $jobskills->company_name }}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Date start</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ Carbon\Carbon::parse($jobskills->date_start)->format('d/m/Y')}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Date end</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ Carbon\Carbon::parse($jobskills->date_end)->format('d/m/Y')}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Job description</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ $jobskills->job_description}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Created at</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{ $jobskills->created_at }}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 control-label">Updated at</label>
                                                    <div class="col-sm-10">
                                                        <label class="control-label">{{$jobskills->updated_at }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $jobskill->links() }}

@endsection