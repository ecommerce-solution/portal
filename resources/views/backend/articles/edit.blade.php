@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Article')
@section('title_header' , 'Edit Article')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('articles.update',[$article->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control {{ $errors->has('title') ? 'has-error' : '' }}"
                           placeholder="Title" name="title"
                           value="{{ $article->title }}">
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    <label>Category <span style="color:red">*</span></label>
                    <select name="category_id" class="form-control">
                        <option selected disabled value="">กรุณาเลือก</option>
                        @foreach( $categories as $category)
                            <option @if ($category->id == $article->category_id) selected
                                    @endif value= {{ $category->id }}>{{$category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" name="short_description"
                              placeholder="Shoet Description" rows="4">{{ $article->short_description }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea rows="7" class="form-control ckeditor" placeholder="Description"
                              name="description">{{ $article->description }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('publish_date') ? 'has-error' : '' }}">
                    <label>Publish Date <span style="color:red">*</span></label>
                    <div class="input-group date" id="datetimepicker">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text"
                               value="{{ Carbon\Carbon::parse($article->publish_date)->format('d/m/Y H:i') }}"
                               class="form-control" name="publish_date" placeholder="DD/MM/YYYY HH:mm" data-mask>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('cover_image') ? 'has-error' : '' }}">
                    <label>Cover Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div id="divShowCover">
                            <a id="linkCover" href="{{ asset('uploads/'. $article->cover_image) }}" target="blank">
                                <img id="previewArticleCover" src="{{ asset('uploads/'. $article->cover_image) }}">
                            </a>
                            <div><input id="buttonCover" type="button" value="Clear" onclick="clearCover()"
                                        style="display:none"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readCover(this);" name="cover_image"
                           id="fileCover">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น
                        <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('thumbnail_image') ? 'has-error' : '' }}">
                    <label>Thumbnail Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div id="divShowThumbnail">
                            <a id="linkThumbnail" href="{{ asset('uploads/'. $article->thumbnail_image) }}"
                               target="blank">
                                <img id="previewArticleThumbnail"
                                     src="{{ asset('uploads/'. $article->thumbnail_image) }}">
                            </a>
                            <div><input id="buttonThumbnail" type="button" style="display:none"
                                        value="Clear" onclick="clearThumbnail()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readThumbnail(this);"
                           name="thumbnail_image" id="fileThumbnail">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น
                        <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="DRAFT" @if ($article->status == "DRAFT") selected @endif >DRAFT</option>
                        <option value="PUBLISHED" @if ($article->status == "PUBLISHED") selected @endif >PUBLISHED
                        </option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('articles.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script>
        $(function () {
            $('#datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                // ignoreReadonly: true,
                allowInputToggle: true,
                showClose: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
            });
        });

        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewArticleCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkCover').removeAttr('href');
                $('#buttonCover').show();
            }
        }

        function readThumbnail(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewArticleThumbnail').attr('src', e.target.result);
                    $('#divShowThumbnail').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkThumbnail').removeAttr('href');
                $('#buttonThumbnail').show();
            }
        }

        function clearThumbnail() {
            $('#previewArticleThumbnail').attr('src', "{{ asset('uploads/'. $article->thumbnail_image) }}");
            $('#linkThumbnail').attr('href', "{{ asset('uploads/'. $article->thumbnail_image) }}");
            $('#fileThumbnail').val(null);
            $('#buttonThumbnail').hide();
        }

        function clearCover() {
            $('#previewArticleCover').attr('src', "{{ asset('uploads/'. $article->cover_image) }}");
            $('#linkCover').attr('href', "{{ asset('uploads/'. $article->cover_image) }}");
            $('#fileCover').val(null);
            $('#buttonCover').hide();
        }

    </script>

@endpush
