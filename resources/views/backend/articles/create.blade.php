@extends('backend.layouts.main_dashboard')
@section('title', 'Create Article')
@section('title_header' , 'Create Article')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="{{ old('title') }}">
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    <label>Category <span style="color:red">*</span></label>
                    <select name="category_id" class="form-control">
                        <option selected disabled value="">กรุณาเลือก</option>
                        @foreach( $categories as $categorie )
                            <option value="{{ $categorie->id }}"
                                    {{ (old("category_id") == $categorie->id) ? 'selected' : '' }}>
                                {{ $categorie->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" name="short_description"
                              placeholder="Short Description" rows="4">{{ old('short_description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea id="inputDescription"
                              name="description" placeholder="Description">{{ old('description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('publish_date') ? 'has-error' : '' }}">
                    <label>Publish Date <span style="color:red">*</span></label>
                    <div class="input-group date" id="inputPublishDate">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" name="publish_date" placeholder="DD/MM/YYYY HH:mm"
                               data-mask value="{{ old('publish_date') }}">
                    </div>
                </div>
                <div class="form-group {{ $errors->has('cover_image') ? 'has-error' : '' }}">
                    <label>Cover Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none" id="divShowCover">
                            <img id="previewArticleCover">
                            <div><input type="button" value="Clear" onclick="clearCover()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readCover(this);" name="cover_image"
                           id="fileCover">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('thumbnail_image') ? 'has-error' : '' }}">
                    <label>Thumbnail Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none " id="divShowThumbnail">
                            <img id="previewArticleThumbnail">
                            <div><input type="button" value="Clear" onclick="clearThumbnail()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readThumbnail(this);"
                           name="thumbnail_image" id="fileThumbnail">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="DRAFT" {{ (old("status") == 'DRAFT') ? 'selected' : '' }}>DRAFT</option>
                        <option value="PUBLISHED" {{ (old("status") == 'PUBLISHED') ? 'selected' : '' }}>PUBLISHED</option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('articles.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script>
        $(function () {
            $('#inputPublishDate').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                // ignoreReadonly: true,
                allowInputToggle: true,
                showClose: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
            });
        });

        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewArticleCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readThumbnail(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewArticleThumbnail').attr('src', e.target.result);
                    $('#divShowThumbnail').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clearThumbnail() {
            $('#divShowThumbnail').hide();
            $('#previewArticleThumbnail').attr('src', false);
            $('#fileThumbnail').val(null);

        }

        function clearCover() {
            $('#divShowCover').hide();
            $('#previewArticleCover').attr('src', '');
            $('#fileCover').val(null);

        }

        CKEDITOR.replace('inputDescription');

    </script>

@endpush
