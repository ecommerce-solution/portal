@extends('backend.layouts.main_dashboard')
@section('title', 'Contact Us')
@section('title_header' , 'Contact Us')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('contact-us.save') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title"
                           name="title" value="{{ array_get($contact,'0.title') }}">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea id="short_description" class="form-control" name="short_description"
                              placeholder="Short Description"
                              rows="4">{{ array_get($contact,'0.short_description') }}</textarea>
                </div>
                <div class="form-group">
                    <label>Time </label>
                    <input type="text" class="form-control" name="time" placeholder="Time"
                           value="{{ array_get($contact,'0.time') }}">
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label>Email <span style="color:red">*</span></label>
                    <input type="email" class="form-control" name="email" placeholder="Email"
                           value="{{ array_get($contact,'0.email') }}">
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label>Address <span style="color:red">*</span></label>
                    <input type="text" class="form-control" name="address" placeholder="Address"
                           value="{{ array_get($contact,'0.address') }}">
                </div>
                <div class="form-group {{ $errors->has('tel') ? 'has-error' : '' }}">
                    <label>Tel <span style="color:red">*</span></label>
                    <input id="phone_number" type="text"  class="form-control" name="tel" placeholder="Tel"
                           value="{{ array_get($contact,'0.tel') }}">
                </div>
                <div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                    <label>Fax <span style="color:red">*</span></label>
                    <input id="fex" type="text"  class="form-control" name="fax" placeholder="Fax"
                           value="{{ array_get($contact,'0.fax') }}">
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
            <input type="hidden" value="{{ array_get($contact,'0.id') }}" name="contact_id">
        </form>
    </div>

@endsection

@push('script')
<script language="JavaScript" type="text/javascript">

    new Cleave('#phone_number', {
        phone: true,
        delimiter: ' ',
        phoneRegionCode: 'TH'
    });

    new Cleave('#fex', {
        phone: true,
        delimiter: ' ',
        phoneRegionCode: 'TH'
    });
</script>
@endpush
