@extends('backend.layouts.main_dashboard')
@section('title', 'Create Role')
@section('title_header' , 'Create Role')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('roles.store') }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label>Name <span style="color:red">*</span></label>
                    <input type="text" class="form-control"
                           placeholder="Name" name="name" id="inputName"
                           value="{{ old('name') }}">
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('roles.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection
