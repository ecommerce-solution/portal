@extends('backend.layouts.main_dashboard')
@section('title', 'Application Form Detail')
@section('title_header', 'Application Form Detail')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $applicationForm->firstname }} {{ $applicationForm->lastname }}</h3>
        </div>
        <form action="{{ route('application-form.update',[$applicationForm]) }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group row">
                    <label class="col-sm-2">Form id :</label>
                    <p class="col-sm-10">{{ $applicationForm->id }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">First name :</label>
                    <p class="col-sm-10">{{ $applicationForm->firstname }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Last name :</label>
                    <p class="col-sm-10">{{ $applicationForm->lastname }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Date :</label>
                    <p class="col-sm-10">{{ $list['date'] }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Phone Number :</label>
                    <p class="col-sm-10">{{ $applicationForm->phone }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Email :</label>
                    <p class="col-sm-10">{{ $applicationForm->email }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">University/School :</label>
                    <p class="col-sm-10">{{ $applicationForm->university }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Degree :</label>
                    <p class="col-sm-10">{{ $applicationForm->degree }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Major/Subject :</label>
                    <p class="col-sm-10">{{ $applicationForm->subject }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">GPA :</label>
                    <p class="col-sm-10">{{ $applicationForm->gpa }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Expected Salary :</label>
                    <p class="col-sm-10">{{ number_format($applicationForm->expected_salary) }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Work Experience :</label>
                    <div class="col-sm-10">
                        @if(!$applicationForm->jobSkillTrainings->isEmpty())
                            <table class="table table-bordered table-hover">
                                <td><b>#</b></td>
                                <td><b>Job Title</b></td>
                                <td><b>Company</b></td>
                                <td><b>Start Date</b></td>
                                <td><b>End Date</b></td>
                                <td><b>Action</b></td>

                                @foreach($applicationForm->jobSkillTrainings as $key => $jobSkillTraining)
                                    <tr>
                                        <td width="5%">{{ $key+1 }}</td>
                                        <td width="15%">{{ $jobSkillTraining->job_title }}</td>
                                        <td width="15%">{{ $jobSkillTraining->company_name }}</td>
                                        <td width="15%">{{ $jobSkillTraining->date_start }}</td>
                                        <td width="15%">{{ $jobSkillTraining->date_end}}</td>
                                        <td>
                                            <a data-toggle="modal"
                                               data-target=".bd-example-modal-lg-{{ $jobSkillTraining->id }}">Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <div class="modal fade in bd-example-modal-lg-{{ $jobSkillTraining->id }}"
                                         tabindex="-1"
                                         role="dialog"
                                         aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h1 class="modal-title">Work Experience</h1>
                                                    <h4 class="modal-title">Career Name
                                                        : {{ $applicationForm->firstname }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="box-body">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">First name</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $applicationForm->firstname }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Last name</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $applicationForm->lastname }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Company name</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $jobSkillTraining->company_name }}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Date start</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ Carbon\Carbon::parse($jobSkillTraining->date_start)->format('d/m/Y') }}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Date end</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ Carbon\Carbon::parse($jobSkillTraining->date_end)->format('d/m/Y')}}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Job
                                                                description</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $jobSkillTraining->job_description }}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Created at</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $jobSkillTraining->created_at }}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Updated at</label>
                                                            <div class="col-sm-10">
                                                                <label class="control-label">{{ $jobSkillTraining->updated_at }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </table>
                        @else
                            -
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Attach :</label>
                    <p class="col-sm-10"><a href="{{ asset('uploads/'. $applicationForm->attach) }}"
                                            target="_blank"> {{ $applicationForm->attach }}</a></p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Select :</label>
                    <div class="col-sm-10">
                        <select name="status" class="form-control">
                            <option value="Waiting" @if ($applicationForm->status == "Waiting") selected @endif >Waiting
                            </option>
                            <option value="Accepted" @if ($applicationForm->status == "Accepted") selected @endif >Accepted
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('application-form.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection
