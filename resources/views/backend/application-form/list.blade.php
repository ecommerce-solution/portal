@extends('backend.layouts.main_dashboard')
@section('title', 'Application Form List')
@section('title_header' , 'Application Form List')
@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="table  table-hover">
                        <div class="form-control-static">
                            <a class="btn btn-adn btn-sm" type="button" href="{{ route('application-form.export') }}">
                                <i class="fa fa-print"></i> Export Excel
                            </a>
                            <select class="selectpicker" multiple data-selected-text-format="count > 3"
                                    multiple title="Filter Career"
                                    name="inputSelectCareer" id="inputSelectCareer"
                                    onchange="window.LaravelDataTables['tableApplicationForm'].draw()">
                                @foreach($careers as $career)
                                    <option value="{{ $career->id }}">{{ $career->role_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row" style="margin-left:20%; margin-top: 40px ">
                            <div class="col-lg-4 col-xs-10 col-md-5">
                                <label class="col-lg-3">From</label>
                                <div class="input-group date col-lg-9" id="formDate">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="inputFormDate"
                                           name="formDate"
                                           value="" placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>

                            <div class="form-group col-lg-4 col-xs-10 col-md-5">
                                <label class="col-lg-2">To</label>
                                <div class="input-group date col-lg-9" id="toDate">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="inputToDate" name="toDate"
                                           value="" placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 col-xs-12 col-md-12">
                                <button type="button" class="btn btn-success btn-sm" onclick="applyFilter()">Apply</button>
                                <button type="button" class="btn btn-success btn-sm" onclick="clearFilter()">Clear</button>
                            </div>

                        </div>
                        {!! $dataTable->table(['class' => 'table table-bordered table-responsive data-table',
                        'id' => 'tableApplicationForm', 'style' => 'width:100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">


            $(document).ready(function () {
                $(document).ready(function () {
                    $(document).on('click', '.btn-delete', function (e) {
                        e.preventDefault();
                        swal({
                            text: "ยืนยันการลบข้อมูล",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                            .then((willDelete) => {
                                if (willDelete) {
                                    var param = $(this).data('url');
                                    var method_type = 'DELETE';

                                    if ($(this).data('method-type')) {
                                        method_type = $(this).data('method-type');
                                    }
                                    $.ajax({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                        },
                                        dataType: 'json',
                                        url: param,
                                        type: method_type,
                                        _token: '{{ csrf_field() }}',

                                    }).done(function (data) {
                                        swal(data.message, {
                                            icon: "success",
                                        });
                                        $('.data-table').DataTable().draw();
                                    });
                                }
                            });
                    });
                })
            });

        $(function () {
            $('.date').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
            });
        });

        $(function () {

            $("#formDate").on("dp.change", function (e) {
                $('#toDate').data("DateTimePicker").minDate(e.date);
                $('#inputToDate').val(e.date.format('DD/MM/YYYY'));
            });
        });


        function applyFilter() {
            window.LaravelDataTables['tableApplicationForm'].draw()
        }

        function clearFilter() {
            $('#inputFormDate').val("");
            $('#inputToDate').val("");
            $('#toDate').data("DateTimePicker").minDate(false);
            window.LaravelDataTables['tableApplicationForm'].draw()
        }

    </script>
    {!! $dataTable->scripts() !!}

@endpush
