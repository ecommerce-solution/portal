@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Product')
@section('title_header' , 'Edit Product')
@section('content')

    <div class="box box-primary">
        <form action="{{ route('products.update',[$product->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label>ชื่อ <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Name" name="name"
                           value="{{ $product->name }}">
                </div>
                <div class="form-group {{ $errors->has('product_category_id') ? 'has-error' : '' }}">
                    <label>หมวดหมู่ <span style="color:red">*</span></label>
                    <select name="product_category_id" class="form-control">
                        <option disabled>กรุณาเลือก</option>
                        @foreach( $product_categories as $product_category )
                            <option value="{{ $product_category->id }}"
                                {{ $product->product_category_id == $product_category->id ? 'selected' : '' }}>
                                {{ $product_category->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                    <label>ราคา (ชิ้น) <span style="color:red">*</span></label>
                    <input type="text" onkeypress=check_number(); class="form-control" placeholder="Price / Unit" name="price"
                           value="{{ $product->price }}">
                </div>
                <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }}">
                    <label>จำนวน</label>
                    <input type="text" id="qty" class="form-control" readonly value="{{ $product->qty }}" name="qty"
                           style="background: white">
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <select name="type" id="type" class="form-control" onchange="changeSelect(this)">
                            <option value="increase">เพิ่มจำนวน</option>
                            <option value="decrease">ลดจำนวน</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Quantity / Unit"
                                   value="" id="inputQty" onkeyup="calculateQty(this)">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>รูป</label>
                    <div class="form-group">
                        <div id="divShowImg">
                            <a id="linkProduct"
                               href="{{ ($product->image == 'NULL') ? '' : asset('uploads/'. $product->image) }}"
                               target="blank">
                                <img id="previewProduct"
                                     src="{{ ($product->image == 'NULL') ? 'https://via.placeholder.com/180x120.png?text=No%20Image'
                                     : asset('uploads/'. $product->image) }}">
                            </a>
                            <div><input type="button" value="Clear" onclick="clearProduct()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readProduct(this);" id="fileProduct"
                           name="image">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB <br>
                    </p>
                </div>
                <div class="form-group">
                    <label>รายละเอียดสินค้า</label>
                    <textarea class="form-control" name="description"
                              placeholder="Description" rows="4">{{ $product->description }}</textarea>
                </div>
            </div>

            <div class="box-footer" style="text-align: center">
                <a href="{{ route('products.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fa fa-floppy-o"></i> Save
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')

    <script language="JavaScript">

        new Cleave('#inputQty', {
            blocks: [6],
            numericOnly: true,
        });

        function readProduct(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewProduct').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkProduct').removeAttr('href');
            }
        }

        function clearProduct() {
            var image = '{{ $product->image }}';
            if (image == 'NULL') {
                $('#previewProduct').attr('src', "https://via.placeholder.com/180x120.png?text=No%20Image");
            }
            else {
                $('#previewProduct').attr('src', "{{ asset('uploads/'. $product->image) }}");
                $('#linkProduct').attr('href', "{{ asset('uploads/'. $product->image) }}");
            }
            $('#fileProduct').val(null);
        }

        function changeSelect() {
            $('#inputQty').val(null);
            $('#qty').val(parseInt("{{ $product->qty }}"));
        }

        function calculateQty($x) {
            var type = $('#type').val();
            if ($x.value != '') {
                if (type == 'increase') {
                    $('#qty').val(parseInt($x.value) + parseInt("{{ $product->qty }}"));
                } else if (type == 'decrease') {
                    $('#qty').val(parseInt("{{ $product->qty }}") - parseInt($x.value));
                }
            } else {
                $('#qty').val(parseInt("{{ $product->qty }}"));
            }
        }

        function check_number() {
            e_k=event.keyCode

            if (e_k != 13 && (e_k < 48) || (e_k > 57)) {
                event.returnValue = false;
            }
        }

    </script>

@endpush
