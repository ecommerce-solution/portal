@extends('backend.layouts.main_dashboard')
@section('title', 'Balance Detail')
@section('title_header', 'Balance Detail')
@section('content')
    <div class="box box-primary">
        <section class="content">
            <div class="box-body">
                <div class="form-group row">
                    <label class="col-sm-2">User ID :</label>
                    <p class="col-sm-10">{{ $user_id }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Name :</label>
                    <p class="col-sm-10">{{ data_get($orders,'0.user_name','-') }}</p>
                </div>
                @if(!$orders->isEmpty())
                    <form action="{{ route('balances.update') }}" method="post" id="update">
                        @csrf
                        <div class="form-group">
                            <label>Select</label>
                            <select class="form-control" name="status_new">
                                <option value="unpaid" {{ ($status == 'unpaid') ? 'selected':'' }}>UNPAID</option>
                                <option value="paid" {{ ($status == 'paid') ? 'selected':'' }}>PAID</option>
                            </select>
                            <input value="{{ $date }}" type="hidden" name="date">
                            <input value="{{ $status }}" type="hidden" name="status">
                            <input value="{{ $user_id }}" type="hidden" name="user_id">
                        </div>
                    </form>

                @endif
                <div class="form-group row">
                    <label class="col-sm-2">Order List :</label>
                    <div class="col-sm-10 scroll-order-detail"
                         style="overflow: auto; overflow-x: hidden; width: auto; height: 450px;">
                        @if(!$orders->isEmpty())
                            <table class="table table-bordered table-hover" id="table">
                                <td><b>#</b></td>
                                <td><b>Order ID</b></td>
                                <td><b>Price</b></td>
                                <td><b>Created At</b></td>
                                <td>Action</td>
                                <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td width="10%">{{ $key+1 }}</td>
                                        <td width="10%">{{ data_get( $order,'id') }}</td>
                                        <td width="20%">{{ data_get( $order,'total_price') }}</td>
                                        <td class="total" width="20%">{{ $order->created_at}}</td>
                                        <td width="10%"><a href="#fee-details-{{ data_get($order,'id') }}"
                                                           data-toggle="modal">Detail</a></td>
                                    </tr>
                                @endforeach
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Order</b></td>
                                <td><b>Total Price</b></td>
                                <tr>
                                    <td colspan="1"></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ count($orders) }}</td>
                                    <td>{{number_format((($total_price)), 2) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        @else
                            -
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <div class="box-footer" style="text-align: center">
            <a href="{{ route('balances.list') }}" class="btn btn-danger btn-sm"><i
                        class="fa fa-arrow-left"></i> Back</a>
            @if(!$orders->isEmpty())
                <button type="submit" form="update" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save
                </button>
            @endif
        </div>
        @foreach($orders as $key => $order)
            <div class="modal fade" id="fee-details-{{ data_get($order,'id') }}" tabindex="-1" role="dialog"
                 aria-labelledby="fee-details-label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h1 class="modal-title">Order Detail</h1>
                            <h4 class="modal-title">Order Id
                                : {{ data_get($order,'id') }}</h4>
                            <h4 class="modal-title">Total Price : {{ data_get($order,'total_price') }}</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table-modal table-condensed">
                                <thead>
                                <tr>
                                    <th><b>#</b></th>
                                    <th><b>Product Name</b></th>
                                    <th><b>Price Per Unit</b></th>
                                    <th><b>Quantity</b></th>
                                    <th><b>Total Price Per Unit</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order->orderDetails as $key => $orderDetail)
                                    <tr>
                                        <td width="5%">{{ $key+1 }}</td>
                                        <td width="15%">{{ data_get( $orderDetail,'products.name') }}</td>
                                        <td width="15%">{{ $orderDetail->price_per_unit }}</td>
                                        <td class="modal-quantity-{{ data_get($order,'id')  }}" width="15%">{{ $orderDetail->product_qty}}</td>
                                        <td class="modal-total-{{ data_get($order,'id')  }}" width="15%">{{ $orderDetail->total_price}}</td>
                                    </tr>

                                @endforeach
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Quantity</b></td>
                                <td><b>Total Price</b></td>
                                <tr>
                                    <td colspan="1"></td>
                                    <td></td>
                                    <td></td>
                                    <td class="total-modal-quantity">{{ $order->orderDetails->sum('product_qty') }}</td>
                                    <td class="total-modal-price">{{ $order->total_price }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        @endforeach
    </div>
@endsection
