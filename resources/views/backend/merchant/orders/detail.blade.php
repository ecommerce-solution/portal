@extends('backend.layouts.main_dashboard')
@section('title', 'History Detail')
@section('title_header', 'History Detail')
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="form-group row">
                <label class="col-sm-2">Order id :</label>
                <p class="col-sm-10">{{ $order->id }}</p>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Total price :</label>
                <p class="col-sm-10">{{ $order->total_price }}</p>
            </div>
            {{--<div class="form-group row">--}}
                {{--<label class="col-sm-2">Status :</label>--}}
                {{--<p class="col-sm-10">{{ $order->status }}</p>--}}
            {{--</div>--}}
            <div class="form-group row">
                <label class="col-sm-2">Date :</label>
                <p class="col-sm-10">{{ $order->created_at }}</p>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Order Detail :</label>
                <div class="col-sm-10 scroll-order-detail" style="overflow: auto; overflow-x: hidden; width: auto; height: 450px;">
                    {{--@dd($order->orderDetails);--}}
                    @if(!$order->orderDetails->isEmpty())
                        <table class="table table-bordered table-hover" id="table">
                            <td><b>#</b></td>
                            <td><b>Product Name</b></td>
                            <td><b>Price Per Unit</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Total Price Per Unit</b></td>
                            <tbody>
                            @foreach($order->orderDetails as $key => $orderDetail)
                                <tr>
                                    <td width="5%">{{ $key+1 }}</td>
                                    <td width="15%">{{ data_get( $orderDetail,'products.name') }}</td>
                                    <td width="15%">{{ $orderDetail->price_per_unit }}</td>
                                    <td class="quantity" width="15%">{{ $orderDetail->product_qty}}</td>
                                    <td class="total" width="15%">{{ $orderDetail->total_price}}</td>
                                </tr>
                            @endforeach
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total Quantity</b></td>
                            <td><b>Total Price</b></td>
                            <tr>
                                <td colspan="1"></td>
                                <td></td>
                                <td></td>
                                <td class="total-quantity"></td>
                                <td class="total-price"></td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                        -
                    @endif

                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('orders.list') }}" class="btn btn-danger btn-sm"><i
                            class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>

@endsection

@push('script')

    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            calculateQuantity();
            calculateTotalPrice();
        });

        function calculateQuantity() {
            var total = 0;
            $('#table .quantity').each(function () {
                var value = parseInt($(this).html());
                total += value;
                $('.total-quantity').text(total);
            });
        }

        function calculateTotalPrice() {
            var total = 0;
            $('#table .total').each(function () {
                var value = parseFloat($(this).html());
                total += value;
                $('.total-price').text(total.toFixed(2));
            });
        }
    </script>

@endpush
