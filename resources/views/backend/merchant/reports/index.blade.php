
<div class="form-group">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ค้นหาโดย
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="{{ route('reports.selectProduct') }}">เลือกตามรายการสั่งซื้อ</a></li>
            <li><a href="{{ route('reports.selectOrder') }}">เลือกตามออร์เดอร์</a></li>
            <li><a href="{{ route('reports.selectUser') }}">เลือกตามรายชื่อ</a></li>
            {{--<li><a href="{{ route('reports.summary') }}">สรุปยอดค้างชำระ</a></li>--}}
        </ul>
    </div>
</div>
<h4>เลือกวันที่ต้องการดูรายการ</h4>
<div class="input-group col-sm-2 date " id="fromDate">
    <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
    </div>
    <input type="text" class="form-control pull-right" id="inputFromDate"
           name="fromDate"
           value="" placeholder="DD/MM/YYYY" autocomplete="off"/>
</div>
<div class="input-group col-sm-2 date " id="toDate">
    <div class="input-group-addon date">
        <i class="fa fa-calendar"></i>
    </div>
    <input type="text" class="form-control pull-right" id="inputToDate" name="toDate"
           value="" placeholder="DD/MM/YYYY" autocomplete="off"/>
</div>
