@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header' , 'Product Report')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="float-md-right">
                    <div class="table table-hover">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col" class="text-center">#</th>
                                <th scope="col" class="text-center col-sm-1">เลขออร์เดอร์</th>
                                <th scope="col" class="text-center">ผู้ซื้อ</th>
                                <th scope="col" class="text-center">สินค้า</th>
                                <th scope="col" class="text-center">ราคาต่อชิ้น</th>
                                <th scope="col" class="text-center col-sm-1">จำนวน(ชิ้น)</th>
                                <th scope="col" class="text-center">ราคารวม</th>
                                <th scope="col" class="text-center">วันที่</th>
                                <th scope="col" class="text-center">เวลา</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order_details as $key => $order_detail)
                                <tr>
                                    <td class="text-center">{{ ($key+1+(($page-1)*$perPage)) }}</td>
                                    <td class="text-center">{{ $order_detail->order_id }}</td>
                                    <td class="text-center">{{ data_get($order_detail,'order.user_name','-') }}</td>
                                    <td class="text-center">{{ $order_detail->product_name }}</td>
                                    <td class="text-center">{{ $order_detail->price_per_unit }}</td>
                                    <td class="text-center col-sm-1">{{ $order_detail->product_qty }}</td>
                                    <td class="text-center">{{ $order_detail->total_price }}</td>
                                    <td class="text-center">{{ date('d-m-Y'), strtotime($order_detail->created_at) }}</td>
                                    <td class="text-center">{{ date('H:i น', strtotime($order_detail->created_at)) }}</td>
                                </tr>
                            @endforeach
                            @if(empty($order_details))
                            <tr>
                                <td class="text-center"></td>
                            </tr>
                                @endif
                            </tbody>
                        </table>
                        <center>{{ $order_details->links() }}</center>
                    </div>
                </div>
                <div style="text-align: center">
                    <a href="{{ route('reports.selectProduct') }}" class="btn btn-danger btn-sm"><i
                            class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                </div>
                <br>
            </div>
        </div>
    </div>
@endsection
