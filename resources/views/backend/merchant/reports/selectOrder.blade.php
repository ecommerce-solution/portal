@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header' , 'Order Report')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body ">
                    <div class="float-md-right">
                        <form action="{{ route('reports.selectOrder') }}" method="get">
                            @include('backend.merchant.reports.index')
                            <br>
                            <div class="input-group date" id="submitDate">
                                <button type="submit" class="btn btn-success btn-md" style="margin-right: 10px"><i class="fa fa-search-minus"></i>
                                    Search
                                </button>
                                <a href="{{ route('reports.exportOrder') }}" class="btn btn-adn"><i class="fa fa-print"></i>Export</a>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="float-md-right">
                                            <div class="table table-hover">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" class="text-center">#</th>
                                                        <th scope="col" class="text-center">Name</th>
                                                        <th scope="col" class="text-center">Date</th>
                                                        <th scope="col" class="text-center">Time</th>
                                                        <th scope="col" class="text-center">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($orders as $key => $order)
                                                        <tr>
                                                            <td class="text-center">{{ ++$key }}</td>
                                                            <td class="text-center">{{ $order->user_name }}</td>
                                                            <td class="text-center">{{ date('d-m-Y'), strtotime($order->created_at) }}</td>
                                                            <td class="text-center">{{ date('H:i น', strtotime($order->created_at)) }}</td>
                                                            <td class="text-center">{{ $order->total_price }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('.date').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    widgetPositioning: {
                        horizontal: 'auto',
                        vertical: 'bottom'
                    },
                });
            });
            $(function () {
                $("#fromDate").on("dp.change", function (e) {
                    $('#toDate').data("DateTimePicker").minDate(e.date);
                    $('#inputToDate').val('');
                });
            });
        });
    </script>
@endpush
