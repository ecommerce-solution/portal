@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header', 'Total Report')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body ">
                    <div class="col embed-responsive-1by1">
                        <form action="{{ route('reports.printSummary') }}" method="GET">
                            <div class="float-md-right">
                                <div class="table table-hover">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="text-center">#</th>
                                            <th scope="col" class="text-center">ชื่อ</th>
                                            <th scope="col" class="text-center">วันที่(ล่าสุด)</th>
                                            <th scope="col" class="text-center">เวลา(ที่ซื้อล่าสุด)</th>
                                            <th scope="col" class="text-center">ยอดรวม</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $count = 1;?>
                                        @foreach($orders as $key => $order_items)
                                            <tr>
                                                <td class="text-center">{{ $count++ }}</td>
                                                @if(count($order_items) == 1)
                                                    <td class="text-center">{{ $order_items[0]['user_name'] }}</td>
                                                    <td class="text-center">{{ date('d/m/Y',strtotime($order_items[0]['updated_at'])) }}</td>
                                                    <td class="text-center">{{ date('H:i น',strtotime($order_items[0]['updated_at'])) }}</td>
                                                    <td class="text-center">{{ $order_items[0]['total_price'] }}</td>
                                                @else
                                                    <?php $username = $order_items[0]['user_name'] ?>
                                                    <td class="text-center">{{ $username }}</td>
                                                    <?php $total = 0;?>
                                                    @for($i = 0 ;$i<count($order_items);$i++)
                                                        <?php $total = $total + $order_items[$i]['total_price'] ?>
                                                    @endfor
                                                    <td class="text-center">{{ date('d/m/Y',strtotime($order_items[0]['updated_at'])) }}</td>
                                                    <td class="text-center">{{ date('H:i น.',strtotime($order_items[0]['updated_at'])) }}</td>
                                                    <td class="text-center">{{ number_format($total,2) }}</td>
                                                    <?php $total = 0;?>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center">
                                <a href="{{ route('reports.selectProduct') }}" class="btn btn-danger btn-sm"><i
                                        class="fa fa-arrow-left"></i> Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
