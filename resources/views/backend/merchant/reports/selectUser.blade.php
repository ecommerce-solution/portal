@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header' , 'User Report')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body ">
                    <div class="float-md-right">
                        <form action="{{ route('reports.userSelected') }}" method="get">
                            @include('backend.merchant.reports.index')
                            <br>
                            <select id="user" class="selectpicker" data-live-search="true"
                                    title="รายชื่อสมาชิก" autocomplete="off" name="userSelected">
                                @foreach($users as $user)
                                    <option>{{ $user->user_name }}</option>
                                @endforeach
                            </select>
                            <br><br>

                            <div class="input-group date" id="submitDate">
                                <button type="submit" class="btn btn-success btn-md" style="margin-right: 10px"><i
                                        class="fa fa-search-minus"></i>
                                    Search
                                </button>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('.date').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    widgetPositioning: {
                        horizontal: 'auto',
                        vertical: 'bottom'
                    },
                });
            });
            $(function () {
                $("#fromDate").on("dp.change", function (e) {
                    $('#toDate').data("DateTimePicker").minDate(e.date);
                    $('#inputToDate').val('');
                });
            });
        });
    </script>
@endpush
