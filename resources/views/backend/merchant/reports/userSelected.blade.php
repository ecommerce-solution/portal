@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header' , 'User Report')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="float-md-right">
                    <form action="{{ route('reports.exportSummary') }}" method="get">
                    <button type="submit" class="btn btn-adn"><i class="fa fa-file-excel-o"> Export Excel</i></button>
                    <div class="table table-hover">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col" class="text-center">ชื่อ</th>
                                <th scope="col" class="text-center">ยอดรวม</th>
                                <th scope="col" class="text-center">วันที่</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(empty($users))
                                @if($user != null)
                                    <tr>
                                        <td class="text-center">{{ data_get($user,'user_name','-') }}</td>
                                        <td class="text-center">{{ data_get($user,'total_price','-') }}</td>
                                        <td class="text-center">{{ date('d-m-Y',strtotime($from_date)). ' - ' .date('d-m-Y',strtotime($to_date)) }}</td>
                                    </tr>
                                    <input type="hidden" value="{{ $user }}" name="user">
                                @else
                                    <tr>
                                        <td class="text-center"></td>
                                        <td class="text-center">ไม่มีข้อมูล</td>
                                        <td class="text-center"></td>
                                        <input type="hidden" value="{{ $user }}" name="user">
                                    </tr>
                                @endif
                            @else
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{ data_get($user,'user_name','-') }}</td>
                                        <td class="text-center">{{ data_get($user,'total_price','-') }}</td>
                                        <td class="text-center">{{ date('d-m-Y',strtotime($from_date)). ' - ' .date('d-m-Y',strtotime($to_date)) }}</td>
                                    </tr>
                                    <input type="hidden" value="{{ $user }}" name="users[]">
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div style="text-align: center">
                            <a href="{{ route('reports.selectUser') }}" class="btn btn-danger btn-sm"><i
                                    class="fa fa-arrow-left"></i>ย้อนกลับ</a>
                        </div>
                        <br>
                        <input type="hidden" value="{{ $from_date }}" name="fromDate">
                        <input type="hidden" value="{{ $to_date }}" name="toDate">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
