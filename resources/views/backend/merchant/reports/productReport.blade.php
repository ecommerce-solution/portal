@extends('backend.layouts.main_dashboard')
@section('title', 'Product Report')
@section('title_header' , 'Product Report')
@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="table table-hover">
                        <div class="form-control-static">
                            <div class="form-group row" style="margin-left:9%; margin-top: 40px ">
                                <div class="form-group col-lg-3 col-xs-10 col-md-5">
                                    <label class="col-lg-3 ">From</label>
                                    <div class="input-group date col-lg-9" id="formDate">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="inputFormDate"
                                               name="formDate"
                                               value="" placeholder="DD/MM/YYYY"/>
                                    </div>
                                </div>

                                <div class="form-group col-lg-3 col-xs-10 col-md-5">
                                    <label class="col-lg-2 ">To</label>
                                    <div class="input-group date col-lg-9" id="toDate">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="inputToDate" name="toDate"
                                               value="" placeholder="DD/MM/YYYY"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-xs-12 col-lg-3">
                                    <select class="selectpicker" title="เลือกหมวดหมู่"
                                            name="inputSelectProduct" id="inputSelectProduct">
                                        <option value="All" selected>สินค้าทั้งหมด</option>
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-lg-3 col-xs-12">
                                    <button type="button"  class="btn btn-success btn-sm"  onclick="applyFilter()">Apply</button>
                                    <button type="button" class="btn btn-success btn-sm" onclick="clearFilter()">Clear</button>
                                </div>
                            </div>
                        </div>
                        {!! $dataTable->table(['class' => 'table table-bordered table-responsive data-table',
                        'id' => 'tableProductReport', 'style' => 'width:100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script language="JavaScript" type="text/javascript">

        $(function () {
            $('.date').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
            });
        });

        $(function () {
            $("#formDate").on("dp.change", function (e) {
                $('#toDate').data("DateTimePicker").minDate(e.date);
                $('#inputToDate').val(e.date.format('DD/MM/YYYY'));
            });
        });

        function applyFilter() {
            window.LaravelDataTables['tableProductReport'].draw()
        }

        function clearFilter() {
            $('#inputFormDate').val("");
            $('#inputToDate').val("");
            $('#inputSelectProduct').val('All');
            $('#inputSelectProduct').selectpicker("refresh");
            $('#toDate').data("DateTimePicker").minDate(false);
            window.LaravelDataTables['tableProductReport'].draw()
        }

    </script>

    {!! $dataTable->scripts() !!}

@endpush
