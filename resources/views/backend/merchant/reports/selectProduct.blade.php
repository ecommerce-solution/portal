@extends('backend.layouts.main_dashboard')
@section('title', 'Report')
@section('title_header' , 'รายการสั่งสินค้าทั้งหมด')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body ">
                    <div class="col embed-responsive-1by1">
                        <form action="{{ route('reports.sendProduct') }}" method="get">
                            <div class="float-md-right">
                                @include('backend.merchant.reports.index')
                                <br>
                                <div class="input-group col-sm-2 date">
                                    <select id="product" class="selectpicker" data-live-search="true"
                                            title="เลือกสินค้าที่ต้องการดู" autocomplete="off" name="productSelected">
                                        @foreach($products as $product)
                                            <option>{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <div class="input-group date" id="submitDate">
                                    <button type="submit" class="btn btn-success btn-md" style="margin-right: 10px"><i
                                            class="fa fa-search-minus"></i>
                                        Search
                                    </button>
                                    <a href="{{ route('reports.exportOrder') }}"
                                       class="btn btn-adn"><i class="fa fa-file-excel-o"></i> Export Excel</a>
                                </div>
                                <br>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" class="text-center">#</th>
                                    <th scope="col" class="text-center col-sm-1">เลขออเดอร์</th>
                                    <th scope="col" class="text-center">ผู้ซื้อ</th>
                                    <th scope="col" class="text-center">สินค้า</th>
                                    <th scope="col" class="text-center">ราคาต่อชิ้น</th>
                                    <th scope="col" class="text-center col-sm-1">จำนวน(ชิ้น)</th>
                                    <th scope="col" class="text-center">ราคารวม</th>
                                    <th scope="col" class="text-center">วันที่</th>
                                    <th scope="col" class="text-center">เวลา</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allOrders as $key => $allOrder)
                                    <tr>
                                        <td class="text-center">{{ ($key+1+(($page-1)*$perPage)) }}</td>
                                        <td class="text-center">{{ $allOrder->order_id }}</td>
                                        <td class="text-center">{{ data_get($allOrder, 'order.user_name', '-') }}</td>
                                        <td class="text-center">{{ $allOrder->product_name }}</td>
                                        <td class="text-center">{{ $allOrder->price_per_unit }}</td>
                                        <td class="text-center col-sm-1">{{ $allOrder->product_qty }}</td>
                                        <td class="text-center">{{ $allOrder->total_price }}</td>
                                        <td class="text-center">{{ date('d-m-Y'), strtotime($allOrder->created_at) }}</td>
                                        <td class="text-center">{{ date('H:i น', strtotime($allOrder->created_at)) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <center>{{ $allOrders->links() }}</center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('.date').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,
                    allowInputToggle: true,
                    widgetPositioning: {
                        horizontal: 'auto',
                        vertical: 'bottom'
                    },
                });
            });
            $(function () {
                $("#fromDate").click("dp.change", function () {
                    $('#toDate').data("DateTimePicker").minDate(e.date);
                    $('#inputToDate').val('');
                });
            });
        });
    </script>
@endpush


