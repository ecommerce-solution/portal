@extends('backend.layouts.main_dashboard')
@section('title', 'Product Category List')
@section('title_header' , 'Product Category List')
@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="float-md-right">
                        <a href="{{ route('product-categories.create') }}" class="btn btn-success btn-sm"><i
                                    class="fa fa-plus"></i>
                            Create Product Category
                        </a>
                    </div>
                    <div class="table table-hover">
                        {!! $dataTable->table(['class' => 'table table-bordered table-responsive data-table',
                        'id' => 'tableProductCategory', 'style' => 'width:100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btn-delete', function (e) {
                var canDelete = $(this).data('can-delete');
                if (canDelete === 'no') {
                    alert('ไม่สามารถลบได้มีการเรียกใช้ Product Category นี้อยู่');
                    return false;
                }
                else {
                    if (!confirm('ยืนยันการลบข้อมูล')) {
                        e.preventDefault();
                        return false;
                    }
                    var param = $(this).data('url');
                    var method_type = 'DELETE';

                    if ($(this).data('method-type')) {
                        method_type = $(this).data('method-type');
                    }
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        dataType: 'json',
                        url: param,
                        type: method_type,
                        _token: '{{ csrf_field() }}',

                    }).done(function (data) {
                        alert(data.message);
                        $('.data-table').DataTable().draw();
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                    });
                    return true;
                }
            });

        });
    </script>
    {!! $dataTable->scripts() !!}

@endpush
