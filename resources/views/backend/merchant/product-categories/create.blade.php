@extends('backend.layouts.main_dashboard')
@section('title', 'Create Product Category')
@section('title_header' , 'Create Product Category')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('product-categories.store') }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>ชื่อ <span style="color:red">*</span></label>
                    <input type="text" class="form-control"
                           placeholder="Name" name="title" id="inputName"
                           value="{{ old('title') }}">
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('product-categories.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection
