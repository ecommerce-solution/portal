@extends('backend.layouts.main_dashboard')
@section('title', 'Employee List')
@section('title_header' , 'History')
@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="table table-hover">
                        {!! $dataTable->table(['class' => 'table table-bordered table-responsive data-table',
                        'id' => 'tableEmployee', 'style' => 'width:100%']) !!}
                    </div>
                    <div class="modal fade" id="modal-default">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Payment</h4>
                                </div>
                                <div class="modal-body" style="margin-bottom: 10px;">
                                    <div class="step-1 hide-modal">
                                        <div>
                                            <div><span class="float-left" id="bill-name"></span>
                                                <div>
                                                    <table class="table" cellspacing="0" border="0">
                                                        <thead>
                                                        <tr>
                                                            <th><em>#</em></th>
                                                            <th>Product</th>
                                                            <th>Quantity</th>
                                                            <th>SubTotal</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="table-tbody">
                                                        </tbody>
                                                    </table>
                                                    <table class="table" cellspacing="0" border="0"
                                                           style="margin-bottom:8px;">
                                                        <tbody>
                                                        <tr>
                                                            <td style="text-align:left;">Total Items
                                                            </td>
                                                            <td id="table-total-quantity"
                                                                style="text-align:right; padding-right:1.5%;">
                                                            </td>
                                                            <td style="text-align:left; padding-left:1.5%;">
                                                                Total
                                                            </td>
                                                            <td id="table-total-price"
                                                                style="text-align:right;font-weight:bold;">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left"
                                            data-dismiss="modal">Close
                                    </button>
                                    <button type="button" class="step-1 btn btn-primary" id="btn-step-1" disabled>
                                        Next
                                    </button>
                                    <button type="button" class="step-2 btn btn-primary hide-modal" disabled
                                            id="btn-step-2">
                                        Next
                                    </button>
                                    <button type="button" class="step-3 btn btn-primary hide-modal"
                                            data-dismiss="modal" id="btn-step-3">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btn-delete', function (e) {
                var canDelete = $(this).data('can-delete');
                if (canDelete === 'no') {
                    alert('ไม่สามารถลบได้มีการเรียกใช้ Product Category นี้อยู่');
                    return false;
                }
                else {
                    if (!confirm('ยืนยันการลบข้อมูล')) {
                        e.preventDefault();
                        return false;
                    }
                    var param = $(this).data('url');
                    var method_type = 'DELETE';

                    if ($(this).data('method-type')) {
                        method_type = $(this).data('method-type');
                    }
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        dataType: 'json',
                        url: param,
                        type: method_type,
                        _token: '{{ csrf_field() }}',

                    }).done(function (data) {
                        alert(data.message);
                        $('.data-table').DataTable().draw();
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                    });
                    return true;
                }
            });

        });
    </script>
    {!! $dataTable->scripts() !!}

@endpush
