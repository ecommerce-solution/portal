<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <a href="{{ route('users.edit',auth()->user()->id) }}">
                    {{--<img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle"--}}
                         {{--style="height: 45px;width: 45px">--}}
                    <img src="{{ asset('adminlte/dist/img/avatar5.png') }}" class="img-circle"
                         style="height: 45px;width: 45px">
                </a>
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->nickname }}</p>
                <a href="{{ route('users.edit',auth()->user()->id) }}"><i
                            class="fa fa-circle text-success"></i> Edit Profile</a>
            </div>
        </div>
    @php($routeName = explode(".",Route::currentRouteName()))
    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Route::currentRouteName() === 'homepage' ? 'active' : null }}"><a
                        href="{{ route('homepage') }}"><i class="fa fa-home "></i> <span>Home</span></a></li>
            @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 )
                <li class="treeview
             {{ $routeName[0] === 'categories' || $routeName[0] === 'contact-us' || $routeName[0] === 'banners'
             || $routeName[0] === 'articles' || $routeName[0] === 'holidays' || $routeName[0] === 'careers'
             || $routeName[0] === 'application-form' || $routeName[0] === 'clients' || $routeName[0] === 'our-businesses'
             || $routeName[0] === 'our-services' || $routeName[0] === 'about-us' || $routeName[0] === 'contacts'
             || $routeName[0] === 'roles'
             ? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Portal</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(auth()->user()->role_id === 1)
                            <li class="{{ Route::currentRouteName() ==='roles.list' || Route::currentRouteName() === 'roles.create'
                                ||Route::currentRouteName() === 'roles.edit'? 'active' : null }}"><a
                                        href="{{ route('roles.list') }}"><i class="fa fa-user-circle"></i>
                                    <span>Roles </span></a>
                            </li>
                        @endif
                        <li class="{{ Route::currentRouteName() ==='categories.list' || Route::currentRouteName() === 'categories.create'
                                ||Route::currentRouteName() === 'categories.edit'? 'active' : null }}"><a
                                    href="{{ route('categories.list') }}"><i class="fa fa-th-large"></i>
                                <span>Category </span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'banners.list' || Route::currentRouteName() === 'banners.create'
                                ||Route::currentRouteName() === 'banners.edit' ? 'active' : null }}">
                            <a href="{{ route('banners.list') }}"><i class="fa fa-bullhorn"></i>
                                <span>Banner</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'articles.list' || Route::currentRouteName() === 'articles.create'
                                ||Route::currentRouteName() === 'articles.edit' ? 'active' : null }}"><a
                                    href="{{ route('articles.list') }}"><i class="fa fa-newspaper-o"></i>
                                <span>Article</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'holidays.list' || Route::currentRouteName() === 'holidays.create'
                                || Route::currentRouteName() === 'holidays.edit' ? 'active' : null }}"><a
                                    href="{{ route('holidays.list') }}"><i class="fa fa-calendar-check-o"></i>
                                <span>Holiday</span></a>
                        </li>
                        <li class="treeview {{ $routeName[0] === 'careers' || $routeName[0] === 'application-form' ? 'active' : null }}">
                            <a href="#">
                                <i class="fa fa-drivers-license" aria-hidden="true"></i> <span>Recruit</span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Route::currentRouteName() === 'careers.list' || Route::currentRouteName() === 'careers.create'
                                         || Route::currentRouteName() === 'careers.edit' ? 'active' : null }}"><a
                                            href="{{ route('careers.list') }}"><i
                                                class="fa fa-gavel"></i><span>Career List</span></a>
                                </li>
                                <li class="{{ Route::currentRouteName() === 'application-form.list'
                                        || Route::currentRouteName() === 'application-form.detail' ? 'active' : null }}">
                                    <a href="{{ route('application-form.list') }}"><i class="fa fa-file-text-o"></i>
                                        <span>Application Form</span></a></li>
                            </ul>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'clients.list' || Route::currentRouteName() === 'clients.create'
                                 || Route::currentRouteName() === 'clients.edit' ? 'active' : null }}"><a
                                    href="{{ route('clients.list') }}"><i class="fa fa-exchange"></i>
                                <span>Client</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'our-businesses.list'
                                || Route::currentRouteName() === 'our-businesses.create'
                                || Route::currentRouteName() === 'our-businesses.edit' ? 'active' : null }}"><a
                                    href="{{ route('our-businesses.list') }}"><i class="fa fa-briefcase"></i>
                                <span>Our Business</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'our-services.list'
                                || Route::currentRouteName() === 'our-services.create'
                                || Route::currentRouteName() === 'our-services.edit' ? 'active' : null }}"><a
                                    href="{{ route('our-services.list') }}"><i class="fa fa-window-restore"></i>
                                <span>Our Service</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'about-us.index' ? 'active' : null }}"><a
                                    href="{{ route('about-us.index') }}"><i class="fa fa-users"></i>
                                <span>About Us</span></a></li>
                        <li class="{{ Route::currentRouteName() === 'contact-us.create' ? 'active' : null }}"><a
                                    href="{{ route('contact-us.create') }}"><i class="fa fa-phone"></i>
                                <span>Contact Us</span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() === 'contacts.list'
                                || Route::currentRouteName() === 'contacts.detail' ? 'active' : null }}">
                            <a href="{{ route('contacts.list') }}"><i class="fa fa-envelope"></i>
                                <span>Contact Form</span></a>
                        </li>
                    </ul>
                </li>
                @if(auth()->user()->role_id === 1)
                    <li class="{{ Route::currentRouteName() === 'users.list' || Route::currentRouteName() === 'users.create'
                         || Route::currentRouteName() === 'users.edit' ? 'active' : null }}">
                        <a href="{{ route('users.list') }}"><i class="glyphicon glyphicon-user"></i>
                            <span>User</span></a>
                    </li>
                @endif
            @endif
            @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 4 )
                <li class="treeview
             {{ $routeName[0] === 'products' || $routeName[0] === 'product-categories' || $routeName[0] === 'balances' || $routeName[0] === 'orders'
             || $routeName[0] === 'reports'? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-suitcase"></i> <span>Merchant</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Route::currentRouteName() ==='product-categories.list' || Route::currentRouteName() === 'product-categories.create'
                                ||Route::currentRouteName() === 'product-categories.edit'? 'active' : null }}"><a
                                    href="{{ route('product-categories.list') }}"><i class="fa fa-th-list"></i>
                                <span>Category </span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() ==='products.list' || Route::currentRouteName() === 'products.create'
                                ||Route::currentRouteName() === 'products.edit'? 'active' : null }}"><a
                                    href="{{ route('products.list') }}"><i class="fa fa-glass"></i>
                                <span>Product </span></a>
                        </li>
                        <li class="{{ Route::currentRouteName() ==='orders.list' || Route::currentRouteName() === 'orders.detail'
                                ||Route::currentRouteName() === 'orders.edit'? 'active' : null }}"><a
                                    href="{{ route('orders.list') }}"><i class="fa fa-shopping-cart"></i>
                                <span>Order </span></a>
                        </li>
                        {{--<li class="{{ Route::currentRouteName() ==='reports.selectProduct'? 'active' : null }}"><a--}}
                                    {{--href="{{ route('reports.selectProduct') }}"><i class="fa fa-files-o"></i>--}}
                                {{--<span>Report </span></a>--}}
                        {{--</li>--}}
                        <li class="treeview {{ $routeName[0] === 'reports' ? 'active' : null }}">
                            <a href="#">
                                <i class="fa fa-files-o" aria-hidden="true"></i> <span>Report</span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Route::currentRouteName() === 'reports.user'  ? 'active' : null }}"><a
                                            href="{{ route('reports.user') }}"><i
                                                class="fa fa-file-archive-o"></i><span>User Report</span></a>
                                </li>
                                <li class="{{ Route::currentRouteName() === 'reports.stock'
                                        || Route::currentRouteName() === 'reports.stock' ? 'active' : null }}">
                                    <a href="{{ route('reports.stock') }}"><i class="fa fa-file-text-o"></i>
                                        <span>Stock</span></a></li>
                                <li class="{{ Route::currentRouteName() === 'reports.product'
                                        || Route::currentRouteName() === 'reports.product' ? 'active' : null }}">
                                    <a href="{{ route('reports.product') }}"><i class="fa fa-file-code-o "></i>
                                        <span>Product Report</span></a></li>
                                <li class="{{ Route::currentRouteName() === 'reports.order'
                                        || Route::currentRouteName() === 'reports.order' ? 'active' : null }}">
                                    <a href="{{ route('reports.order') }}"><i class="fa fa-file-text"></i>
                                        <span>Order Report</span></a></li>
                            </ul>
                        </li>
                        {{--<li class="{{ Route::currentRouteName() ==='balances.list' || Route::currentRouteName() === 'balances.detail'--}}
                                {{--||Route::currentRouteName() ==='balances.list'? 'active' : null }}"><a--}}
                                {{--href="{{ route('balances.list') }}"><i class="fa fa-money"></i>--}}
                                {{--<span>Balance </span></a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
            @endif
            <li class="{{ Route::currentRouteName() ==='purchase-histories.list' || Route::currentRouteName() === 'purchase-histories.detail'
                                ? 'active' : null }}"><a
                        href="{{ route('purchase-histories.list') }}"><i class="fa fa-list-alt"></i>
                    <span>Purchase History </span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
