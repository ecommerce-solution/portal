<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('homepage') }}" class="logo">
        <span class="logo-mini"><b>E</b>CS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>E</b>CS <b>P</b>ortal</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('logout') }}" type="submit">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
