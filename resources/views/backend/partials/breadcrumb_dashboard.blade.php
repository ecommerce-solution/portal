<section class="content-header">
    <h1>
        @yield('title_header')
    </h1>
    {{--<ol class="breadcrumb">--}}
    {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class=""><a href="">{{ $breadcrumb['title'] }}</a></li>--}}
    {{--@if(isset($breadcrumb['title']))--}}
    {{--<li class="active"> {{ $breadcrumb['title'] }}</li>--}}
    {{--@endif--}}
    {{--</ol>--}}
</section>