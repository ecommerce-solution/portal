<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery 3 -->
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<!-- Ckeditor -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<!-- Datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datepicker/moment/min/moment.min.js') }}"></script>
<!-- Date Time Picker -->
<script type="text/javascript"
        src="{{ asset('datepicker/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('js/data-tables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/data-tables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/data-tables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
<!-- Bootstrap Select-->
<script src="{{ asset('bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<!-- Custom JS -->
<script src=" {{ asset('js/custom/custom.js') }} "></script>
<!-- Cleave.js -->
<script src="{{ asset('cleave.js/dist/cleave.min.js') }}"></script>
<script src="{{ asset('cleave.js/dist/addons/cleave-phone.th.js') }}"></script>
<!-- Sweet Alert -->
<script src="{{ asset('sweetalert/dist/sweetalert.min.js') }}"></script>
<!-- Time Limit Alert -->
<script>
    $('document').ready(function () {
        var duration = 4000; // 4 seconds
        setTimeout(function () {
            $('.alert').hide();
        }, duration);
    });
</script>



