<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_token" content="{{ csrf_token() }}">
    <title> @yield('title') </title>
    <link rel="icon" href="{{ asset('img/title-icon.jpg') }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet"
          href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Date Time Picker -->
    <link rel="stylesheet"
          href="{{ asset('datepicker/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="{{ asset('bootstrap-select/dist/css/bootstrap-select.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/custom/custom.css') }} ">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('css/data-tables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('css/data-tables/buttons.dataTables.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('backend.partials.navbar_dashboard')
    @include('backend.partials.sidebar_dashboard')
    <div class="content-wrapper">
        @include('backend.partials.alert_success')
        @include('backend.partials.alert_error')
        @include('backend.partials.breadcrumb_dashboard')
        <section class="content" style="min-height: 901px;">
            @yield('content')
        </section>
    </div>
    @include('backend.partials.footer_dashboard')
</div>
@include('backend.layouts.script')
@stack('script')
</body>
</html>
