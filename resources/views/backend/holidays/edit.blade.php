@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Holiday')
@section('title_header' , 'Edit Holiday')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('holidays.update',[$holiday->id]) }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="{{ $holiday->title }}">
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Description" name="description"
                           value="{{ $holiday->description }}">
                </div>
                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                    <label>Start date <span style="color:red">*</span></label>
                    <div class="input-group date" id="start_date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="start_date"
                               value="{{ $list['start_date'] }}" placeholder="DD/MM/YYYY"/>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('holidays.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script type="text/javascript">
        $(function () {
            $('#start_date').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom',
                },
                showClear: true,
            });
        });
    </script>

@endpush
