@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Career')
@section('title_header' , 'Edit Career')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('careers.update',[$career->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('role_name') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="role_name"
                           value="{{ $career->role_name }}">
                </div>
                <div class="form-group {{ $errors->has('icon') ? 'has-error' : '' }}">
                    <label>Icon <span style="color:red">*</span></label>
                    <div class="form-group">
                        <a id="linkCareer" href="{{ asset('uploads/'. $career->icon) }}" target="blank">
                            <img id="previewCareer" src="{{ asset('uploads/'. $career->icon) }}">
                        </a>
                        <div><input id="buttonCareer" type="button" value="Clear" onclick="clearCareer()"
                                    style="display:none"></div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" id="fileCareer"
                           onchange="readCareer(this);" name="icon">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea type="text" class="form-control" placeholder="Short Description"
                              name="short_description" rows="4">{{ $career->short_description }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea rows="7" class="form-control ckeditor"
                              placeholder="Description..."
                              name="description">{{ $career->description }}</textarea></div>
                <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
                    <label>Position <span style="color:red">*</span></label>
                    <input type="text" onkeypress="check_number()" class="form-control" placeholder="Position" name="position"
                           value="{{ $career->position }}">
                </div>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="Open" @if ($career->status == "Open") selected @endif >Open</option>
                        <option value="Close" @if ($career->status == "Close") selected @endif >Close</option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('careers.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readCareer(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewCareer').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkCareer').removeAttr('href');
                $('#buttonCareer').show();
            }
        }

        function clearCareer() {
            $('#fileCareer').val(null);
            $('#previewCareer').attr('src', "{{ asset('uploads/'. $career->icon) }}");
            $('#linkCareer').attr('href', "{{ asset('uploads/'. $career->icon) }}");
            $('#buttonCareer').hide();
        }

        function check_number() {
            e_k=event.keyCode

            if (e_k != 13 && (e_k < 48) || (e_k > 57)) {
                event.returnValue = false;
            }
        }
    </script>

@endpush
