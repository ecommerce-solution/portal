@extends('backend.layouts.main_dashboard')
@section('title', 'Career  List')
@section('title_header' , 'Career List')
@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="float-md-right">
                        <a href="{{ route('careers.create') }}" class="btn btn-success btn-sm"><i
                                    class="fa fa-plus"></i>
                            Create Career
                        </a>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div class="table table-hover">
                            {!! $dataTable->table(['class' => 'table table-bordered table-responsive data-table',
                            'id' => 'tableCareer', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                $(document).on('click', '.btn-delete', function (e) {
                    e.preventDefault();
                    swal({
                        text: "ยืนยันการลบข้อมูล",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                var param = $(this).data('url');
                                var method_type = 'DELETE';

                                if ($(this).data('method-type')) {
                                    method_type = $(this).data('method-type');
                                }
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                    },
                                    dataType: 'json',
                                    url: param,
                                    type: method_type,
                                    _token: '{{ csrf_field() }}',

                                }).done(function (data) {
                                    swal(data.message, {
                                        icon: "success",
                                    });
                                    $('.data-table').DataTable().draw();
                                });
                            }
                        });
                });
            })

        });
    </script>
    {!! $dataTable->scripts() !!}

@endpush

