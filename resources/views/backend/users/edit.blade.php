@extends('backend.layouts.main_dashboard')
@section('title') {{ ($user->id == auth()->user()->id) ?'Edit Profile' :' Edit User' }} @endsection
@section('title_header') {{ ($user->id == auth()->user()->id) ?'Edit Profile' :' Edit User' }} @endsection
@section('content')

    <div class="box box-primary">
        <form action="{{ route('users.update',[$user->id]) }}" method="post">
            @csrf
            <section class="content">
                <div class="row">
                    <div class="col-md-5">
                        <div class="box-header with-border">
                            <h3 class="box-title">User</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label>Email <span style="color:red">*</span></label>
                                <input type="email" class="form-control" placeholder="Email"
                                       value="{{ $user->email }}" name="email">
                            </div>
                            <div class="form-group {{ $errors->has('id_card') ? 'has-error' : '' }}">
                                <label>ID Card <span style="color:red">*</span></label>
                                <input type="text" class="form-control" placeholder="ID Card"
                                       value="{{ $user->id_card }}" name="id_card" id="id_card">

                            </div>
                            @if(auth()->user()->role_id == 1)
                                <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
                                    <label>Role <span style="color:red">*</span></label>
                                    <select class="form-control" name="role_id">
                                        <option selected disabled>กรุณาเลือก</option>
                                        @foreach ($roles as $role)
                                            <option {{ ($user->role_id == $role->id) ? 'selected' : '' }}
                                                    value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profile</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label>First Name <span style="color:red">*</span></label>
                                <input type="text" name="first_name" class="form-control"
                                       placeholder="First Name" value="{{ $user->first_name }}">
                            </div>
                            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <label>Last Name <span style="color:red">*</span></label>
                                <input type="text" name="last_name" class="form-control"
                                       placeholder="Last Name" value="{{ $user->last_name }}">
                            </div>
                            <div class="form-group {{ $errors->has('nickname') ? 'has-error' : '' }}">
                                <label>Nickname <span style="color:red">*</span></label>
                                <div>
                                    <input type="text" name="nickname" class="form-control"
                                           placeholder="Nickname" value="{{ $user->nickname }}">
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('birth_date') ? 'has-error' : '' }}">
                                <label>Birth Date </label>
                                <div class="input-group date" id="birth_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="birth_date"
                                           value="{{ date('d/m/Y', strtotime($user->birth_date)) }}"
                                           placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number </label>
                                <input type="text" class="form-control" placeholder="Phone Number"
                                       name="phone_number" id="phone_number" value="{{ $user->phone_number }}">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="box-header with-border">
                            <h3 class="box-title">Reset Password</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password"
                                       name="password">
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label>Password Confirm</label>
                                <input type="password" class="form-control"
                                       placeholder="Password Confirm" name="password_confirmation">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box-header with-border">
                            <h3 class="box-title">Company Requirement</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('blood_type') ? 'has-error' : '' }}">
                                <label>Blood Type </label>
                                <select class="form-control" name="blood_type">
                                    <option selected disabled>กรุณาเลือก</option>
                                    <option {{ ('A' == $user->blood_type) ? 'selected' : '' }} value="A">A</option>
                                    <option {{ ('B' == $user->blood_type) ? 'selected' : '' }} value="B">B</option>
                                    <option {{ ('O' == $user->blood_type) ? 'selected' : '' }} value="O">O</option>
                                    <option {{ ('AB'== $user->blood_type) ? 'selected' : '' }} value="AB">AB</option>
                                </select>
                            </div>
                            <div class="form-group {{ $errors->has('bank_account_no') ? 'has-error' : '' }}">
                                <label>Bank Account *K Plus </label>
                                <input type="text" name="bank_account_no" class="form-control" id="bank_account_no"
                                       placeholder="Bank Account" value="{{ $user->bank_account_no }}">
                            </div>
                            <div class="form-group {{ $errors->has('bank_branch') ? 'has-error' : '' }}">
                                <label>Bank Branch *K Plus </label>
                                <input type="text" name="bank_branch" class="form-control"
                                       placeholder="Bank Branch" value="{{ $user->bank_branch }}">
                            </div>
                            <div class="form-group {{ $errors->has('start_work_date') ? 'has-error' : '' }}">
                                <label>Start Work </label>
                                <div class="input-group date" id="start_work_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="start_work_date"
                                           value="{{ date('d/m/Y', strtotime($user->start_work_date)) }}"
                                           placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('users.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script type="text/javascript">
        $(function () {
            $('#birth_date,#start_work_date').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
                showClear: true,
            });
        });

        new Cleave('#id_card', {
            blocks: [1, 4, 5, 2, 1],
            numericOnly: true,
        });

        new Cleave('#phone_number', {
            phone: true,
            delimiter: ' ',
            phoneRegionCode: 'TH'
        });

        new Cleave('#bank_account_no', {
            blocks: [3, 1, 5, 5, 1],
            delimiter: ' ',
            numericOnly: true,
        });
    </script>

@endpush
