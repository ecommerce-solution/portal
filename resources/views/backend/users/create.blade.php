@extends('backend.layouts.main_dashboard')
@section('title', 'Create User')
@section('title_header' , 'Create User')
@section('content')

    <div class="box box-primary">
        <form action="{{ route('users.store') }}" method="post">
            @csrf
            <section class="content">
                <div class="row">
                    <div class="col-md-5">
                        <div class="box-header with-border">
                            <h3 class="box-title">User</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label>Email <span style="color:red">*</span></label>
                                <input type="email" class="form-control" placeholder="Email"
                                       value="{{ old("email") }}" name="email">
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label>Password <span style="color:red">*</span></label>
                                <input type="password" class="form-control" placeholder="Password"
                                       name="password">
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label>Password Confirm <span style="color:red">*</span></label>
                                <input type="password" class="form-control"
                                       placeholder="Password Confirm" name="password_confirmation">
                            </div>
                            <div class="form-group {{ $errors->has('id_card') ? 'has-error' : '' }}">
                                <label>ID Card <span style="color:red">*</span></label>
                                <input type="text" class="form-control" placeholder="ID Card"
                                       value="{{ old("id_card") }}" name="id_card" id="id_card">
                            </div>
                            <div class="form-group">
                                <label>Role </label>
                                <select class="form-control" name="role_id">
                                    <option selected disabled>กรุณาเลือก</option>
                                    @foreach ($roles as $role)
                                        <option
                                            {{ (old("role_id") == $role->id ? "selected":"") }} value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profile</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label>First Name <span style="color:red">*</span></label>
                                <input type="text" name="first_name" class="form-control"
                                       placeholder="First Name" value="{{ old("first_name") }}">
                            </div>
                            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <label>Last Name <span style="color:red">*</span></label>
                                <input type="text" name="last_name" class="form-control"
                                       placeholder="Last Name" value="{{ old("last_name") }}">
                            </div>
                            <div class="form-group {{ $errors->has('nickname') ? 'has-error' : '' }}">
                                <label>Nickname <span style="color:red">*</span></label>
                                <div>
                                    <input type="text" name="nickname" class="form-control"
                                           placeholder="Nickname" value="{{ old("nickname") }}">
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('birth_date') ? 'has-error' : '' }}">
                                <label>Birth Date </label>
                                <div class="input-group date" id="birth_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="birth_date"
                                           value="{{ old('birth_date') }}" placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" placeholder="Phone Number"
                                       name="phone_number" id="phone_number" value="{{ old("phone_number") }}">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <div class="box-header with-border">
                            <h3 class="box-title">Company Requirement</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('blood_type') ? 'has-error' : '' }}">
                                <label>Blood Type </label>
                                <select class="form-control" name="blood_type">
                                    <option selected disabled>กรุณาเลือก</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="O">O</option>
                                    <option value="AB">AB</option>
                                </select>
                            </div>
                            <div class="form-group {{ $errors->has('bank_account_no') ? 'has-error' : '' }}">
                                <label>Bank Account *K Plus</label>
                                <input type="text" name="bank_account_no" class="form-control" id="bank_account_no"
                                       placeholder="Bank Account" value="{{ old("bank_account_no") }}">

                            </div>
                            <div class="form-group {{ $errors->has('bank_branch') ? 'has-error' : '' }}">
                                <label>Bank Branch *K Plus</label>
                                <input type="text" name="bank_branch" class="form-control"
                                       placeholder="Bank Branch" value="{{ old("bank_branch") }}">
                            </div>
                            <div class="form-group {{ $errors->has('start_work_date') ? 'has-error' : '' }}">
                                <label>Start Work </label>
                                <div class="input-group date" id="start_work_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="start_work_date"
                                           value="{{ old('start_work_date') }}" placeholder="DD/MM/YYYY"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('users.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script type="text/javascript">
        $(function () {
            $('#birth_date,#start_work_date').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true,
                allowInputToggle: true,
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                },
                showClear: true,
            });
        });

        new Cleave('#id_card', {
            blocks: [1, 4, 5, 2, 1],
            numericOnly: true,
        });

        new Cleave('#phone_number', {
            phone: true,
            delimiter: ' ',
            phoneRegionCode: 'TH'
        });

        new Cleave('#bank_account_no', {
            blocks: [3, 1, 5, 5, 1],
            delimiter: ' ',
            numericOnly: true,
        });
    </script>

@endpush
