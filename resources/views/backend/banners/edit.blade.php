@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Banner')
@section('title_header' , 'Edit Banner')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('banners.update',[$banner->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="{{ $banner->title }}">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control"
                              name="short_description"
                              placeholder="Short Description" rows="4">{{ $banner->short_description }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <a id="linkBanner" href="{{ asset('uploads/'. $banner->image) }}" target="blank">
                            <img id="previewBanner" src="{{ asset('uploads/'. $banner->image) }}"></a>
                        <div><input type="button" value="Clear" onclick="clearPreview()" id="buttonClear"
                                    style="display: none"></div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readBanner(this);" name="image" id="fileBanner">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('banners.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readBanner(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewBanner').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkBanner').removeAttr('href');
                $('#buttonClear').show();
            }
        }

        function clearPreview() {
            $('#fileBanner').val(null);
            $('#previewBanner').attr('src', "{{ asset('uploads/'. $banner->image) }}");
            $('#linkBanner').attr('href', "{{ asset('uploads/'. $banner->image) }}");
            $('#buttonClear').hide();
        }

    </script>

@endpush


