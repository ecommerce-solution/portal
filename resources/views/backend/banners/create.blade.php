@extends('backend.layouts.main_dashboard')
@section('title', 'Create Banner')
@section('title_header' , 'Create Banner')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('banners.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="{{ old('title') }}">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" placeholder="Short Description" name="short_description" rows="4"
                              value="{{ old('short_description') }}"></textarea>
                </div>
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none " id="divShowImg">
                            <img id="previewBanner">
                            <div><input type="button" value="Clear" onclick="clearBanner()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readBanner(this);" id="fileBanner"
                           name="image">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB <br>
                    </p>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('banners.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fa fa-floppy-o"></i> Save
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readBanner(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewBanner').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clearBanner() {
            $('#divShowImg').hide();
            $('#previewBanner').attr('src', '');
            $('#fileBanner').val(null);
        }

    </script>

@endpush
