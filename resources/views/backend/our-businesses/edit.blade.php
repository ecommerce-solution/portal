@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Our Business')
@section('title_header' , 'Edit Our Business')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('our-businesses.update',[$ourBusiness->id]) }}" method="post"
              enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="{{ $ourBusiness->title }}">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description<span style="color:red">*</span></label>
                    <textarea class="form-control" placeholder="Short Description"
                              name="short_description" rows="4">{{ $ourBusiness->short_description }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea id="messageContent" rows="7" class="form-control ckeditor"
                              placeholder="Description"
                              name="description">{{ $ourBusiness->description }}
                    </textarea>
                </div>
                <div class="form-group {{ $errors->has('cover_image') ? 'has-error' : '' }}">
                    <label>Cover Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div id="divShowCover">
                            <a id="linkCover" href="{{ asset('uploads/'. $ourBusiness->cover_image) }}" target="blank">
                                <img id="previewOurBusinessCover"
                                     src="{{ asset('uploads/'. $ourBusiness->cover_image) }}"></a>
                            <div><input id="buttonCover" type="button" value="Clear" onclick="clearCover()"
                                        style="display:none"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readCover(this);" name="cover_image" id="fileCover">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('thumbnail_image') ? 'has-error' : '' }}">
                    <label>Thumbnail Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div id="divShowThumbnail">
                            <a id="linkThumbnail" href="{{ asset('uploads/'. $ourBusiness->thumbnail_image) }}"
                               target="blank">
                                <img id="previewOurBusinessThumbnail"
                                     src="{{ asset('uploads/'. $ourBusiness->thumbnail_image) }}"></a>
                            <div><input id="buttonThumbnail" type="button" value="Clear" onclick="clearThumbnail()"
                                        style="display:none"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readThumbnail(this);" name="thumbnail_image" id="fileThumbnail">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Url </label>
                    <input type="text" class="form-control" placeholder="Url" name="url"
                           value="{{ $ourBusiness->url }}">
                </div>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="DRAFT" @if ($ourBusiness->status == "DRAFT") selected @endif >DRAFT</option>
                        <option value="PUBLISHED" @if ($ourBusiness->status == "PUBLISHED") selected @endif >PUBLISHED
                        </option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('our-businesses.list') }}" class="btn btn-danger btn-sm"><i
                            class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script>
        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurBusinessCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkCover').removeAttr('href');
                $('#buttonCover').show();
            }
        }

        function readThumbnail(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurBusinessThumbnail').attr('src', e.target.result);
                    $('#divShowThumbnail').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkThumbnail').removeAttr('href');
                $('#buttonThumbnail').show();
            }
        }

        function clearCover() {
            $('#fileCover').val(null);
            $('#previewOurBusinessCover').attr('src', "{{ asset('uploads/'. $ourBusiness->cover_image) }}");
            $('#linkCover').attr('href', "{{ asset('uploads/'. $ourBusiness->cover_image) }}");
            $('#buttonCover').hide();
        }

        function clearThumbnail() {
            $('#fileThumbnail').val(null);
            $('#previewOurBusinessThumbnail').attr('src', "{{ asset('uploads/'. $ourBusiness->thumbnail_image) }}");
            $('#linkThumbnail').attr('href', "{{ asset('uploads/'. $ourBusiness->thumbnail_image) }}");
            $('#buttonThumbnail').hide();
        }

    </script>
@endpush
