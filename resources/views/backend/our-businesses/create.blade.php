@extends('backend.layouts.main_dashboard')
@section('title', 'Create Our Business')
@section('title_header' , 'Create Our Business')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('our-businesses.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title" id="inputTitle"
                           value="{{ old('title') }}">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" id="inputShortDescription" placeholder="Short Description"
                              name="short_description" rows="4">{{ old('short_description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea id="inputDescription" name="description">{{ old('description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('cover_image') ? 'has-error' : '' }}">
                    <label>Cover Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none " id="divShowCover">
                            <img id="previewOurBusinessCover">
                            <div><input type="button" value="Clear" onclick="clearCover()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readCover(this);" name="cover_image" id="fileCover">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group {{ $errors->has('thumbnail_image') ? 'has-error' : '' }}">
                    <label>Thumbnail Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none " id="divShowThumbnail">
                            <img id="previewOurBusinessThumbnail">
                            <div><input type="button" value="Clear" onclick="clearThumbnail()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readThumbnail(this);" name="thumbnail_image" id="fileThumbnail">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label for="inputName">Url </label>
                    <input type="text" class="form-control" placeholder="Url" name="url" id="inputName"
                           value="{{ old('url') }}">
                </div>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="PUBLISHED">PUBLISHED</option>
                        <option value="DRAFT">DRAFT</option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('our-businesses.list') }}" class="btn btn-danger btn-sm">
                    <i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script>
        $(function () {
            $('#inputPublishDate').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                ignoreReadonly: true,
                allowInputToggle: true,
                showClose: true,
            });
        });

        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurBusinessCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readThumbnail(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurBusinessThumbnail').attr('src', e.target.result);
                    $('#divShowThumbnail').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clearCover() {
            $('#divShowCover').hide();
            $('#previewOurBusinessCover').attr('src', '');
            $('#fileCover').val(null);
        }

        function clearThumbnail() {
            $('#divShowThumbnail').hide();
            $('#previewOurBusinessThumbnail').attr('src', '');
            $('#fileThumbnail').val(null);
        }

        CKEDITOR.replace('inputDescription');
    </script>

@endpush
