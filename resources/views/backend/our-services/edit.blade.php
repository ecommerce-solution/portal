@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Our Service')
@section('title_header' , 'Edit Our Service')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('our-services.update',[$ourService->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title" id="inputTitle"
                           value="{{  $ourService->title }}">
                </div>
                <div class="form-group">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" id="inputShortDescription" placeholder="Short Description"
                              name="short_description" rows="4">{{  $ourService->short_description }}</textarea>
                </div>
                <div class="form-group">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea id="inputDescription" name="description">{{  $ourService->description }}</textarea>
                </div>
                <div class="form-group">
                    <label> Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div id="divShowCover">
                            <a id="linkImage" href="{{ asset('uploads/'. $ourService->image) }}" target="blank">
                                <img class="item-image" id="previewOurServiceCover"
                                     src="{{ asset('uploads/'. $ourService->image) }}"></a>
                            <div><input id="buttonImage" type="button" value="Clear" onclick="clearCover()"
                                        style="display:none"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readCover(this);" name="image" id="fileImage">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="OPEN" @if ($ourService->status == "OPEN") selected @endif >OPEN</option>
                        <option value="CLOSE" @if ($ourService->status == "CLOSE") selected @endif >CLOSE
                        </option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('our-services.list') }}" class="btn btn-danger btn-sm">
                    <i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurServiceCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkImage').removeAttr('href');
                $('#buttonImage').show();
            }
        }

        function clearCover() {
            $('#fileImage').val(null);
            $('#previewOurServiceCover').attr('src', "{{ asset('uploads/'. $ourService->image) }}");
            $('#linkImage').attr('href', "{{ asset('uploads/'. $ourService->image) }}");
            $('#buttonImage').hide();
        }

        CKEDITOR.replace('inputDescription');
    </script>

@endpush
