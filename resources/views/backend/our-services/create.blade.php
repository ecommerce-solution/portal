@extends('backend.layouts.main_dashboard')
@section('title', 'Create Our Service')
@section('title_header' , 'Create Our Service')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('our-services.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="title" id="inputTitle"
                           value="{{ old('title') }}">
                </div>
                <div class="form-group">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" id="inputShortDescription" placeholder="Short Description"
                              name="short_description" rows="4">{{ old('short_description') }}</textarea>
                </div>
                <div class="form-group">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea id="inputDescription" name="description">{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div style="display: none " id="divShowCover">
                            <img class="item-image" id="previewOurServiceCover">
                            <div><input type="button" value="Clear" onclick="clearCover()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png"
                           onchange="readCover(this);" name="image" id="fileCover">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Status <span style="color:red">*</span></label>
                    <select name="status" class="form-control">
                        <option value="OPEN">OPEN</option>
                        <option value="CLOSE">CLOSE</option>
                    </select>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('our-services.list') }}" class="btn btn-danger btn-sm">
                    <i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script>

        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewOurServiceCover').attr('src', e.target.result);
                    $('#divShowCover').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clearCover() {
            $('#divShowCover').hide();
            $('#previewOurServiceCover').attr('src', '');
            $('#fileCover').val(null);
        }

        CKEDITOR.replace('inputDescription');
    </script>

@endpush
