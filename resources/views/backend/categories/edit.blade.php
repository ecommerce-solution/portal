@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Category')
@section('title_header' , 'Edit Category')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('categories.update',[$category->id]) }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="name"
                           value="{{ $category->name }}">
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label>Slug <span style="color:red"></span></label>
                    <input type="text" class="form-control" placeholder="Slug" name="slug"
                           value="{{ $category->slug }}">
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('categories.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Save</button>
            </div>
        </form>
    </div>

@endsection
