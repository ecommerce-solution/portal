@extends('backend.layouts.main_dashboard')
@section('title', 'Homepage')
@section('content')
    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 4 )
        <section class="content-header">
            <h1>Merchant</h1>
        </section>
    @endif
        <section class="content" >
            @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 4 )
            <div class="row">
                <div class="box-body col-lg-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow">
                            <img src="{{ asset('img/member.png') }}" style="height: 60px;width: 60px">
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Members</span>
                            <span class="info-box-number">{{count($users)}}</span>
                        </div>
                    </div>
                </div>
                <div class="box-body col-lg-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua">
                            <img src="{{ asset('img/pro.png') }}" style="height: 40px;width: 40px;">
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Product</span>
                            <span class="info-box-number">{{count($products)}}</span>
                        </div>
                    </div>
                </div>
                <div class="box-body col-lg-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-green">
                            <img src="{{ asset('img/product.png') }}" style="height: 40px;width: 40px;">
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sales Today</span>
                            <?php $total = 0; ?>
                            @foreach($sumtodays as $sumtoday)
                            <?php $total = $total+$sumtoday->total_price ?>
                            @endforeach
                            <span class="info-box-number">{{$total}}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <h1 class="form-group" style="font-size: 23px">Order Today</h1>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group row">
                            <label class="col-sm-2">Order Detail :</label>
                            <div class="col-sm-10 ">
                                @if(!$orders->isEmpty())

                                    <table class="table table-bordered table-hover" id="table">
                                        <td><b>#</b></td>
                                        <td><b>Product Name</b></td>
                                        <td><b>Price Per Unit</b></td>
                                        <td><b>Quantity</b></td>
                                        <td><b>Total Price Per Unit</b></td>
                                        <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($orders as $order)
                                        @foreach($order->orderDetails as $key => $orderDetail)
                                            <tr>
                                                <td width="5%">{{ $no }}</td>
                                                <td width="15%">{{ data_get( $orderDetail,'products.name') }}</td>
                                                <td width="15%">{{ $orderDetail->price_per_unit }}</td>
                                                <td class="quantity" width="15%">{{ $orderDetail->product_qty}}</td>
                                                <td class="total" width="15%">{{ $orderDetail->total_price}}</td>
                                            </tr>
                                        @endforeach
                                            <?php $no++ ?>
                                        @endforeach
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>Total Quantity</b></td>
                                        <td><b>Total Price</b></td>
                                        <tr>
                                            <td colspan="1"></td>
                                            <td></td>
                                            <td></td>
                                            <td class="total-quantity"></td>
                                            <td class="total-price"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @else
                                    <p> No Data</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        </section>
@endsection
@push('script')
<script language="JavaScript" type="text/javascript">
    $(document).ready(function () {
        calculateQuantity();
        calculateTotalPrice();
    });

    function calculateQuantity() {
        var total = 0;
        $('#table .quantity').each(function () {
            var value = parseInt($(this).html());
            total += value;
            $('.total-quantity').text(total);
        });
    }

    function calculateTotalPrice() {
        var total = 0;
        $('#table .total').each(function () {
            var value = parseFloat($(this).html());
            total += value;
            $('.total-price').text(total.toFixed(2));
        });
    }
</script>
@endpush
