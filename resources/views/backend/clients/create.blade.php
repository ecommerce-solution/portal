@extends('backend.layouts.main_dashboard')
@section('title', 'Create Client')
@section('title_header' , 'Create Client')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('clients.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="name"
                           value="{{ old('name') }}">
                </div>
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group {{ $errors->has('role_name') ? 'has-error' : '' }}">
                        <div style="display: none" id="divShowImg">
                            <img id="previewClient">
                            <div><input type="button" value="Clear" onclick="clearClient()"></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readClient(this);" name="image"
                           id="fileClient">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Url </label>
                    <input type="text" class="form-control" placeholder="Url" name="url"
                           value="{{ old('url') }}">
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('clients.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                    Back</a>
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readClient(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewClient').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clearClient() {
            $('#divShowImg').hide();
            $('#previewClient').attr('src', '');
            $('#fileClient').val(null);
        }

    </script>

@endpush
