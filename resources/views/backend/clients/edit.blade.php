@extends('backend.layouts.main_dashboard')
@section('title', 'Edit Client')
@section('title_header' , 'Edit Client')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('clients.update',[$client->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" placeholder="Title" name="name"
                           value="{{ $client->name }}">
                </div>
                <div class="form-group">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <a id="linkClient" href="{{ asset('uploads/'. $client->image) }}" target="blank">
                            <img id="previewClient" src="{{ asset('uploads/'. $client->image) }}"></a>
                        <div><input id="buttonClient" type="button" value="Clear" onclick="clearClient()"
                                    style="display:none"></div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" id="fileClient"
                           onchange="readClient(this);" name="image">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
                <div class="form-group">
                    <label>Url </label>
                    <input type="text" class="form-control " placeholder="Url" name="url"
                           value="{{ $client->url }}">
                </div>
                <div class="box-footer" style="text-align: center">
                    <a href="{{ route('clients.list') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i>
                        Back</a>
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javascript" type="text/javascript">
        function readClient(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewClient').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#linkClient').removeAttr('href');
                $('#buttonClient').show();
            }
        }

        function clearClient() {
            $('#fileClient').val(null);
            $('#previewClient').attr('src', "{{ asset('uploads/'. $client->image) }}");
            $('#linkClient').attr('href', "{{ asset('uploads/'. $client->image) }}");
            $('#buttonClient').hide();
        }
    </script>

@endpush

