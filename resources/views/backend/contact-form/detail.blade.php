@extends('backend.layouts.main_dashboard')
@section('title', 'Detail Contact Form')
@section('title_header','Detail Contact Form')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('contacts.update',[$contacts]) }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group row">
                    <label class="col-sm-2"> Id :</label>
                    <p class="col-sm-10">{{ $contacts->id }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2"> Name :</label>
                    <p class="col-sm-10">{{ $contacts->name }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2"> Email :</label>
                    <p class="col-sm-10">{{ $contacts->email }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2"> Company :</label>
                    <p class="col-sm-10">{{ $contacts->company }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2"> Message :</label>
                    <p class="col-sm-10">{{ $contacts->message }}</p>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Status :</label>
                    <div class="col-sm-10">
                        <select name="status" class="form-control">
                            <option value="Waiting" @if ($contacts->status == "Waiting") selected @endif >Waiting
                            </option>
                            <option value="Accepted" @if ($contacts->status == "Accepted") selected @endif >Accepted
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Note :</label>
                    <div class="col-sm-10">
                       <textarea id="messageContent" class="form-control" rows="4"
                                 name="note" placeholder="Note">{{ $contacts->note }}</textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="{{ route('contacts.list') }}" class="btn btn-danger btn-sm">Back</a>
                <button type="submit" class="update btn btn-success btn-sm">Save</button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script language="javaScript" type="text/javascript">
        $(document).ready(function () {
            $("button.update").click(function (e) {
                if (!confirm('ยืนยันการเปลี่ยนแปลงข้อมูล')) {
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        });
    </script>
@endpush
