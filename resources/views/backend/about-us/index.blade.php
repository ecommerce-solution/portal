@extends('backend.layouts.main_dashboard')
@section('title', 'About Us')
@section('title_header' , 'About Us')
@section('content')
    <div class="box box-primary">
        <form action="{{ route('about-us.save') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label>Title <span style="color:red">*</span></label>
                    <input type="text" class="form-control" name="title" value="{{ array_get($about,'0.title') }}"
                           placeholder="Title">
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label>Short Description <span style="color:red">*</span></label>
                    <textarea class="form-control" name="short_description"
                              placeholder="Short Description"
                              rows="4">{{ array_get($about,'0.short_description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label>Description <span style="color:red">*</span></label>
                    <textarea rows="7" class="form-control ckeditor" placeholder="Description" name="description"
                              placeholder="Description">{{ array_get($about,'0.description') }}</textarea>
                </div>
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Image <span style="color:red">*</span></label>
                    <div class="form-group">
                        <div @if(empty(array_get($about,'0.image'))) style="display: none " @endif id="divShowImg">
                            <a id="link"
                               @if(!empty(array_get($about,'0.image'))) href="{{ asset('uploads/'. array_get($about,'0.image')) }}"
                               @endif target="blank">
                                <img id="previewArticleCover"
                                     @if(!empty(array_get($about,'0.image')))  src="{{ asset('uploads/'. array_get($about,'0.image')) }}"
                                        @endif>
                            </a>
                            <div><input type="button" value="Clear" id="buttonClear"
                                        onclick="clearPreview({{ empty(array_get($about,'0.image')) }})"
                                        @if(!empty(array_get($about,'0.image'))) style="display:none" @endif></div>
                        </div>
                    </div>
                    <input type="file" accept="image/jpeg, image/png" onchange="readCover(this);" name="image"
                           id="filePreview">
                    <p class="help-block">
                        ไฟล์ภาพต้องเป็นนามสกุล jpeg,png เท่านั้น <br>
                        ขนาดไฟล์ไม่เกิน 1 MB
                    </p>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
            <input type="hidden" value="{{ array_get($about,'0.id') }}" name="id">
        </form>
    </div>

@endsection

@push('script')
    <script>
        function readCover(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#falseinput').attr('src', e.target.result);
                    $('#previewArticleCover').attr('src', e.target.result);
                    $('#divShowImg').show();
                };
                reader.readAsDataURL(input.files[0]);
                $('#link').removeAttr('href');
                $('#buttonClear').show();
            }
        }

        function clearPreview(checkImage) {
            if (checkImage == 1) {
                $('#divShowImg').hide();
                $('#previewArticleCover').attr('src', '');
                $('#filePreview').val(null);
            }
            else {
                $('#filePreview').val(null);
                $('#previewArticleCover').attr('src', "{{ asset('uploads/'. array_get($about,'0.image')) }}");
                $('#link').attr('href', "{{ asset('uploads/'. array_get($about,'0.image')) }}");
                $('#buttonClear').hide();
            }
        }

    </script>
@endpush
