<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSkillsTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_skills_training', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('career_form_id')->nullable()->index();
            $table->date('date_start')->nullable()->index();
            $table->date('date_end')->nullable()->index();
            $table->string('job_description')->nullable()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_skills_training');
    }
}
