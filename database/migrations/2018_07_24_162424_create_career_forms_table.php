<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->index();
            $table->integer('career_id')->nullable()->index();
            $table->string('firstname')->nullable()->index();
            $table->string('lastname')->nullable()->index();
            $table->date('date')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->string('email')->nullable()->index();
            $table->string('address')->nullable()->index();
            $table->string('city')->nullable()->index();
            $table->string('province')->nullable()->index();
            $table->integer('zipcode')->nullable()->index();
            $table->string('university')->nullable()->index();
            $table->string('degree')->nullable()->index();
            $table->string('subject')->nullable()->index();
            $table->string('gpa')->nullable()->index();
            $table->string('describe_your_skills')->nullable()->index();
            $table->string('certification')->nullable()->index();
            $table->string('status')->nullable()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_forms');
    }
}
