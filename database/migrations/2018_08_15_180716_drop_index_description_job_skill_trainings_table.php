<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIndexDescriptionJobSkillTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_skill_trainings', function (Blueprint $table) {
            $table->dropIndex('job_skills_training_job_description_index')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_skill_trainings', function (Blueprint $table) {
            $table->index('job_description')->change();
        });
    }
}
