<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIndexColumnCareerFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('career_forms', function (Blueprint $table) {
            $table->dropIndex('career_forms_address_index');
            $table->dropIndex('career_forms_city_index');
            $table->dropIndex('career_forms_province_index');
            $table->dropIndex('career_forms_zipcode_index');
            $table->dropIndex('career_forms_describe_your_skills_index');
            $table->dropIndex('career_forms_certification_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('career_forms', function (Blueprint $table) {
            $table->index('address');
            $table->index('city');
            $table->index('province');
            $table->index('zipcode');
            $table->index('describe_your_skills');
            $table->index('certification');
        });
    }
}
