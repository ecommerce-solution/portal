<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name', 'first_name');
            $table->string('last_name')->after('name')->nullable();
            $table->string('nickname')->after('id_card')->nullable();
            $table->date('birth_date')->after('password')->nullable();
            $table->string('blood_type')->after('birth_date')->nullable();
            $table->string('bank_account_no')->after('blood_type')->nullable();
            $table->string('bank_branch')->after('bank_account_no')->nullable();
            $table->date('start_work_date')->after('bank_branch')->nullable();
            $table->integer('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('first_name', 'name');
            $table->dropColumn([
                'last_name',
                'nickname',
                'birth_date',
                'blood_type',
                'bank_account_no',
                'bank_branch',
                'start_work_date',
                'role_id'
            ]);
        });
    }

}
