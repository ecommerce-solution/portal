<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFieldOrdersAndOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('price', 'total_price');
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->renameColumn('price', 'price_per_unit');
            $table->renameColumn('qty','product_qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('total_price', 'price');
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->renameColumn('price_per_unit', 'price');
            $table->renameColumn('product_qty', 'qty');
        });
    }
}
