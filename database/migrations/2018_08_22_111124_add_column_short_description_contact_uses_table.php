<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnShortDescriptionContactUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_uses', function (Blueprint $table) {
            $table->text('short_description')->after('title')->nullable();
            $table->string('fax')->after('tel')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_uses', function (Blueprint $table) {
            $table->dropColumn('short_description');
            $table->dropColumn('fax');
        });
    }
}
