<?php

namespace App\Exports;

use App\ApplicationForm;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ApplicationFormsExport implements FromCollection, WithHeadings

{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $field_select = [
        'id',
        'firstname',
        'lastname',
        'date',
        'phone',
        'email',
        'university',
        'degree',
        'subject',
        'gpa',
        'status',
        'expected_salary',
        'career_id'
        ];
        $application_form = ApplicationForm::query()->select($field_select)
                ->with([
                        'career' => function ($query) {
                            $query->select('id', 'role_name');
                        }
                ])
                ->orderBy('updated_at', 'desc')
                ->get();
        $applicationForms = $application_form->toArray();
        foreach ($applicationForms as $key => $applicationForm) {
            $applicationForms[$key] += ['career_name' => $applicationForm['career']['role_name']];
            unset($applicationForms[$key]['career']);
            unset($applicationForms[$key]['career_id']);
        }
        return collect($applicationForms);
    }

    public function headings(): array
    {
        return [
                'ID',
                'Name',
                'Lastname',
                'Birthdate',
                'Phone',
                'Email',
                'University',
                'Degree',
                'Subject',
                'Gpa',
                'Status',
                'Expected-Salary',
                'Career'
        ];
    }
}




