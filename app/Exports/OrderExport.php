<?php

namespace App\Exports;

use App\Order;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
class OrderExport implements FromCollection,WithHeadings
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $order_query = Order::query()
            ->where('status', '=', 'unpaid')
            ->orderBy('created_at', 'DESC')->get();

        return collect($order_query);
    }

    public function headings(): array
    {
        return ['Name', 'Total'];
    }
}
