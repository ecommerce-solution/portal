<?php

namespace App\Exports;
use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;
class SummaryExport implements WithHeadings,FromCollection
{
    public function collection()
    {
        $orders = Order::query()->groupBy('user_name')->select([
            'user_name',
            DB::raw('sum(total_price) as total_price')
        ])->get();

        return collect($orders);
    }

    public function headings(): array
    {
        return ['ชื่อ', 'ยอดรวมทั้งสิ้น'];
    }
}
