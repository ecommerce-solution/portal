<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'short_description',
        'description',
        'status',
        'publish_date',
        'cover_image',
        'thumbnail_image'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
