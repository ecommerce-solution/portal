<?php

namespace App\DataTables;

use App\User;
use App\Contact;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ContactFormDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Contact $contact) {
                    return '<form action="#">
                        ' . $this->getActionButtons($contact) . '
                </form>    ';
                })
                ->editColumn('status', function ($contact) {
                    if ($contact->status == "Waiting") {
                        return '<span class="label  label-warning">' . $contact->status . '</span>';
                    }
                    if ($contact->status == "Accepted") {
                        return '<span class="label  label-success">' . $contact->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('name', function ($contact) {
                    if ($contact->name != null) {
                        return str_limit($contact->name, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('email', function ($contact) {
                    if ($contact->email != null) {
                        return str_limit($contact->email, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('company', function ($contact) {
                    if ($contact->company != null) {
                        return str_limit($contact->company, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('created_at', function ($contact) {
                    return Carbon::parse($contact->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($contact) {
                    return Carbon::parse($contact->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $chack = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        $query = Contact::query();

        if (!empty($chack)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                ')
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($contact)
    {
        return implode('&nbsp;', [
                $this->viewToFrontend($contact),
                $this->deleteButton($contact),
        ]);
    }

    private function viewToFrontend($contact): string
    {
        $route = route('contacts.detail', [$contact->id]);
        $attributes = [
                'id' => "button-edit-{$contact->id}",
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-eye'> </i> Detail", $attributes, false, false);

    }

    private function deleteButton($contact): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('contacts.destroy', [$contact]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'email',
                        'name' => 'email',
                        'title' => trans('Email'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'company',
                        'name' => 'company',
                        'title' => trans('Company'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                ],
                [
                        'data' => 'tel',
                        'name' => 'tel',
                        'title' => trans('Telephone'),
                        "className" => "text-left align-middle",
                        'orderable' => false,
                        "width" => "10%",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "5%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
        ];
    }
}
