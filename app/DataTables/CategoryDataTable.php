<?php

namespace App\DataTables;

use App\Article;
use App\Category;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class CategoryDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Category $category) {
                    return '<form action="#">
                        ' . $this->getActionButtons($category) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($category) {
                    return Carbon::parse($category->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($category) {
                    return Carbon::parse($category->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('slug', function ($category) {
                    if ($category->slug != null) {
                        return str_limit($category->slug, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('name', function ($category) {
                    if ($category->name != null) {
                        return str_limit($category->name, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Category::query()
                ->with('article');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($category)
    {
        return implode('&nbsp;', [
                $this->editButton($category),
                $this->deleteButton($category),
        ]);
    }

    private function editButton($category): string
    {
        $route = route('categories.edit', [$category->id]);
        $attributes = [
                'id' => "button-edit-{$category->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($category): string
    {
        $article = Article::query()->where('category_id', $category->id)->first();
        $disabled = !is_null($article) ? "disabled" : "";
        $attributes = [
                'class' => 'btn btn-danger btn-delete delete-category btn-sm',
                'data-url' => route('categories.destroy', [$category]),
                'data-can-delete' => count($category->article) == 0 ? 'yes' : 'no',
                $disabled,
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'desc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'autoWidth' => true,
                'lengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "25%",
                ],
                [
                        'data' => 'slug',
                        'name' => 'slug',
                        'title' => trans('Slug'),
                        "className" => "text-left align-middle",
                        "width" => "30%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
        ];
    }
}
