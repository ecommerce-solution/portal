<?php
namespace App\DataTables;

use App\Role;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class RoleDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Role $role) {
                    return '<form action="#">
                        ' . $this->getActionButtons($role) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($role) {
                    return Carbon::parse($role->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($role) {
                    return Carbon::parse($role->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action',])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Role::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($role)
    {
        return implode('&nbsp;', [
                $this->editButton($role),
        ]);
    }

    private function editButton($role): string
    {
        $route = route('roles.edit', [$role->id]);
        $attributes = [
                'id' => "button-edit-{$role->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => trans('Name'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
