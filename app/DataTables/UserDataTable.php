<?php

namespace App\DataTables;

use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('action', function (User $user) {
                return '<form action="#">
                        ' . $this->getActionButtons($user) . '
                </form>    ';
            })
            ->editColumn('created_at', function ($user) {
                return Carbon::parse($user->created_at)
                    ->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($user) {
                return Carbon::parse($user->updated_at)
                    ->format('d/m/Y H:i');

            })
            ->editColumn('role_id', function ($user) {
                if ($user->role->name == "Employee") {
                    return '<span class="label  label-success">' . $user->role->name . '</span>';
                }
                if ($user->role->name == "Merchant") {
                    return '<span class="label  label-warning">' . $user->role->name . '</span>';
                }
                if ($user->role->name == "Writer") {
                    return '<span class="label  label-info">' . $user->role->name . '</span>';
                }
                if ($user->role->name == "Admin") {
                    return '<span class="label  label-default">' . $user->role->name . '</span>';
                }
                else {
                    return '-';
                }
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'status','role_id'])
            ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = User::query()
            ->where('id', '!=', auth()->user()->id)->with('role');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                "className" => "text-left align-middle",
                "width" => "20%",
            ])
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($user)
    {
        return implode('&nbsp;', [
            $this->editButton($user),
            $this->deleteButton($user),
        ]);
    }

    private function editButton($user): string
    {
        $route = route('users.edit', [$user->id]);
        $attributes = [
            'id' => "button-edit-{$user->id}",
            'class' => 'btn btn-warning btn-sm',
            'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($user): string
    {

        $attributes = [
            'class' => 'btn btn-danger btn-delete btn-sm',
            'data-url' => route('users.destroy', [$user]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
            $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[1, 'asc']],
            'dom' => '<"top"f>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 10,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => true,
            'responsive' => true,
            "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'nickname',
                'name' => 'nickname',
                'title' => trans('Name'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'title' => trans('Email'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'role_id',
                'name' => 'role_id',
                'title' => trans('Role'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('Created at'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "orderable" => false,
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'title' => trans('Updated at'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "orderable" => false,
            ],
        ];
    }
}
