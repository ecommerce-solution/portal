<?php

namespace App\DataTables;

use App\Banner;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class BannerDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Banner $banner) {
                    return '<form action="#">
                        ' . $this->getActionButtons($banner) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($banner) {
                    return Carbon::parse($banner->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($banner) {
                    return Carbon::parse($banner->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('image', function ($banner) {
                    $image = asset('uploads/' . $banner->image);
                    return '<img class="img-responsive" src=' . $image . '>';
                })
                ->editColumn('status', function ($bannerForm) {
                    if ($bannerForm->status == "Close") {
                        return '<span class="label  label-danger">' . $bannerForm->status . '</span>';
                    }
                    if ($bannerForm->status == "Open") {
                        return '<span class="label  label-success">' . $bannerForm->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('title', function ($bannerForm) {
                    if ($bannerForm->title != null) {
                        return str_limit($bannerForm->title, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status', 'image'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Banner::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($banner)
    {
        return implode('&nbsp;', [
                $this->editButton($banner),
                $this->deleteButton($banner),
        ]);
    }

    private function editButton($banner): string
    {
        $route = route('banners.edit', [$banner->id]);
        $attributes = [
                'id' => "button-edit-{$banner->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($banner): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete delete-category btn-sm',
                'data-url' => route('banners.destroy', [$banner]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'desc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'title',
                        'name' => 'title',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "25%",
                ],
                [
                        'data' => 'image',
                        'name' => 'image',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "20%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
