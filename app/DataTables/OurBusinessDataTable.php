<?php

namespace App\DataTables;

use App\OurBusiness;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class OurBusinessDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (OurBusiness $ourBusiness) {
                    return '<form action="#">
                        ' . $this->getActionButtons($ourBusiness) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($ourBusiness) {
                    return Carbon::parse($ourBusiness->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($ourBusiness) {
                    return Carbon::parse($ourBusiness->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('thumbnail_image', function ($ourBusiness) {
                    $image = asset('uploads/' . $ourBusiness->thumbnail_image);
                    return '<img class="img-responsive" src=' . $image . '>';
                })
                ->editColumn('status', function ($ourBusiness) {
                    if ($ourBusiness->status == "DRAFT") {
                        return '<span class="label  label-warning">' . $ourBusiness->status . '</span>';
                    }
                    if ($ourBusiness->status == "PUBLISHED") {
                        return '<span class="label  label-success">' . $ourBusiness->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('title', function ($ourBusiness) {
                    if ($ourBusiness->title != null) {
                        return str_limit($ourBusiness->title, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'thumbnail_image', 'status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = OurBusiness::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($ourBusiness)
    {
        return implode('&nbsp;', [
                $this->editButton($ourBusiness),
                $this->deleteButton($ourBusiness),
        ]);
    }

    private function editButton($ourBusiness): string
    {
        $route = route('our-businesses.edit', [$ourBusiness->id]);
        $attributes = [
                'id' => "button-edit-{$ourBusiness->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-eye'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($ourBusiness): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('our-businesses.destroy', [$ourBusiness]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'title',
                        'name' => 'title',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'thumbnail_image',
                        'name' => 'thumbnail_image',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
