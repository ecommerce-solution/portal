<?php

namespace App\DataTables;

use App\Career;
use App\ApplicationForm;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class CareerDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Career $career) {
                    return '<form action="#">
                        ' . $this->getActionButtons($career) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($career) {
                    return Carbon::parse($career->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($career) {
                    return Carbon::parse($career->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('icon', function ($career) {
                    $image = asset('uploads/' . $career->icon);
                    return '<img class="img-responsive" src=' . $image . '>';
                })
                ->editColumn('status', function ($career) {
                    if ($career->status == "Close") {
                        return '<span class="label  label-danger">' . $career->status . '</span>';
                    }
                    if ($career->status == "Open") {
                        return '<span class="label  label-success">' . $career->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('role_name', function ($career) {
                    if ($career->role_name != null) {
                        return str_limit($career->role_name, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status', 'icon'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Career::query()
                ->with('careerform');;
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($career)
    {
        return implode('&nbsp;', [
                $this->editButton($career),
                $this->deleteButton($career),
        ]);
    }

    private function editButton($career): string
    {
        $route = route('careers.edit', [$career->id]);
        $attributes = [
                'id' => "button-edit-{$career->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($career): string
    {
        $careerForm = ApplicationForm::query()->where('career_id', $career->id)->first();
        $disabled = !is_null($careerForm) ? "disabled" : "";
        $attributes = [
                'class' => 'btn btn-danger btn-delete delete-category btn-sm',
                'data-url' => route('careers.destroy', [$career]),
                'data-can-delete' => count($career->careerform) == 0 ? 'yes' : 'no',
                $disabled
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'role_name',
                        'name' => 'role_name',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'icon',
                        'name' => 'icon',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "10%",
                ],
                [
                        'data' => 'position',
                        'name' => 'position',
                        'title' => trans('Position'),
                        "className" => "text-left align-middle",
                        "width" => "5%",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
