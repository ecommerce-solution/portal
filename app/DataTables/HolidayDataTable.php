<?php

namespace App\DataTables;

use App\Holiday;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class HolidayDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Holiday $holiday) {
                    return '<form action="#">
                        ' . $this->getActionButtons($holiday) . '
                </form>    ';
                })
                ->editColumn('note', function ($holiday) {
                    if ($holiday->note != null) {
                        return str_limit($holiday->note, '15', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('created_at', function ($holiday) {
                    return Carbon::parse($holiday->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($holiday) {
                    return Carbon::parse($holiday->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('start_date', function ($holiday) {
                    return Carbon::parse($holiday->start_date)
                            ->format('d/m/Y ');
                })
                ->editColumn('title', function ($holiday) {
                    if ($holiday->title != null) {
                        return str_limit($holiday->title, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action',])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Holiday::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($holiday)
    {
        return implode('&nbsp;', [
                $this->editButton($holiday),
                $this->deleteButton($holiday),
        ]);
    }

    private function editButton($holiday): string
    {
        $route = route('holidays.edit', [$holiday->id]);
        $attributes = [
                'id' => "button-edit-{$holiday->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($holiday): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('holidays.destroy', [$holiday]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'title',
                        'name' => 'title',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "30%",
                ],
                [
                        'data' => 'start_date',
                        'name' => 'start_date',
                        'title' => trans('Start date'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
