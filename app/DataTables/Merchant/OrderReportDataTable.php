<?php

namespace App\DataTables\Merchant;

use App\Order;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;

class OrderReportDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addIndexColumn()
            ->editColumn('created_at', function ($productReport) {
                return Carbon::parse($productReport['created_at'])
                    ->format('d/m/Y H:i');
            })
            ->setRowClass('text-center')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return datatables($query);
    }

    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $check = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        $query = Order::query();
        if (!empty($check)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }
//        $query = $query->orderBy('created_at', 'desc')->get()->toArray();

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                ')
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'OrderReport_' . date('YmdHis');
    }

    protected function getActionButtons($report)
    {
        return implode('&nbsp;', []);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[1, 'desc']],
            'dom' => '<"top"Bf>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 30,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => false,
            'responsive' => true,
            "scrollX" => true,
            "buttons" => ['excel']
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'user_id',
                'name' => 'user_id',
                'title' => trans('User ID'),
                "className" => "text-left align-middle",
                "width" => "5%",
                "visible"=>false,
            ],
            [
                'data' => 'id',
                'name' => 'id',
                'title' => trans('Order ID'),
                "className" => "text-left align-middle",
                "width" => "5%",
                "visible"=>false,
            ],
            [
                'data' => 'user_name',
                'name' => 'user_name',
                'title' => trans('ผู้ซื้อ'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'total_price',
                'name' => 'total_price',
                'title' => trans('ราคารวม'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('วันที่'),
                "className" => "text-left align-middle",
                "width" => "20%",
                "orderable" => false,
            ],
        ];
    }
}
