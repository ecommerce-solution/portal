<?php

namespace App\DataTables\Merchant;

use App\Order;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('action', function (Order $order) {
                return '<form action="#">
                        ' . $this->getActionButtons($order) . '
                </form>    ';
            })
            ->editColumn('created_at', function ($order) {
                return Carbon::parse($order->created_at)
                    ->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($order) {
                return Carbon::parse($order->updated_at)
                    ->format('d/m/Y H:i');
            })
//                ->editColumn('status', function ($order) {
//                    if ($order->status == 'unpaid') {
//                        return '<span class="label  label-danger">' . 'UNPAID' . '</span>';
//                    } elseif ($order->status == 'paid') {
//                        return '<span class="label  label-success">' . 'PAID' . '</span>';
//                    } else{
//                        return '-';
//                    }
//                })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');
        $inputSelectUser = request()->get('inputSelectUser');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $check = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        $query = Order::query()->with('user');;

        if(!empty($inputSelectUser))
            if ($inputSelectUser != 'All'){
                $query = $query->where('user_id', $inputSelectUser);
            }

        if (!empty($check)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }


        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('','
            data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                data.inputSelectUser = $("#inputSelectUser").val();
                ')
            ->addAction([
                "className" => "text-left align-middle",
                "width" => "20%",
            ])
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'Order-report_at_' . date('d-m-Y');
    }

    protected function getActionButtons($order)
    {
        return implode('&nbsp;', [
            $this->detailButton($order),
//                $this->deleteButton($order),
        ]);
    }

    private function detailButton($order): string
    {
        $route = route('orders.detail', [$order->id]);
        $attributes = [
            'id' => "button-detail-{$order->id}",
            'class' => 'btn btn-primary btn-sm',
            'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-eye'> </i> Details", $attributes, false, false);

    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[2, 'desc']],
            'dom' => '<"top"f>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 20,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => false,
            'responsive' => true,
            "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'user_id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'user_id',
                'name' => 'user_id',
                'title' => trans('User ID'),
                "className" => "text-left align-middle",
                "width" => "8%",
                "visible"=>false,
            ],
            [
                'data' => 'id',
                'name' => 'id',
                'title' => trans('Order ID'),
                "className" => "text-left align-middle",
                "width" => "8%",
                "visible"=>false,
            ],
            [
                'data' => 'user_name',
                'name' => 'user_name',
                'title' => trans('ผู้ซื้อ'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'total_price',
                'name' => 'total_price',
                'title' => trans('ราคารวม'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
//                [
//                        'data' => 'status',
//                        'name' => 'status',
//                        'title' => trans('Status'),
//                        "className" => "text-left align-middle",
//                        "width" => "15%",
//                ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('วันที่'),
                "className" => "text-left align-middle",
                "width" => "15%",
                "orderable" => false,
            ],
        ];
    }
}
