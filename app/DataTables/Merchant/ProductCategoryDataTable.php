<?php

namespace App\DataTables\Merchant;

use App\Product;
use App\ProductCategory;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ProductCategoryDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('action', function (ProductCategory $productCategory) {
                return '<form action="#">
                        ' . $this->getActionButtons($productCategory) . '
                </form>    ';
            })
            ->editColumn('created_at', function ($productCategory) {
                return Carbon::parse($productCategory->created_at)
                    ->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($productCategory) {
                return Carbon::parse($productCategory->updated_at)
                    ->format('d/m/Y H:i');
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action',])
            ->make(true);
    }

    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    public function query()
    {
        $query = ProductCategory::query()
            ->with('products');
        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                "className" => "text-left align-middle",
                "width" => "20%",
            ])
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($productCategory)
    {
        return implode('&nbsp;', [
            $this->editButton($productCategory),
            $this->deleteButton($productCategory),
        ]);
    }

    private function editButton($productCategory): string
    {
        $route = route('product-categories.edit', [$productCategory->id]);
        $attributes = [
            'id' => "button-edit-{$productCategory->id}",
            'class' => 'btn btn-warning btn-sm',
            'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($productCategory): string
    {
        $product = Product::query()->where('product_category_id', $productCategory->id)->first();
        $disabled = !is_null($product) ? "disabled" : "";
        $attributes = [
            'class' => 'btn btn-danger btn-delete btn-sm',
            'data-url' => route('product-categories.destroy', [$productCategory]),
            'data-can-delete' => count($productCategory->products) == 0 ? 'yes' : 'no',
            $disabled,
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
            $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[1, 'asc']],
            'dom' => '<"top"f>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 10,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => true,
            'responsive' => true,
            "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'title',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'title',
                'name' => 'title',
                'title' => trans('ชื่อ'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('Created at'),
                "className" => "text-left align-middle",
                "width" => "15%",
                "orderable" => false,
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'title' => trans('Updated at'),
                "className" => "text-left align-middle",
                "width" => "15%",
                "orderable" => false,
            ],
        ];
    }
}
