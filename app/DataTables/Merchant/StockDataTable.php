<?php

namespace App\DataTables\Merchant;

use App\Stock;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;

class StockDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->editColumn('created_at', function ($stock) {
                return (Carbon::parse($stock['created_at'])
                    ->format('d/m/Y H:i'));
            })
            ->editColumn('order_id', function ($stock) {
                if ($stock['order_id'] == null) {
                    $stock['order_id'] = 'เพิ่มลดโดย Admin';
                }
                return $stock['order_id'];
            })
            ->editColumn('products.name', function ($stock) {
                    return data_get($stock, 'products.name','ไม่พบข้อมูล Product ID :'.$stock['product_id']);
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return datatables($query);
    }

    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');
        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $check = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }
        $query = Stock::query()->with('products', 'orders.user');

        if (!empty($check)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }

//        $query = $query->orderBy('created_at', 'desc')->get()->toArray();

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                ')
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'Stock_' . date('YmdHis');
    }

    protected function getActionButtons($stock)
    {
        return implode('&nbsp;', [
        ]);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[1, 'desc']],
            'dom' => '<"top"Bf>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 30,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => false,
            'responsive' => true,
            "scrollX" => true,
            "buttons" => ['excel']
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'order_id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'order_id',
                'name' => 'order_id',
                'title' => trans('Order ID'),
                "className" => "text-left align-middle",
                "width" => "15%",
                "visible"=>false,
            ],
            [
                'data' => "products.name",
                'name' => "products.name",
                'title' => trans('สินค้า'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => 'type',
                'name' => 'type',
                'title' => trans('Type'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "orderable" => false,
            ],
            [
                'data' => 'quantity',
                'name' => 'quantity',
                'title' => trans('จำนวน (ชิ้น)'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'total',
                'name' => 'total',
                'title' => trans('จำนวน (ทั้งหมด)'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('วันที่'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "orderable" => false,
            ],
        ];
    }
}
