<?php

namespace App\DataTables\Merchant;

use App\Product;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;

class ProductDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('action', function (Product $product) {
                return '<form action="#">
                        ' . $this->getActionButtons($product) . '
                </form>    ';
            })
            ->editColumn('created_at', function ($product) {
                return Carbon::parse($product->created_at)
                    ->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($product) {
                return Carbon::parse($product->updated_at)
                    ->format('d/m/Y H:i');
            })
            ->editColumn('image', function ($product) {
                if ($product->image == 'NULL') {
                    return '<img style="height: 120px;width: 120px;" src="https://via.placeholder.com/180x120.png?text=No%20Image">';
                }
                $image = asset('uploads/' . $product->image);
                return '<img style="height: 120px;width: 120px;" src=' . $image . '>';
            })
            ->editColumn('product_category.title', function ($product) {
//                    return $product->product_category->name;
                return data_get($product, 'product_category.title','ไม่พบข้อมูล Product Category ID : ' . $product['product_category_id']);
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    public function query()
    {
        $checkSelect = request()->get('inputSelectCategory');
        if (empty($checkSelect)) {
            $query = Product::query()
                ->with('product_category');
        } else {
            $query = Product::with('product_category')->whereIn('product_category_id', $checkSelect);
        }

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputSelectCategory = $("#inputSelectCategory").val();
                ')
            ->addAction([
                "className" => "text-left align-middle",
                "width" => "20%",
            ])
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($product)
    {
        return implode('&nbsp;', [
            $this->editButton($product),
            $this->deleteButton($product),
        ]);
    }

    private function editButton($product): string
    {
        $route = route('products.edit', [$product->id]);
        $attributes = [
            'id' => "button-edit-{$product->id}",
            'class' => 'btn btn-warning btn-sm',
            'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($product): string
    {
        $attributes = [
            'class' => 'btn btn-danger btn-delete delete-category btn-sm',
            'data-url' => route('products.destroy', [$product]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
            $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[5, 'desc']],
            'dom' => '<"top"f>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 20,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => true,
            'responsive' => true,
            "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'name',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'name',
                'name' => 'name',
                'title' => trans('ชื่อ'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'image',
                'name' => 'image',
                'title' => trans('รูป'),
                "className" => "text-left align-middle",
                "orderable" => false,
                "searchable" => false,
                "width" => "15%",
            ],
            [
                'data' => 'product_category.title',
                'name' => 'product_category.title',
                'title' => trans('หมวดหมู่'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'qty',
                'name' => 'qty',
                'title' => trans('จำนวน'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "width" => "10%",
            ],
            [
                'data' => 'price',
                'name' => 'price',
                'title' => trans('ราคา'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "width" => "10%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('Created at'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "orderable" => false,
                "width" => "10%",
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'title' => trans('Updated at'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "orderable" => false,
                "width" => "12%",
            ],
        ];
    }
}