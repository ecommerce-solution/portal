<?php

namespace App\DataTables\Merchant;

use App\Order;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class UserReportDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->editColumn('date', function () {
                $inputFormDate = request()->get('inputFormDate');
                $inputToDate = request()->get('inputToDate');
                if (!empty($inputFormDate) && !empty($inputToDate)) {
                    $inputFormDate = Carbon::createFromFormat('d/m/Y',
                        request()->get('inputFormDate'))->startOfDay()->format('d/m/Y H:i');
                    $inputToDate = Carbon::createFromFormat('d/m/Y',
                        request()->get('inputToDate'))->endOfDay()->format('d/m/Y H:i');
                    return $inputFormDate . ' - ' . $inputToDate;
                } else {
                    return 'รวมทุกวัน';
                }
            })
            ->editColumn('user.nickname', function ($user) {
                return data_get($user, 'user.nickname', 'ไม่พบข้อมูล User ID : ' . $user['user_id']);
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return datatables($query);
    }

    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');
        $inputSelectUser = request()->get('inputSelectUser');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $check = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        $query = Order::query();

        if (!empty($inputSelectUser)) {
            if ($inputSelectUser != 'All') {
                $query = $query->where('user_id', $inputSelectUser);
            }
        }

        $query = $query->with('user');

        if (!empty($check)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }

        $query = $query->groupBy('user_id')
            ->select(['user_id', DB::raw('sum(total_price) as total_price')]);
        $query = $query->get()->toArray();

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                data.inputSelectUser = $("#inputSelectUser").val();
                ')
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'UserReport_' . date('YmdHis');
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[2, 'desc']],
            'dom' => '<"top"Bf>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 30,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => false,
            'responsive' => true,
            "scrollX" => true,
            "buttons" => ['excel']
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'user_id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "orderable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'user_id',
                'name' => 'user_id',
                'title' => trans('User ID'),
                "className" => "text-left align-middle",
                "width" => "5%",
                "visible"=>false,
            ],
            [
                'data' => 'user.nickname',
                'name' => 'user.nickname',
                'title' => trans('ชื่อ'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => "total_price",
                'name' => "total_price",
                'title' => trans('ยอดรวม'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => "date",
                'name' => "date",
                'title' => trans('วันที่'),
                "className" => "text-left align-middle",
                "width" => "15%",
                "orderable" => false,
            ],
        ];
    }
}
