<?php

namespace App\DataTables\Merchant;

use App\OrderDetail;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;

class ProductReportDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addIndexColumn()
            ->editColumn('created_at', function ($productReport) {
                return Carbon::parse($productReport['created_at'])
                    ->format('d/m/Y H:i');
            })
            ->editColumn('order.user_name', function ($productReport) {
                return data_get($productReport, 'order.user_name', 'ไม่พบข้อมูล');
            })
            ->setRowClass('text-center')
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return datatables($query);
    }

    public function query()
    {
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');
        $inputSelectProduct = request()->get('inputSelectProduct');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $check = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        $query = OrderDetail::query()
            ->with('order.user', 'products');

        if(!empty($inputSelectProduct))
            if ($inputSelectProduct != 'All'){
                $query = $query->where('product_id', $inputSelectProduct);
            }

        if (!empty($check)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }

//        $query = $query->orderBy('created_at', 'desc')->get()->toArray();

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                data.inputSelectProduct = $("#inputSelectProduct").val();
                ')
            ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ProductReport_' . date('YmdHis');
    }

    protected function getActionButtons($report)
    {
        return implode('&nbsp;', []);
    }

    protected function getBuilderParameters()
    {
        return [
            'order' => [[3, 'desc']],
            'dom' => '<"top"Bf>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 30,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => false,
            'responsive' => true,
            "scrollX" => true,
            "buttons" => ['excel']
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'name' => 'order_id',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "searchable" => false,
                "orderable" => false,
                "width" => "5%",
            ],
            [
                'data' => 'order_id',
                'name' => 'order_id',
                'title' => trans('Order ID'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "visible"=>false,
            ],
            [
                'data' => 'order.user_name',
                'name' => 'order.user_name',
                'title' => trans('ผู้ซื้อ'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'product_name',
                'name' => 'product_name',
                'title' => trans('สินค้า'),
                "className" => "text-left align-middle",
                "width" => "15%",
            ],
            [
                'data' => 'price_per_unit',
                'name' => 'price_per_unit',
                'title' => trans('ราคาต่อชิ้น'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'product_qty',
                'name' => 'product_qty',
                'title' => trans('จำนวน(ชิ้น)'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'total_price',
                'name' => 'total_price',
                'title' => trans('ราคารวม'),
                "className" => "text-left align-middle",
                "width" => "10%",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => trans('วันที่'),
                "className" => "text-left align-middle",
                "width" => "10%",
                "orderable" => false,
            ],
        ];
    }
}
