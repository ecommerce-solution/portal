<?php

namespace App\DataTables\Merchant;

use App\Order;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class PurchaseHistoryDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Order $order) {
                    return '<form action="#">
                        ' . $this->getActionButtons($order) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($order) {
                    return Carbon::parse($order->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($order) {
                    return Carbon::parse($order->updated_at)
                            ->format('d/m/Y H:i');
                })
//                ->editColumn('status', function ($order) {
//                    if ($order->status == 'unpaid') {
//                        return '<span class="label  label-danger">' . 'UNPAID' . '</span>';
//                    } elseif ($order->status == 'paid') {
//                        return '<span class="label  label-success">' . 'PAID' . '</span>';
//                    } else{
//                        return '-';
//                    }
//                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action','status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Order::query()->where('user_id','=',auth()->user()->id);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($order)
    {
        return implode('&nbsp;', [
                $this->detailButton($order),
//                $this->deleteButton($order),
        ]);
    }

    private function detailButton($order): string
    {
        $route = route('purchase-histories.detail', [$order->id]);
        $attributes = [
                'id' => "button-edit-{$order->id}",
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-eye'> </i> Detail", $attributes, false, false);

    }


    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'desc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => trans('Order Id'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'total_price',
                        'name' => 'total_price',
                        'title' => trans('Total'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
//                [
//                        'data' => 'status',
//                        'name' => 'status',
//                        'title' => trans('Status'),
//                        "className" => "text-left align-middle",
//                        "width" => "15%",
//                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Date'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
