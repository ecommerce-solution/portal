<?php

namespace App\DataTables\Merchant;

use App\Order;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class BalanceDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('action', function ($query) {
                return '<form action="#">
                        ' . $this->getActionButtons($query) . '
                </form>    ';
            })
            ->editColumn('status', function ($query) {
                if ($query['status'] == 'unpaid') {
                    return '<span class="label  label-danger">' . 'UNPAID' . '</span>';
                } elseif ($query['status'] == 'paid') {
                    return '<span class="label  label-success">' . 'PAID' . '</span>';
                }
            })
            ->addIndexColumn()
            ->setRowClass('text-center')
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function dataTable($query)
    {
        return datatables($query);
    }

    public function query()
    {
        $query = collect();
        $check = 0;
        $checkSelect = request()->get('inputSelectBalance');
        if (empty($checkSelect) || $checkSelect == 'all') {
            $check = 0;
        } else {
            if ($checkSelect == 'unpaid') {
                $check = 1;
            } else {
                if ($checkSelect == 'paid') {
                    $check = 2;
                }
            }
        }

        if ($check == 1 || $check == 0) {
            $orders_unpaid = Order::with('user')->where('status', 'unpaid')
                ->orderBy('created_at', 'DESC')
                ->get()
                ->groupBy('user_id');
            if (count($orders_unpaid)) {
                foreach ($orders_unpaid as $group_unpaid) {
                    $grouped_unpaid[] = $group_unpaid->groupBy(function ($created_at) {
                        return Carbon::parse($created_at->created_at)->format('m/Y');
                    })->values();
                }
                foreach ($grouped_unpaid as $group_unpaid) {
                    foreach ($group_unpaid as $a) {
                        $query->push([
                            'name' => $a[0]->user_name,
                            'user_id' => $a[0]->user_id,
                            'date' => $a[0]->created_at->format('m/Y'),
                            'total_price' => number_format($a->sum('total_price'), 2),
                            'status' => $a[0]->status
                        ]);
                    }
                };
            }
        }

        if ($check == 2 || $check == 0) {
            $orders_paid = Order::with('user')->where('status', 'paid')
                ->orderBy('created_at', 'DESC')
                ->get()
                ->groupBy('user_id');
            if (count($orders_paid)) {
                foreach ($orders_paid as $group_paid) {
                    $grouped_paid[] = $group_paid->groupBy(function ($created_at) {
                        return Carbon::parse($created_at->created_at)->format('m/Y');
                    })->values();
                }
                foreach ($grouped_paid as $group_paid) {
                    foreach ($group_paid as $a) {
                        $query->push([
                            'name' => $a[0]->user_name,
                            'user_id' => $a[0]->user_id,
                            'date' => $a[0]->created_at->format('m/Y'),
                            'total_price' => number_format($a->sum('total_price'), 2),
                            'status' => $a[0]->status
                        ]);
                    }
                };
            }
        }

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', '
                data.inputSelectBalance = $("#inputSelectBalance").val();
                ')
            ->addAction([
                "className" => "text-left align-middle",
                "width" => "20%",
            ])
            ->parameters($this->getBuilderParameters());
    }

    protected function getBuilderParameters()
    {
        return [
            'dom' => '<"top"Bf>tr<"bottom"ip><"clear">',
            'processing' => true,
            'pageLength' => 10,
            'bAutoWidth' => true,
            'bLengthChange' => true,
            'info' => true,
            'searching' => true,
            'responsive' => true,
            "scrollX" => true,
            "buttons" => ['excel'],
        ];
    }

    protected function getColumns()
    {
        return [
            [
                'data' => 'DT_Row_Index',
                'title' => trans('#'),
                "className" => "text-left align-middle",
                "width" => "5%",
            ],
            [
                'data' => 'name',
                'name' => 'name',
                'title' => trans('Name'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'date',
                'name' => 'date',
                'title' => trans('Date'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'total_price',
                'name' => 'total_price',
                'title' => trans('Total Price'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'title' => trans('Status'),
                "className" => "text-left align-middle",
                "width" => "20%",
            ],
        ];
    }

    protected function filename()
    {
        return 'Merchant-Balance_' . date('m:Y');
    }

    protected function getActionButtons($query)
    {
        return implode('&nbsp;', [
            $this->detailButton($query),
        ]);
    }

    private function detailButton($query): string
    {
        $route = route('balances.detail', [$query['user_id'], 'status' => $query['status'], 'date' => $query['date']]);
        $attributes = [
            'id' => "button-edit-" . $query['user_id'],
            'class' => 'btn btn-warning btn-sm',
            'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }
}
