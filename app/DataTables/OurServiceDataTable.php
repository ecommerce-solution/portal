<?php

namespace App\DataTables;

use App\OurService;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class OurServiceDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (OurService $ourService) {
                    return '<form action="#">
                        ' . $this->getActionButtons($ourService) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($ourService) {
                    return Carbon::parse($ourService->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($ourService) {
                    return Carbon::parse($ourService->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('image', function ($ourService) {
                    $image = asset('uploads/' . $ourService->image);
                    return '  <div class="item-image" align="center">
  <img class="img-responsive" src=' . $image . '></div>';
                })
                ->editColumn('status', function ($ourService) {
                    if ($ourService->status == "CLOSE") {
                        return '<span class="label  label-danger">' . $ourService->status . '</span>';
                    }
                    if ($ourService->status == "OPEN") {
                        return '<span class="label  label-success">' . $ourService->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('title', function ($ourService) {
                    if ($ourService->title != null) {
                        return str_limit($ourService->title, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'image', 'status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = OurService::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($ourService)
    {
        return implode('&nbsp;', [
                $this->editButton($ourService),
                $this->deleteButton($ourService),
        ]);
    }

    private function editButton($ourService): string
    {
        $route = route('our-services.edit', [$ourService->id]);
        $attributes = [
                'id' => "button-edit-{$ourService->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($ourService): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('our-services.destroy', [$ourService]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'title',
                        'name' => 'title',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'image',
                        'name' => 'image',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "15%",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                        "orderable" => false,
                ],
        ];
    }
}
