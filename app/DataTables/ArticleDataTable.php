<?php

namespace App\DataTables;

use App\Article;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ArticleDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Article $article) {
                    return '<form action="#">
                        ' . $this->getActionButtons($article) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($article) {
                    return Carbon::parse($article->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($article) {
                    return Carbon::parse($article->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('publish_date', function ($article) {
                    return Carbon::parse($article->publish_date)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('thumbnail_image', function ($article) {
                    $image = asset('uploads/' . $article->thumbnail_image);
                    return '<img class="img-responsive" src=' . $image . '>';
                })
                ->editColumn('status', function ($article) {
                    if ($article->status == "DRAFT") {
                        return '<span class="label  label-warning">' . $article->status . '</span>';
                    }
                    if ($article->status == "PUBLISHED") {
                        return '<span class="label  label-success">' . $article->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('title', function ($article) {
                    if ($article->title != null) {
                        return str_limit($article->title, '80', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('category.name', function ($article) {
                    return data_get($article, 'category.name','ไม่พบข้อมูล Career ID : ' . $article['category_id']);
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status', 'thumbnail_image'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Article::query()
                ->with('category');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($article)
    {
        return implode('&nbsp;', [
                $this->editButton($article),
                $this->deleteButton($article),
        ]);
    }

    private function editButton($article): string
    {
        $route = route('articles.edit', [$article->id]);
        $attributes = [
                'id' => "button-edit-{$article->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($article): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete delete-category btn-sm',
                'data-url' => route('articles.destroy', [$article]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'buttons' => ['reload'],
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'title',
                        'name' => 'title',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ],
                [
                        'data' => 'thumbnail_image',
                        'name' => 'thumbnail_image',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "15%",
                ],
                [
                        'data' => 'category.name',
                        'name' => 'category.name',
                        'title' => trans('Category'),
                        "className" => "text-left align-middle",
                        "width" => "5%",
                ],
                [
                        'data' => 'publish_date',
                        'name' => 'publish_date',
                        'title' => trans('Publish date'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "5%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
        ];
    }
}
