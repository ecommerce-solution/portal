<?php

namespace App\DataTables;

use App\ApplicationForm;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ApplicationFormDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (ApplicationForm $applicationForm) {
                    return '<form action="#">
                        ' . $this->getActionButtons($applicationForm) . '
                </form>    ';

                })
                ->editColumn('status', function ($applicationForm) {
                    if ($applicationForm->status == "Waiting") {
                        return '<span class="label  label-warning">' . $applicationForm->status . '</span>';
                    }
                    if ($applicationForm->status == "Accepted") {
                        return '<span class="label  label-success">' . $applicationForm->status . '</span>';
                    } else {
                        return '-';
                    }
                })
                ->editColumn('created_at', function ($applicationForm) {
                    return Carbon::parse($applicationForm->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($applicationForm) {
                    return Carbon::parse($applicationForm->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('firstname', function ($applicationForm) {
                    if ($applicationForm->firstname != null) {
                        return str_limit($applicationForm->firstname, '30', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('email', function ($applicationForm) {
                    if ($applicationForm->email != null) {
                        return str_limit($applicationForm->email, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('career.role_name', function ($applicationForm) {
                    return data_get($applicationForm, 'career.role_name','ไม่พบข้อมูล Career ID : ' . $applicationForm['career_id']);
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $checkSelect = request()->get('inputSelectCareer');
        $inputFormDate = request()->get('inputFormDate');
        $inputToDate = request()->get('inputToDate');

        if (!empty($inputFormDate) && !empty($inputToDate)) {
            $chack = true;
            $inputFormDate = Carbon::createFromFormat('d/m/Y', request()->get('inputFormDate'))->startOfDay();
            $inputToDate = Carbon::createFromFormat('d/m/Y', request()->get('inputToDate'))->endOfDay();
        }

        if (empty($checkSelect)) {
            $query = ApplicationForm::query()
                    ->with('career');
        } else {
            $query = ApplicationForm::with('career')->whereIn('career_id', $checkSelect);
        }

        if (!empty($chack)) {
            $query = $query->whereBetween('created_at', [$inputFormDate, $inputToDate]);
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax('', '
                data.inputSelectCareer = $("#inputSelectCareer").val();
                data.inputFormDate = $("#inputFormDate").val();
                data.inputToDate = $("#inputToDate").val();
                ')
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());

    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($applicationForm)
    {
        return implode('&nbsp;', [

                $this->viewToFrontend($applicationForm),
                $this->deleteButton($applicationForm),

        ]);
    }

    private function viewToFrontend($applicationForm): string
    {
        $route = route('application-form.detail', [$applicationForm->id]);
        $attributes = [

                'id' => "button-edit-{$applicationForm->id}",
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',

        ];

        return link_to($route, "<i class='fa fa-eye'> </i> Detail", $attributes, false, false);

    }

    private function deleteButton($applicationForm): string
    {
        $attributes = [

                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('application-form.destroy', [$applicationForm]),

        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'desc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'firstname',
                        'name' => 'firstname',
                        'title' => trans('Name'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'email',
                        'name' => 'email',
                        'title' => trans('Email'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'career.role_name',
                        'name' => 'career.role_name',
                        'title' => trans('Career'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "defaultContent" => "-",
                ],
                [
                        'data' => 'phone',
                        'name' => 'phone',
                        'title' => trans('Telephone'),
                        "className" => "text-left align-middle",
                        'orderable' => false,
                        "width" => "10%",
                        "defaultContent" => "-",
                ],
                [
                        'data' => 'status',
                        'name' => 'status',
                        'title' => trans('Status'),
                        "className" => "text-left align-middle",
                        "width" => "5%",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
        ];
    }
}
