<?php

namespace App\DataTables;

use App\Client;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ClientDataTable extends DataTable
{
    public function ajax()
    {
        return $this->dataTable($this->query())
                ->addColumn('action', function (Client $client) {
                    return '<form action="#">
                        ' . $this->getActionButtons($client) . '
                </form>    ';
                })
                ->editColumn('created_at', function ($client) {
                    return Carbon::parse($client->created_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('updated_at', function ($client) {
                    return Carbon::parse($client->updated_at)
                            ->format('d/m/Y H:i');
                })
                ->editColumn('image', function ($client) {
                    $image = asset('uploads/' . $client->image);
                    return '<img class="img-responsive previewImageClientList" src=' . $image . '>';
                })
                ->editColumn('name', function ($client) {
                    if ($client->name != null) {
                        return str_limit($client->name, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->editColumn('url', function ($client) {
                    if ($client->url != null) {
                        return str_limit($client->url, '35', '...');
                    } else {
                        return '-';
                    }
                })
                ->addIndexColumn()
                ->setRowClass('text-center')
                ->rawColumns(['action', 'status', 'image'])
                ->make(true);
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return new EloquentDataTable($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Client::query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction([
                        "className" => "text-left align-middle",
                        "width" => "20%",
                ])
                ->parameters($this->getBuilderParameters());
    }

    protected function filename()
    {
        return 'ApplicationForm_' . date('YmdHis');
    }

    protected function getActionButtons($client)
    {
        return implode('&nbsp;', [
                $this->editButton($client),
                $this->deleteButton($client),
        ]);
    }

    private function editButton($client): string
    {
        $route = route('clients.edit', [$client->id]);
        $attributes = [
                'id' => "button-edit-{$client->id}",
                'class' => 'btn btn-warning btn-sm',
                'data-toggle' => 'tooltip',
        ];

        return link_to($route, "<i class='fa fa-pencil'> </i> Edit", $attributes, false, false);

    }

    private function deleteButton($client): string
    {

        $attributes = [
                'class' => 'btn btn-danger btn-delete btn-sm',
                'data-url' => route('clients.destroy', [$client]),
        ];

        return link_to(('#'), "<i class='fa fa-trash'></i> Delete ",
                $attributes, false, false);
    }

    protected function getBuilderParameters()
    {
        return [
                'order' => [[1, 'asc']],
                'dom' => '<"top"f>tr<"bottom"ip><"clear">',
                'processing' => true,
                'pageLength' => 10,
                'bAutoWidth' => true,
                'bLengthChange' => true,
                'info' => true,
                'searching' => true,
                'responsive' => true,
                "scrollX" => true,
        ];
    }

    protected function getColumns()
    {
        return [
                [
                        'data' => 'DT_Row_Index',
                        'name' => 'id',
                        'title' => trans('#'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "5%",
                ],
                [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => trans('Title'),
                        "className" => "text-left align-middle",
                        "width" => "15%",
                ],
                [
                        'data' => 'image',
                        'name' => 'image',
                        'title' => trans('Image'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "15%",
                ],
                [
                        'data' => 'url',
                        'name' => 'url',
                        'title' => trans('URL'),
                        "className" => "text-left align-middle",
                        "orderable" => false,
                        "searchable" => false,
                        "width" => "25%",
                        "defaultContent" => "-",
                ],
                [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => trans('Created at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
                [
                        'data' => 'updated_at',
                        'name' => 'updated_at',
                        'title' => trans('Updated at'),
                        "className" => "text-left align-middle",
                        "width" => "10%",
                        "orderable" => false,
                ],
        ];
    }
}
