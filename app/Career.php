<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
    use SoftDeletes;
    protected $fillable = ['role_name', 'icon', 'short_description', 'description', 'position', 'status',];

    public function careerform()
    {
        return $this->hasMany(ApplicationForm::class);
    }
}
