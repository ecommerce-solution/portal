<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;
    protected $fillable = ['order_id', 'product_id', 'type', 'quantity', 'total','user_id',];

    public function products()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
    public function orders()
    {
        return $this->belongsTo(Order::class,'order_id');
    }

}
