<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationForm extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'career_id',
        'firstname',
        'lastname',
        'date',
        'phone',
        'email',
        'university',
        'degree',
        'subject',
        'gpa',
        'status',
        'expected_salary',
        'attach'
    ];
    protected $dates = ['deleted_at'];

    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function jobSkillTrainings()
    {
        return $this->hasMany(JobSkillTraining::class);

    }
}
