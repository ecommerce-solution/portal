<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id', 'user_name', 'total_price', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
}
