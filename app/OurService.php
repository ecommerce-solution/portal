<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurService extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'short_description', 'description', 'image','status'];
    protected $dates = ['deleted_at'];

}
