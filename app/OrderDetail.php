<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderDetail extends Model
{
    use SoftDeletes;
    protected $fillable = ['order_id', 'product_id', 'product_name', 'price_per_unit', 'product_qty'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
    public function products()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
