<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurBusiness extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'short_description', 'description', 'cover_image', 'thumbnail_image', 'url','status'];
    protected $dates = ['deleted_at'];
}
