<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = ['title', 'short_description', 'time', 'email', 'address', 'tel', 'fax'];
    protected $table = 'contact_uses';
}
