<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'price', 'product_category_id', 'description', 'image', 'qty',];


    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }
    public function order_detail()
    {
        return $this->hasMany(OrderDetail::class);
    }
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
}
