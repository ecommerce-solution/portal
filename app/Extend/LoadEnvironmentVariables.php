<?php
/**
 * Created by PhpStorm.
 * User: Boom
 * Date: 7/18/2018
 * Time: 1:47 PM
 */

namespace App\Extend;

use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables as Base;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Illuminate\Contracts\Foundation\Application;


class LoadEnvironmentVariables extends Base
{
    public function bootstrap(Application $app)
    {
        if ($app->configurationIsCached()) {
            return;
        }

        $this->checkForSpecificEnvironmentFile($app);

        try {

            $match = [
                '' => ['portal.test',],
                '.igetapp' => ['ecs-dev.igetapp','192.168.103.113','ecommerce-solution.co.th'],
            ];

            foreach ($match as $env => $link) {
                foreach ($link as $url) {
                    if (strpos(url()->current(), $url) !== false) {
                        (new Dotenv($app->environmentPath(), '.env' . $env))->load();
                        return;
                    }
                }
            }

            (new Dotenv($app->environmentPath(), $app->environmentFile()))->load();

        } catch (InvalidFileException $e) {
            die('The environment file is invalid: ' . $e->getMessage());
        }
    }
}