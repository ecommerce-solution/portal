<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSkillTraining extends Model
{
    use SoftDeletes;
    protected $fillable = ['application_form_id', 'company_name', 'date_start', 'date_end', 'job_description', 'job_title'];
    protected $dates = ['deleted_at'];

    public function careerForm()
    {
        return $this->belongsTo(ApplicationForm::class);
    }
}
