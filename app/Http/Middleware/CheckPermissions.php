<?php

namespace App\Http\Middleware;


use Closure;


class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check() && $request->user()->role_id != 1) {
            return response()->view('error.404');
        }
        return $next($request);
    }
}
