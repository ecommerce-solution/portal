<?php

namespace App\Http\Requests;

use App\Rules\MinidCard;
use App\Rules\UniqueIdcard;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $this->id_card = str_replace(" ", "", $this->id_card);
        $user = $this->user->id;
        return [
           'email' => 'required|unique:users,email,'.$this->user->id,
           'id_card' => ['required',new UniqueIdcard($user) ,new MinidCard()],
           'first_name' => 'required','string','max:255',
           'last_name' => 'required|string|max:255',
           'nickname' => 'required|unique:users,nickname,'.$this->user->id,
           'birth_date' => 'nullable|date_format:d/m/Y',
           'phone_number' => 'nullable',
           'blood_type' => 'nullable|string|max:255',
           'bank_account_no' => 'nullable|string|max:255',
           'bank_branch' => 'nullable|string|max:255',
           'start_work_date' => 'nullable|date_format:d/m/Y',
    ];





//        $rules = [
//            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id),],
//            'id_card' => 'required|string|max:255',
//            'first_name' => 'required|string|max:255',
//            'last_name' => 'required|string|max:255',
//            'nickname' => ['required|unique:users', 'string', Rule::unique('users')->ignore($user->id),],
//            'birth_date' => 'nullable|date_format:d/m/Y',
//            'phone_number' => 'nullable|numeric|digits_between:1,10',
//            'blood_type' => 'nullable|string|max:255',
//            'bank_account_no' => 'nullable|string|max:255',
//            'bank_branch' => 'nullable|string|max:255',
//            'start_work_date' => 'nullable|date_format:d/m/Y',
//        ];
//
//        return  Validator::make( $rules, [])->validate();
//
    }
}
