<?php

namespace App\Http\Controllers\Backend;

use App\Contact;
use App\DataTables\ContactFormDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    public function list(ContactFormDataTable $dataTable)
    {
        return $dataTable->render('backend.contact-form.list');
    }

    public function update(Request $request, Contact $contact)
    {
        $contact->update([
                'status' => $request->status,
                'note' => $request->note,
        ]);

        return redirect()->route('contacts.list')->withSuccess('แก้ไข Contact Form สำเร็จ');
    }

    public function detail(Contact $contact)
    {
        $contacts = $contact;

        return view('backend.contact-form.detail', compact('contacts'));
    }

    public function destroy(Contact $contact)
    {
        if(! request()->ajax()){
            return response()->json(['error'=>'Can not Delete']);
        }
        $contact->delete();

        return response()->json(['message' => "ลบ Contact Form สำเร็จ"]);
    }

}
