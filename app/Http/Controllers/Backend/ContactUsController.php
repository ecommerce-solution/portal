<?php

namespace App\Http\Controllers\Backend;

use App\ContactUs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function create()
    {
        $contact = [];
        $tempContact = ContactUs::get();
        if ($tempContact->isNotEmpty()) {
            $contact = $tempContact;
        }

        return view('backend.contact-us.index', compact('contact'));
    }

    public function postContact(Request $request)
    {
        $contact_id = $request->contact_id;
        if (empty($contact_id)) {
            $this->validateContactUsCreate($request);
            $data = [
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'time' => $request->time,
                    'email' => $request->email,
                    'address' => $request->address,
                    'tel' => $request->tel,
                    'fax' => $request->fax,
            ];
            ContactUs::Create($data);

            return redirect()->route('contact-us.create')->withSuccess('สร้าง Contact Us สำเร็จ');
        } else {
            $this->validateContactUsEdit($request);
            $data = [
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'time' => $request->time,
                    'email' => $request->email,
                    'address' => $request->address,
                    'tel' => $request->tel,
                    'fax' => $request->fax,
            ];

            ContactUs::where('id', $request->contact_id)->update($data);

            return redirect()->route('contact-us.create')->withSuccess('แก้ไข Contact Us สำเร็จ');
        }
    }

    public function validateContactUsCreate($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'email' => 'required',
                'address' => 'required',
                'tel' => 'required',
                'fax' => 'required',
        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'email' => 'Email',
                'address' => 'Address',
                'tel' => 'Tel',
                'fax' => 'Fax',
        ]);
    }

    public function validateContactUsEdit($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'email' => 'required',
                'address' => 'required',
                'tel' => 'required',
                'fax' => 'required',
        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'email' => 'Email',
                'address' => 'Address',
                'tel' => 'Tel',
                'fax' => 'Fax',
        ]);
    }

}
