<?php

namespace App\Http\Controllers\Backend\Merchant;

use App\DataTables\Merchant\OrderDataTable;
use App\Exports\SummaryExport;
use App\Http\Controllers\Controller;
use App\Order;
use Maatwebsite\Excel\Facades\Excel;
use App\User;


class OrderController extends Controller
{
    public function list(OrderDataTable $dataTable)
{
    $users = User::all();
    return $dataTable->render('backend.merchant.orders.list',compact('users'));
}

    public function export()
    {
        return Excel::download(new SummaryExport,'Order-report.xlsx');
    }
    public function detail(Order $order)
    {
        $order = $order->with('orderDetails.products')->find($order->id);

        return view('backend.merchant.orders.detail', compact('order','details'));
    }
}
