<?php

namespace App\Http\Controllers\Backend\Merchant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Product;
use App\DataTables\Merchant\ProductDataTable;
use App\Stock;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function list(ProductDataTable $dataTable)
    {
        $categories = ProductCategory::all();

        return $dataTable->render('backend.merchant.products.list', compact('categories'));
    }

    public function create()
    {
        $product_categories = ProductCategory::all();

        return view('backend.merchant.products.create', compact('product_categories'));
    }

    public function store(Request $request)
    {
        $this->validateCreate($request);
        $create = [
            'name' => $request->name,
            'product_category_id' => $request->product_category_id,
            'price' => $request->price,
            'qty' => $request->qty,
            'description' => $request->description,
        ];
        DB::transaction(function () use ($request, $create) {
            if (!empty($request->image)) {
                $img = Storage::disk('uploads')->put('products',
                    $request->file('image'));
                $create += ['image' => $img,];
            } else {
                $create += ['image' => 'NULL',];
            }
            $product = Product::create($create);
            Stock::create(
                [
                    'product_id' => $product->id,
                    'type' => 'increase',
                    'quantity' => $request->qty,
                    'total' => $request->qty,
                    'user_id' => auth()->user()->id,
                ]
            );
        });
        return redirect()->route('products.list')->withSuccess('สร้าง Product สำเร็จ');
    }

    public function validateCreate($request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'product_category_id' => 'required|string|max:255',
            'price' => 'required|numeric|between:0,999999.99',
            'qty' => 'required|numeric|integer',
        ];
        $customAttributes = [
            'name' => 'Name',
            'product_category_id' => 'Category',
            'price' => 'Price / Unit',
            'qty' => 'Quantity / Unit',
        ];

        if (!empty($request->image)) {
            $rules += ['image' => 'required|mimes:jpeg,png|max:1024',];
            $customAttributes += ['image' => 'Image',];
        };
        $request->validate($rules, [], $customAttributes);
    }

    public function edit(Product $product)
    {
        $product_categories = ProductCategory::all();
        return view('backend.merchant.products.edit', compact('product', 'product_categories'));
    }

    public function update(Product $product, Request $request)
    {
        $before_product = $product->toArray();
        $this->validateCreate($request);
        $update = [
            'name' => $request->name,
            'product_category_id' => $request->product_category_id,
            'price' => $request->price,
            'qty' => $request->qty,
            'description' => $request->description,
        ];

        if (!empty($request->image)) {
            if ($product != 'NULL') {
                Storage::disk('uploads')->delete($product->image);
            }
            $img = Storage::disk('uploads')->put('products',
                $request->file('image'));
            $update += ['image' => $img,];
        }
        $products = $product->update($update);
        if ($products) {

            if ($request->qty < $before_product['qty']) {
                $qty = $before_product['qty'] - $request->qty;
                $type = 'decrease';
            } elseif ($request->qty > $before_product['qty']) {
                $qty = $request->qty - $before_product['qty'];
                $type = 'increase';
            } else {
                return redirect()->route('products.list')->withSuccess('แก้ไข Product สำเร็จ');
            }
        }
        Stock::create(
            [
                'product_id' => $before_product['id'],
                'type' => $type,
                'quantity' => $qty,
                'total' => $request->qty,
                'user_id' => auth()->user()->id,
            ]
        );

        return redirect()->route('products.list')->withSuccess('แก้ไข Product สำเร็จ');
    }

    public function destroy(Product $product)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $product->delete();
        Storage::disk('uploads')->delete($product->image);

        return response()->json(['message' => "ลบ Product Category สำเร็จ"]);
    }
}
