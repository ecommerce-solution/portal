<?php

namespace App\Http\Controllers\Backend\Merchant;

use App\DataTables\Merchant\PurchaseHistoryDataTable;
use App\OrderDetail;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchaseHistoryController extends Controller
{
    public function list(PurchaseHistoryDataTable $dataTable)
    {
        return $dataTable->render('backend.merchant.purchase-histories.list');
    }

    public function detail(Order $order)
    {
        $order = $order->with('orderDetails.products')->find($order->id);
        return view('backend.merchant.purchase-histories.detail', compact('order','details'));
    }
}
