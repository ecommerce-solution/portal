<?php

namespace App\Http\Controllers\Backend\Merchant;

use App\DataTables\Merchant\UserReportDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;


class UserReportController extends Controller
{
    public function  list(UserReportDataTable $dataTables)
    {
        $users = User::all();
        return $dataTables->render('backend.merchant.reports.userReport',compact('users'));
    }
}
