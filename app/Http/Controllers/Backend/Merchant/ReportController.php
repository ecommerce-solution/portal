<?php

namespace App\Http\Controllers\Backend\Merchant;


use App\DataTables\Merchant\OrderReportDataTable;
use App\DataTables\Merchant\ProductReportDataTable;
use App\DataTables\Merchant\UserReportDataTable;
use App\DataTables\Merchant\StockDataTable;
use App\Exports\OrderExport;
use App\Exports\SummaryExport;
use App\Order;
use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\Product;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function list(UserReportDataTable $dataTable)
    {
        $order_query = Order::with('user')->where('status', '=', 'unpaid')->get();
        $orders = collect($order_query)->groupBy('user_id');

        return view('backend.merchant.reports.summary', compact('orders'));
    }

    public function selectOrder(Request $request)
    {
        $from = $request->input('fromDate');
        $to = $request->input('toDate');
        $perPage = 15;
        if (!empty($from) && !empty($to)) {
            $from_date = Carbon::createFromFormat('d/m/Y', $request->get('fromDate'))->startOfDay();
            $to_date = Carbon::createFromFormat('d/m/Y', $request->get('toDate'))->endOfDay();
        } else {
            $from_date = Carbon::create('1995', '1', '1', '00', '00', '00');
            $to_date = Carbon::now();
        }

        $order_query = Order::query()
            ->whereBetween('created_at', [$from_date, $to_date])
            ->where('status', '=', 'unpaid')
            ->orderBy('created_at', 'DESC');

        $orders = $order_query->paginate($perPage);
        return view('backend.merchant.reports.selectOrder', compact('orders'));
    }

    public function selectProduct()
    {
        $products = Product::all();
        $perPage = 15;
        $allOrders = OrderDetail::query()
                ->with('order')
                ->orderBy('created_at', 'DESC')
                ->paginate($perPage);
        $page = (int)\request('page') ?: 1;

        return view('backend.merchant.reports.selectProduct', compact('allOrders', 'products', 'page', 'perPage'));
    }

    public function sendProduct(Request $request)
    {
        $from = $request->input('fromDate');
        $to = $request->input('toDate');
        $product = $request->input('productSelected');

        if (!empty($from) && !empty($to) && !empty($product)) {
            $perPage = 15;
            $from_date = Carbon::createFromFormat('d/m/Y', $request->get('fromDate'))->startOfDay();
            $to_date = Carbon::createFromFormat('d/m/Y', $request->get('toDate'))->endOfDay();
            $product = $request->get('productSelected');
            $order_details = OrderDetail::query()
                    ->where('product_name', $product)
                    ->whereBetween('created_at', [$from_date, $to_date])
                    ->orderBy('created_at', 'DESC')
                    ->with('order')
                    ->paginate($perPage);
            $page = (int)\request('page') ?: 1;

            return view('backend.merchant.reports.productSelected',
                    compact('order_details', 'page', 'perPage', 'product'));
        } elseif (!empty($from) && !empty($to) && empty($product)) {
            $perPage = 15;
            $product = Product::all();
            $from_date = Carbon::createFromFormat('d/m/Y', $request->get('fromDate'))->startOfDay();
            $to_date = Carbon::createFromFormat('d/m/Y', $request->get('toDate'))->endOfDay();
            $order_details = OrderDetail::query()
                    ->whereBetween('created_at', [$from_date, $to_date])
                    ->orderBy('created_at', 'DESC')
                    ->with('order')
                    ->paginate($perPage);
            $page = (int)\request('page') ?: 1;

            return view('backend.merchant.reports.productSelected',
                    compact('order_details', 'perPage', 'product', 'page'));
        } elseif (empty($from) && empty($to) && !empty($product)) {
            $perPage = 15;
            $product = Product::all();
            $query_product = $request->get('productSelected');
            $order_details = OrderDetail::query()
                    ->where('product_name', '=', $query_product)
                    ->with('order')
                    ->paginate($perPage);
            $page = (int)\request('page') ?: 1;

            return view('backend.merchant.reports.productSelected',
                    compact('order_details', 'perPage', 'product', 'page'));
        } else {
            $perPage = 15;
            $order_details = OrderDetail::with('order')->paginate($perPage);
            $page = (int)\request('page') ?: 1;
            $product = Product::all();
            return view('backend.merchant.reports.productSelected',
                    compact('order_details', 'perPage', 'product', 'page'));
        }
    }

    public function selectUser()
    {
        $perPage = 15;
        $page = (int)\request('page') ?: 1;
        $users = Order::groupBy('user_name')
                ->select(['user_name', DB::raw('sum(total_price) as total_price')])
                ->paginate($perPage);
        return view('backend.merchant.reports.selectUser', compact('users', 'perPage', 'page'));
    }

    public function userSelected(Request $request)
    {
        $user_select = $request->input('userSelected');
        $from = $request->input('fromDate');
        $to = $request->input('toDate');

        if (!empty($user_select) && !empty($from) && !empty($to)) {
            $from_date = Carbon::createFromFormat('d/m/Y', $request->get('fromDate'))->startOfDay();
            $to_date = Carbon::createFromFormat('d/m/Y', $request->get('toDate'))->endOfDay();
            $user_select = $request->get('userSelected');
            $user = Order::query()
                    ->where('user_name', $user_select)
                    ->whereBetween('created_at', [$from_date, $to_date])
                    ->groupBy('user_name')
                    ->select(['user_name', DB::raw('sum(total_price) as total_price')])
                    ->first();

            return view('backend.merchant.reports.userSelected',
                compact('user', 'page', 'perPage', 'from_date', 'to_date'));


        }
        if (empty($user_select) && !empty($from) && !empty($to)) {
            $perPage = 15;
            $page = (int)\request('page') ?: 1;
            $from_date = Carbon::createFromFormat('d/m/Y', $request->get('fromDate'))->startOfDay();
            $to_date = Carbon::createFromFormat('d/m/Y', $request->get('toDate'))->endOfDay();
            $users = Order::query()
                ->whereBetween('created_at', [$from_date, $to_date])
                ->groupBy('user_name')
                ->select(['user_name', DB::raw('sum(total_price) as total_price')])
                ->paginate($perPage);

            return view('backend.merchant.reports.userSelected',
                compact('users', 'perPage', 'page', 'from_date', 'to_date'));
        } else {
            return redirect()->back()->with('error', 'กรุณากรอกวันที่ให้ครบ');
        }
    }

    public function exportSummary(Request $request)
    {

        return Excel::download(new $orders, 'Summary-report.xlsx');
    }

    public function exportOrder()
    {
        return Excel::download(new OrderExport, 'Order-report.xlsx');
    }

    public function stock(StockDataTable $dataTable)
    {
        return $dataTable->render('backend.merchant.reports.stock');
    }
    public function productReport(ProductReportDataTable $dataTable)
    {
        $products = Product::all();

        return $dataTable->render('backend.merchant.reports.productReport',compact('products'));
    }
    public function orderReport(OrderReportDataTable $dataTable)
    {
        return $dataTable->render('backend.merchant.reports.orderReport');
    }
}
