<?php

namespace App\Http\Controllers\Backend\Merchant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\DataTables\Merchant\BalanceDataTable;
use App\Order;
use Carbon\Carbon;


class BalanceController extends Controller
{
    public function list(BalanceDataTable $dataTable)
    {
//        $orders = Order::get()->where('status', 'unpaid')
//            ->groupBy('user_id');
//        if (count($orders)) {
//            foreach ($orders as $group) {
//                $grouped[] = $group->groupBy(function ($d) {
//                    return Carbon::parse($d->created_at)->format('m/Y');
//                })->values();
//            }
//            $query = collect();
//            foreach ($grouped as $group) {
//                foreach ($group as $a) {
//                    $query->push([
//                        'user_id' => $a[0]->user_id,
//                        'date' => $a[0]->created_at->format('m/Y'),
//                        'total_price' => $a->sum('total_price'),
//                        'status' => $a[0]->status
//                    ]);
//                }
//            };
//        }
//
//        dump($query);
//        $ad = Order::where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),"2018-12")->get();
//        dd($ad);

        return $dataTable->render('backend.merchant.balances.list');
    }

    public function detail(Request $request, $user_id)
    {
        try {
            $date = Carbon::createFromFormat('m/Y', $request->date)->format('Y-m');
        } catch (\Exception $e) {
            return response()->view('error.404');
        }

        $orders = Order::where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"), $date)
            ->where('status', $request->status)
            ->where('user_id', $user_id)
            ->with('orderDetails.products')
            ->orderBy('created_at', 'DESC')
            ->get();
        $total_price = $orders->sum('total_price');
        $status = $request->status;

        return view('backend.merchant.balances.detail', compact('orders', 'total_price', 'user_id', 'date', 'status'));
    }

    public function update(Request $request)
    {
        if (!($request->status == 'unpaid' || $request->status == 'paid' &&
            $request->status_new == 'unpaid' || $request->status_new == 'paid')) {
            return response()->view('error.404');
        }
        try {
            $date = Carbon::createFromFormat('Y-m', $request->date)->format('Y-m');
        } catch (\Exception $e) {
            return response()->view('error.404');
        }

        $orders = Order::where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"), $date)
            ->where('status', $request->status)
            ->where('user_id', $request->user_id);

        $orders->update(['status' => $request->status_new]);

        return redirect()->route('balances.list')->withSuccess('แก้ไข Balance สำเร็จ');

    }

}
