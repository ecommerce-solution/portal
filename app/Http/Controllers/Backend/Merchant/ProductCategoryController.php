<?php

namespace App\Http\Controllers\Backend\Merchant;

use App\DataTables\Merchant\ProductCategoryDataTable;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    public function list(ProductCategoryDataTable $dataTable)
    {
        return $dataTable->render('backend.merchant.product-categories.list');
    }

    public function create()
    {
        return view('backend.merchant.product-categories.create');
    }

    public function store(Request $request)
    {
        $this->validateRole($request);
        ProductCategory::create([
                'title' => $request->title,
        ]);
        return redirect()->route('product-categories.list')->withSuccess('สร้างหมวดหมู่สำเร็จ');
    }

    public function validateRole($request,$productCategory = null)
    {
        if ($productCategory == null) {
            $request->validate([
                    'title' => 'required',
            ], [], [
                    'title' => 'Name',
            ]);
        } else {
            $request->validate([
                    'title' => 'required',
            ], [], [
                    'title' => 'Name',
            ]);
        }
    }

    public function edit(ProductCategory $productCategory)
    {
        return view('backend.merchant.product-categories.edit',compact('productCategory'));
    }

    public function update(ProductCategory $productCategory, Request $request)
    {
        $this->validateRole($request, $productCategory);
        $productCategory->update([
                'title' => $request->title,
        ]);

        return redirect()->route('product-categories.list')->withSuccess('แก้ไข Product Category สำเร็จ');
    }

    public function destroy(ProductCategory $productCategory)
    {
        $productCategory = ProductCategory::with('products')
                ->where('id', $productCategory->id)
                ->first();
        if (count($productCategory->products) > 0 || !request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $productCategory->delete();
        return response()->json(['message' => "ลบ Product Category สำเร็จ"]);
    }
}
