<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\HolidayDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Holiday;
use Carbon\Carbon;

class HolidayController extends Controller
{
    public function list(HolidayDataTable $dataTable)
    {
        return $dataTable->render('backend.holidays.list');
    }

    public function create()
    {
        return view('backend.holidays.create');
    }

    public function store(Request $request)
    {
        $this->validateHoliday($request);
        $start_date = Carbon::createFromFormat('d/m/Y', $request->start_date);
        Holiday::create([
            'title' => $request->title,
            'description' => $request->description,
            'start_date' => $start_date,
        ]);

        return redirect()->route('holidays.list')->withSuccess('สร้าง Holiday สำเร็จ');
    }

    public function validateHoliday($request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'start_date' => 'required|date_format:d/m/Y',
        ], [], [
            'title' => 'Title',
            'description' => 'Description',
            'start_date' => 'Start date',
        ]);
    }

    public function destroy(Holiday $holiday)
    {
        if(!request()->ajax()){
            return response()->json(['error'=>'ไม่สามารถลบข้อมูลได้']);
        }
        $holiday->delete();

        return response()->json(['message' => "ลบ Holiday สำเร็จ"]);
    }

    public function edit(Holiday $holiday)
    {
        $list['start_date'] = Carbon::parse($holiday->start_date)->format('d/m/Y');

        return view('backend.holidays.edit', compact('holiday', 'list'));
    }

    public function update(Holiday $holiday, Request $request)
    {
        $this->validateHoliday($request);
        $start_date = Carbon::createFromFormat('d/m/Y', $request->start_date);
        $holiday->update([
            'title' => $request->title,
            'description' => $request->description,
            'start_date' => $start_date,
        ]);

        return redirect()->route('holidays.list')->withSuccess('แก้ไข Holiday สำเร็จ');
    }

}
