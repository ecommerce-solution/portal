<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;
use Illuminate\Support\Facades\Storage;

class AboutUsController extends Controller
{
    public function getAbout()
    {
        $about = [];
        $oAbout = About::get();
        if ($oAbout->isNotEmpty()) {
            $about = $oAbout;
        }

        return view('backend.about-us.index', compact('about'));
    }

    public function postAbout(Request $request)
    {
        $id = $request->id;
        if (!empty($id)) {
            $this->validateAboutUsEdit($request);
            $data = [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
            ];
            if (!empty($request->image)) {
                $about = About::all()->where('id', $request->id)->first();
                Storage::disk('uploads')->delete($about->image);
                $pathImage = Storage::disk('uploads')->put('about-us', $request->file('image'));
                $data['image'] = $pathImage;
            }
            About::where('id', $request->id)->update($data);

            return redirect()->route('about-us.index')->withSuccess('แก้ไข About Us สำเร็จ');
        } else {

            $this->validateAboutUsCreate($request);
            $pathImage = Storage::disk('uploads')->put('about-us', $request->file('image'));
            $data = [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'image' => $pathImage,
            ];
            About::Create($data);

            return redirect()->route('about-us.index')->withSuccess('สร้าง About Us สำเร็จ');
        }
    }

    public function validateAboutUsCreate($request)
    {
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpeg,png|max:1024',
        ], [], [
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'image' => 'Image',
        ]);
    }

    public function validateAboutUsEdit($request)
    {
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,png|max:1024',
        ], [], [
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'image' => 'Image',
        ]);
    }
}