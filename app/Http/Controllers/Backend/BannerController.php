<?php

namespace App\Http\Controllers\Backend;

use App\Banner;
use App\DataTables\BannerDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    public function list(BannerDataTable $dataTable)
    {
        return $dataTable->render('backend.banners.list');
    }

    public function destroy(Banner $banner)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $banner->delete();
        Storage::disk('uploads')->delete($banner->image);

        return response()->json(['message' => "ลบ Banner สำเร็จ"]);
    }

    public function create()
    {
        return view('backend.banners.create');
    }

    public function store(Request $request)
    {
        $this->validateCreate($request);
        $image = Storage::disk('uploads')->put('banners', $request->file('image'));
        Banner::create([
            'title' => $request->title,
            'short_description' => $request->short_description,
            'image' => $image,
        ]);

        return redirect()->route('banners.list')->withSuccess('สร้าง Banner สำเร็จ');
    }

    public function edit(Banner $banner)
    {
        return view('backend.banners.edit', compact('banner'));
    }

    public function update(Banner $banner, Request $request)
    {
        $this->validateEdit($request);
        if (!empty($request->image)) {
            Storage::disk('uploads')->delete($banner->image);
            $image = Storage::disk('uploads')->put('banners', $request->file('image'));
            $banner->update([
                'title' => $request->title,
                'short_description' => $request->short_description,
                'image' => $image,
            ]);
        } else {
            $banner->update(['title' => $request->title, 'short_description' => $request->short_description]);
        }

        return redirect()->route('banners.list')->withSuccess('แก้ไข Banner สำเร็จ');
    }

    public function validateEdit($request)
    {
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'image' => 'mimes:jpeg,png|max:1024',
        ], [], [
            'title' => 'Title',
            'short_description' => 'Short Description',
            'image' => 'Image',
        ]);
    }

    public function validateCreate($request)
    {
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'image' => 'required|mimes:jpeg,png|max:1024',
        ], [], [
            'title' => 'Title',
            'short_description' => 'Short Description',
            'image' => 'Image',
        ]);
    }
}

