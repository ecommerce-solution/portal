<?php

namespace App\Http\Controllers\Backend;

use App\Career;
use App\DataTables\CareerDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CareerController extends Controller
{
    public function list(CareerDataTable $dataTable)
    {
        return $dataTable->render('backend.careers.list');
    }

    public function create(Career $careers)
    {
        return view('backend.careers.create', compact('careers'));
    }

    public function store(Request $request)
    {
        $this->validateCreate($request);
        $icon = Storage::disk('uploads')->put('career_icon', $request->file('icon'));
        Career::create([
            'role_name' => $request->role_name,
            'icon' => $icon,
            'description' => $request->description,
            'position' => $request->position,
            'status' => $request->status,
            'short_description' => $request->short_description,
        ]);

        return redirect()->route('careers.list')->withSuccess('สร้าง Career สำเร็จ');

    }

    public function edit(Career $career)
    {
        return view('backend.careers.edit', compact('career'));
    }

    public function update(Career $career, Request $request)
    {
        $this->validateField($request);
        if (!empty($request->icon)) {
            Storage::disk('uploads')->delete($request->icon);
            $icon = Storage::disk('uploads')->put('career_icon', $request->file('icon'));
            $career->update([
                'role_name' => $request->role_name,
                'icon' => $icon,
                'description' => $request->description,
                'position' => $request->position,
                'status' => $request->status,
                'short_description' => $request->short_description,
            ]);
        } else {
            $career->update([
                'role_name' => $request->role_name,
                'description' => $request->description,
                'position' => $request->position,
                'status' => $request->status,
                'short_description' => $request->short_description,
            ]);
        }

        return redirect()->route('careers.list')->withSuccess('แก้ไข Career สำเร็จ');

    }

    public function destroy(Career $career)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }

        $career->delete();
        Storage::disk('uploads')->delete($career->icon);

        return response()->json(['message' => "ลบ Career สำเร็จ"]);
    }

    public function validateField($request)
    {
        $request->validate([
            'role_name' => 'required',
            'icon' => 'sometimes|mimes:jpeg,png|max:1024',
            'description' => 'required',
            'position' => 'required|numeric|min:1',
            'status' => 'required',
            'short_description' => 'required',
        ], [], [
            'role_name' => 'Role',
            'icon' => 'Icon',
            'description' => 'Description',
            'position' => 'Position',
            'status' => 'Status',
            'short_description' => 'Short Description',
        ]);
    }

    public function validateCreate($request)
    {
        $request->validate([
            'role_name' => 'required',
            'icon' => ' required|mimes:jpeg,png|max:1024',
            'description' => 'required',
            'position' => 'required|numeric|min:1',
            'status' => 'required',
            'short_description' => 'required',
        ], [], [
            'role_name' => 'Role',
            'icon' => 'Icon',
            'description' => 'Description',
            'position' => 'Position',
            'status' => 'Status',
            'short_description' => 'Short Description',
        ]);
    }
}
