<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\Order;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member = auth()->user();
        $now = Carbon::now()->format('Y-m-d');
        $products = Product::all();
        $users = User::all();
//        $sumtodays = DB::select('select * from orders WHERE date(created_at)=curdate()');
        $sumtodays = Order::where('created_at', '>=', $now.' 00:00:00')
            ->where('created_at', '<=', $now.' 23:59:59')
            ->orderBy('created_at','desc')
            ->get();
        $orders = Order::where('user_id',$member->id)
            ->where('created_at', '>=', $now.' 00:00:00')
            ->where('created_at', '<=', $now.' 23:59:59')
            ->orderBy('created_at','desc')
            ->get();
        return view('backend.homepage', compact('users', 'products', 'orders','sumtodays'));
    }
}
