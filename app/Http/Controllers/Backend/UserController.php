<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class UserController extends Controller
{

    public function list(UserDataTable $dataTable)
    {
        return $dataTable->render('backend.users.list');
    }

    public function create()
    {
        $roles = Role::all();
        return view('backend.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['id_card'] = str_replace(" ", "", "$request->id_card");
        $data['phone_number'] = str_replace("-", "", "$request->phone_number");
        $data['bank_account_no'] = str_replace("-", "", "$request->bank_account_no");
        $this->validateCreate($data)->validate();
        $create = [
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_card' => $data['id_card'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'nickname' => $data['nickname'],
            'phone_number' => $data['phone_number'],
            'bank_account_no' => $data['bank_account_no'],
            'bank_branch' => $data['bank_branch'],
        ];
        if (!empty($data['birth_date'])) {
            $birth_date = Carbon::createFromFormat('d/m/Y', $data['birth_date']);
            $create += ['birth_date' => $birth_date];
        }
        if (!empty($data['start_work_date'])) {
            $start_work_date = Carbon::createFromFormat('d/m/Y', $data['start_work_date']);
            $create += ['start_work_date' => $start_work_date];
        }
        if (empty($data['blood_type'])) {
            $create += ['blood_type' => null];
        }else {
            $create += ['blood_type' => $data['blood_type']];
        }
        if (empty($data['role_id'])) {
            $create += ['role_id' => 3];
        } else {
            $create += ['role_id' => $data['role_id']];
        }

        User::create($create);

        return redirect()->route('users.list')->withSuccess('สร้าง User สำเร็จ');
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        if (auth()->user()->role_id == 1 || auth()->user()->id == $user->id) {
            return view('backend.users.edit', compact('user', 'roles'));
        } else {
            return response()->view('error.404');
        }
    }

    public function update(User $user, UserRequest $request)
    {
        $data = $request->all();
        $data['id_card'] = str_replace(" ", "", "$request->id_card");
        $data['phone_number'] = str_replace("-", "", "$request->phone_number");
        $data['bank_account_no'] = str_replace("-", "", "$request->bank_account_no");

//        $this->validateEdit($data, $user);
        $editUser = [
            'email' => $data['email'],
            'id_card' => $data['id_card'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'nickname' => $data['nickname'],
            'phone_number' => $data['phone_number'],
            'bank_account_no' => $data['bank_account_no'],
            'bank_branch' => $data['bank_branch'],
        ];

        if (!empty($data['birth_date'])) {
            $birth_date = Carbon::createFromFormat('d/m/Y', $data['birth_date']);
            $editUser += ['birth_date' => $birth_date];
        }
        if (!empty($data['start_work_date'])) {
            $start_work_date = Carbon::createFromFormat('d/m/Y', $data['start_work_date']);
            $editUser += ['start_work_date' => $start_work_date];
        }
        if (empty($data['blood_type'])) {
            $editUser += ['blood_type' => null];
        }else {
            $editUser += ['blood_type' => $data['blood_type']];
        }
        if (empty($data['role_id'])) {
            $editUser += ['role_id' => 3];
        } else {
            $editUser += ['role_id' => $data['role_id']];
        }
        if (!empty($request->password)) {
            $newPassword = Hash::make($data['password']);
            $editUser += ['password' => $newPassword,];
        }

        $user->update($editUser);

        if (auth()->user()->role_id != 1) {
            return redirect()->back()->withSuccess('แก้ไข User สำเร็จ');
        }
        return redirect()->route('users.list')->withSuccess('แก้ไข User สำเร็จ');

    }

    public function validateCreate(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'id_card' => 'required|string|min:13|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'nickname' => 'required|string|max:255|unique:users',
            'birth_date' => 'nullable|date_format:d/m/Y',
            'phone_number' => 'nullable',
            'blood_type' => 'nullable|string|max:255',
            'bank_account_no' => 'nullable|string|max:255',
            'bank_branch' => 'nullable|string|max:255',
            'start_work_date' => 'nullable|date_format:d/m/Y',
        ], [], [
            'email' => 'Email',
            'password' => 'Password',
            'id_card' => 'ID Card',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'nickname' => 'Nickname',
            'birth_date' => 'Birth Date',
            'phone_number' => 'Phone Number',
            'blood_type' => 'Blood Type',
            'bank_account_no' => 'Bank Account',
            'bank_branch' => 'Bank Branch',
            'start_work_date' => 'Start Work',
        ]);
    }

//    public function validateEdit($data, $user)
//    {
//        $rules = [
//            'email' => ['required', 'email', Rule::unique('users')->ignore($user),],
//            'id_card' => 'required|string|max:255',
//            'first_name' => 'required|string|max:255',
//            'last_name' => 'required|string|max:255',
//            'nickname' => ['required', 'string', Rule::unique('users')->ignore($user),],
//            'birth_date' => 'nullable|date_format:d/m/Y',
//            'phone_number' => 'nullable|numeric|digits_between:1,10',
//            'blood_type' => 'nullable|string|max:255',
//            'bank_account_no' => 'nullable|string|max:255',
//            'bank_branch' => 'nullable|string|max:255',
//            'start_work_date' => 'nullable|date_format:d/m/Y',
//        ];
//        $customAttributes = [
//            'email' => 'Email',
//            'id_card' => 'ID Card',
//            'first_name' => 'First Name',
//            'last_name' => 'Last Name',
//            'nickname' => 'Nickname',
//            'birth_date' => 'Birth Date',
//            'phone_number' => 'Phone Number',
//            'blood_type' => 'Blood Type',
//            'bank_account_no' => 'Bank Account',
//            'bank_branch' => 'Bank Branch',
//            'start_work_date' => 'Start Work',
//        ];
//
////        if ($user->id == $checkUser->id && !empty($request->password)) {
//        if (!empty($data['password'])) {
//            $rules += ['password' => 'required|string|min:6|confirmed',];
//            $customAttributes += ['password' => 'Password',];
//        };
//        Validator::make($data, $rules, [], $customAttributes)->validate();
//
//    }

    public function destroy(User $user)
    {
        if (!request()->ajax() || (auth()->user()->name) == $user->email) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $user->delete();

        return response()->json(['message' => "ลบ User  สำเร็จ"]);
    }
}
