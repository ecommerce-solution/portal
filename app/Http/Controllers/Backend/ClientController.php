<?php

namespace App\Http\Controllers\Backend;

use App\Client;
use App\DataTables\ClientDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    public function index(ClientDataTable $dataTable)
    {
        return $dataTable->render('backend.clients.list');
    }

    public function create()
    {
        return view('backend.clients.create');
    }

    public function store(Request $request)
    {
        $this->validateFormCreate($request);
        $image = Storage::disk('uploads')->put('clients', $request->file('image'));
        Client::create([
            'name' => $request->name,
            'image' => $image,
            'url' => $request->url,
        ]);

        return redirect()->route('clients.list')->withSuccess('สร้าง Client สำเร็จ');
    }

    public function edit(Client $client)
    {
        return view('backend.clients.edit', compact('client'));
    }

    public function update(Client $client, Request $request)
    {
        $this->validateFormEdit($request);
        if (!empty($request->image)) {
            Storage::disk('uploads')->delete($client->image);
            $image = Storage::disk('uploads')->put('clients', $request->file('image'));
            $client->update([
                'name' => $request->name,
                'image' => $image,
                'url' => $request->url,
            ]);
        } else {
            $client->update([
                'name' => $request->name,
                'url' => $request->url,
            ]);
        }

        return redirect()->route('clients.list')->withSuccess('แก้ไข Client สำเร็จ');
    }

    public function validateFormCreate($request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|mimes:jpeg,png|max:1024',
            'url' => 'url',
        ], [], [
            'name' => 'Name',
            'image' => 'Image',
            'url' => 'URL'
        ]);
    }

    public function validateFormEdit($request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'mimes:jpeg,png|max:1024',
            'url' => 'url',
        ], [], [
            'name' => 'Name',
            'image' => 'Image',
            'url' => 'URL'
        ]);
    }

    public function destroy(Client $client)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $client->delete();
        Storage::disk('uploads')->delete($client->image);

        return response()->json(['message' => "ลบ Client สำเร็จ"]);
    }

}
