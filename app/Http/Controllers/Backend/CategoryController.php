<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\DataTables\CategoryDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    public function list(CategoryDataTable $dataTable)
    {
        return $dataTable->render('backend.categories.list');
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(Request $request)
    {
        $this->validateCategory($request);
        Category::create([
            'name' => $request->name,
            'slug' => $request->slug,
        ]);

        return redirect()->route('categories.list')->withSuccess('สร้างหมวดหมู่สำเร็จ');
    }

    public function validateCategory($request, $category = null)
    {
        if ($category == null) {
            $request->validate([
                'name' => 'required',
                'slug' => 'required|unique:categories',
            ], [], [
                'name' => 'Name',
                'slug' => 'Slug',
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'slug' => ['required', Rule::unique('categories')->ignore($category),],
            ], [], [
                'name' => 'Name',
                'slug' => 'Slug',
            ]);
        }
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit', compact('category'));
    }

    public function update(Category $category, Request $request)
    {
        $this->validateCategory($request, $category);
        $category->update([
            'name' => $request->name,
            'slug' => $request->slug,
        ]);

        return redirect()->route('categories.list')->withSuccess('แก้ไขหมวดหมู่สำเร็จ');
    }

    public function destroy(Category $category)
    {
        $category = Category::with('article')
            ->where('id', $category->id)
            ->first();
        if (count($category->article) > 0 || !request()->ajax()) {
            return response()->json(['error' => 'Can not Delete']);
        }
        $category->delete();

        return response()->json(['message' => "ลบ Category สำเร็จ"]);
    }

}
