<?php

namespace App\Http\Controllers\Backend;

use App\Career;
use App\DataTables\ApplicationFormDataTable;
use App\Exports\ApplicationFormsExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApplicationForm;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ApplicationFormController extends Controller
{
    public function list(ApplicationFormDataTable $dataTable)
    {
        $careers = Career::get();

        return $dataTable->render('backend.application-form.list', compact('careers'));
    }

    public function destroy(ApplicationForm $applicationForm)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $applicationForm->delete();

        return response()->json(['message' => "ลบ Application Form สำเร็จ"]);
    }


    public function detail(ApplicationForm $applicationForm)
    {
        $list['date'] = Carbon::parse($applicationForm->date)->format('d/m/Y');
        $applicationForm = $applicationForm->with('jobSkillTrainings')->find($applicationForm->id);

        return view('backend.application-form.detail', compact('applicationForm', 'list'));
    }

    public function update(ApplicationForm $applicationForm, Request $request)
    {
        $applicationForm->update([
            'status' => $request->status,
        ]);

        return redirect()->route('application-form.list')->withSuccess('แก้ไข Application Form สำเร็จ');
    }

    public function export()
    {
        return Excel::download(new ApplicationFormsExport, 'Application-form-list.xlsx');
    }
}
