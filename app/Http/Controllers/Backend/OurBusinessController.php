<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\OurBusinessDataTable;
use App\OurBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class OurBusinessController extends Controller
{
    public function index(OurBusinessDataTable $dataTable)
    {
        return $dataTable->render('backend.our-businesses.list');
    }

    public function create()
    {
        return view('backend.our-businesses.create');
    }

    public function store(Request $request)
    {
        $this->validateFieldCreate($request);
        $cover_image_path = Storage::disk('uploads')->put('our-business/cover_image',
                $request->file('cover_image'));
        $thumbnail_image_path = Storage::disk('uploads')->put('our-business/thumbnail_image',
                $request->file('thumbnail_image'));

        $data = [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'cover_image' => $cover_image_path,
                'thumbnail_image' => $thumbnail_image_path,
                'url' => $request->url,
                'status' => $request->status,
        ];
        OurBusiness::Create($data);

        return redirect()->route('our-businesses.list')->withSuccess('สร้าง Our Business สำเร็จ');

    }

    public function edit(OurBusiness $ourBusiness)
    {
        return view('backend.our-businesses.edit', compact('ourBusiness'));
    }

    public function update(Request $request, OurBusiness $ourBusiness)
    {
        $this->validateFieldEdit($request);
        if (!empty($request->cover_image)) {
            Storage::disk('uploads')->delete($ourBusiness->cover_image);
            $cover_image_path = Storage::disk('uploads')
                    ->put('our-business/cover_image', $request->file('cover_image'));

            $ourBusiness->update([
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'description' => $request->description,
                    'cover_image' => $cover_image_path,
                    'url' => $request->url,
                    'status' => $request->status,
            ]);
        }
        if (!empty($request->thumbnail_image)) {
            Storage::disk('uploads')->delete($ourBusiness->thumbnail_image);
            $thumbnail_image_path = Storage::disk('uploads')
                    ->put('our-business/thumbnail_image', $request->file('thumbnail_image'));
            $ourBusiness->update([
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'description' => $request->description,
                    'thumbnail_image' => $thumbnail_image_path,
                    'url' => $request->url,
                    'status' => $request->status,
            ]);
        } else {
            $ourBusiness->update([
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'description' => $request->description,
                    'url' => $request->url,
                    'status' => $request->status,
            ]);
        }

        return redirect()->route('our-businesses.list')->withSuccess('แก้ไข Our Business สำเร็จ');
    }

    public function destroy(OurBusiness $ourBusiness)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $ourBusiness->delete();
        Storage::disk('uploads')->delete($ourBusiness->cover_image);
        Storage::disk('uploads')->delete($ourBusiness->thumbnail_image);

        return response()->json(['message' => "ลบ Our Business สำเร็จ"]);
    }

    public function validateFieldCreate($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'description' => 'required',
                'cover_image' => 'required|mimes:jpeg,png|max:1024',
                'thumbnail_image' => 'required|mimes:jpeg,png|max:1024',
                'status' => 'required',
                'url' => 'url'

        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'description' => 'Description',
                'cover_image' => 'Cover Image',
                'thumbnail_image' => 'Thumbnail Image',
                'status' => 'Status',
                'url' => 'URL'
        ]);
    }

    public function validateFieldEdit($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'description' => 'required',
                'cover_image' => 'sometimes|mimes:jpeg,png|max:1024',
                'thumbnail_image' => 'sometimes|mimes:jpeg,png|max:1024',
                'status' => 'required',
                'url' => 'url'

        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'description' => 'Description',
                'cover_image' => 'Cover Image',
                'thumbnail_image' => 'Thumbnail Image',
                'status' => 'Status',
                'url' => 'URL'
        ]);
    }
}
