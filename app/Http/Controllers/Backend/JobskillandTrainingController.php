<?php

namespace App\Http\Controllers\Backend;

use App\JobSkillTraining;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobskillandTrainingController extends Controller
{
    public function list()
    {
        $jobskill = JobSkillTraining::query()->orderBy('updated_at', 'desc')
            ->with('careerForm')
            ->paginate(20);

        return view('backend.job-skill-training.list', compact('jobskill'));
    }

}
