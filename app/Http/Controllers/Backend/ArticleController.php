<?php

namespace App\Http\Controllers\Backend;

use App\Article;
use App\Category;
use App\DataTables\ArticleDataTable;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function list(ArticleDataTable $dataTable)
    {
        return $dataTable->render('backend.articles.list');
    }

    public function create()
    {
        $categories = Category::all();
        $articles = Article::all();

        return view('backend.articles.create', compact('articles', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validateFieldCreate($request);
        $cover_image = Storage::disk('uploads')->put('articles/cover_image',
            $request->file('cover_image'));
        $thumbnail_image = Storage::disk('uploads')->put('articles/thumbnail_image',
            $request->file('thumbnail_image'));
        $publish_date = Carbon::createFromFormat('d/m/Y H:i', $request->publish_date);
        $data = [
            'title' => $request->title,
            'short_description' => $request->short_description,
            'description' => $request->description,
            'publish_date' => $publish_date,
            'status' => $request->status,
            'category_id' => $request->category_id,
            'cover_image' => $cover_image,
            'thumbnail_image' => $thumbnail_image,
        ];
        Article::Create($data);

        return redirect()->route('articles.list')->withSuccess('สร้าง Article สำเร็จ');
    }

    public function edit(Article $article)
    {
        $categories = Category::query()->get();

        return view('backend.articles.edit', compact('article', 'categories'));
    }

    public function update(Article $article, Request $request)
    {
        $this->validateFieldEdit($request);
        $publish_date = Carbon::createFromFormat('d/m/Y H:i', $request->publish_date);
        if (!empty($request->cover_image)) {
            Storage::disk('uploads')->delete($article->cover_image);
            $cover_image = Storage::disk('uploads')
                ->put('articles/cover_image', $request->file('cover_image'));
            $article->update([
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'publish_date' => $publish_date,
                'status' => $request->status,
                'category_id' => $request->category_id,
                'cover_image' => $cover_image,
            ]);
        }

        if (!empty($request->thumbnail_image)) {
            Storage::disk('uploads')->delete($article->thumbnail_image);
            $thumbnail_image = Storage::disk('uploads')->put('articles/thumbnail_image',
                $request->file('thumbnail_image'));
            $article->update([
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'publish_date' => $publish_date,
                'status' => $request->status,
                'category_id' => $request->category_id,
                'thumbnail_image' => $thumbnail_image,
            ]);
        } else {
            $article->update([
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'publish_date' => $publish_date,
                'status' => $request->status,
                'category_id' => $request->category_id,
            ]);
        }

        return redirect()->route('articles.list')->withSuccess('แก้ไข Article สำเร็จ');

    }

    public function destroy(Article $article)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $article->delete();
        Storage::disk('uploads')->delete($article->cover_image);
        Storage::disk('uploads')->delete($article->thumbnail_image);

        return response()->json(['message' => "ลบ Article สำเร็จ"]);
    }

    public function validateFieldCreate($request)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'publish_date' => 'required',
            'status' => 'required',
            'cover_image' => 'required|mimes:jpeg,png|max:1024',
            'thumbnail_image' => 'required|mimes:jpeg,png|max:1024',
        ], [], [
            'category_id' => 'Category',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'publish_date' => 'Publish Date',
            'status' => 'Status',
            'cover_image' => 'Cover Image',
            'thumbnail_image' => 'Thumbnail Image',
        ]);
    }

    public function validateFieldEdit($request)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'publish_date' => 'required',
            'status' => 'required',
            'cover_image' => 'sometimes|mimes:jpeg,png|max:1024',
            'thumbnail_image' => 'sometimes|mimes:jpeg,png|max:1024',
        ], [], [
            'category_id' => 'Category',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'publish_date' => 'Publish Date',
            'status' => 'Status',
            'cover_image' => 'Cover Image',
            'thumbnail_image' => 'Thumbnail Image',
        ]);
    }
}
