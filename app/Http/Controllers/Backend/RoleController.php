<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\RoleDataTable;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function list(RoleDataTable $dataTable)
    {
        return $dataTable->render('backend.roles.list');
    }

    public function create()
    {
        return view('backend.roles.create');
    }

    public function store(Request $request)
    {
        $this->validateRole($request);
        Role::create([
            'name' => $request->name,
        ]);
        return redirect()->route('roles.list')->withSuccess('สร้างหมวดหมู่สำเร็จ');
    }

    public function validateRole($request,$role = null)
    {
        if ($role == null) {
            $request->validate([
                'name' => 'required',
            ], [], [
                'name' => 'Name',
            ]);
        } else {
            $request->validate([
                'name' => 'required',
            ], [], [
                'name' => 'Name',
            ]);
        }
    }
    public function edit(Role $role)
    {
        return view('backend.roles.edit', compact('role'));
    }

    public function update(Role $role, Request $request)
    {
        $this->validateRole($request, $role);
        $role->update([
            'name' => $request->name,
        ]);

        return redirect()->route('roles.list')->withSuccess('แก้ไขสิทธิ์สำเร็จ');
    }

    public function destroy(Role $role)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $role->delete();
        return response()->json(['message' => "ลบ Role สำเร็จ"]);
    }
}
