<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\OurServiceDataTable;
use App\OurService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class OurServiceController extends Controller
{
    public function index(OurServiceDataTable $dataTable)
    {
        return $dataTable->render('backend.our-services.list');
    }

    public function create()
    {
        return view('backend.our-services.create');
    }

    public function store(Request $request)
    {
        $this->validateFieldCreate($request);
        $image = Storage::disk('uploads')->put('our-service',
                $request->file('image'));
        $data = [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'image' => $image,
                'status' => $request->status,
        ];
        OurService::Create($data);

        return redirect()->route('our-services.list')->withSuccess('สร้าง Our Service สำเร็จ');

    }

    public function edit(OurService $ourService)
    {
        return view('backend.our-services.edit', compact('ourService'));
    }

    public function update(Request $request, OurService $ourService)
    {
        $this->validateFieldEdit($request);
        if (!empty($request->image)) {
            Storage::disk('uploads')->delete($ourService->image);
            $image = Storage::disk('uploads')
                    ->put('our-service', $request->file('image'));

            $ourService->update([
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'description' => $request->description,
                    'image' => $image,
                    'status' => $request->status,
            ]);
        } else {
            $ourService->update([
                    'title' => $request->title,
                    'short_description' => $request->short_description,
                    'description' => $request->description,
                    'status' => $request->status,
            ]);
        }

        return redirect()->route('our-services.list')->withSuccess('แก้ไข Our Service สำเร็จ');
    }

    public function destroy(OurService $ourService)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ไม่สามารถลบข้อมูลได้']);
        }
        $ourService->delete();
        Storage::disk('uploads')->delete($ourService->image);

        return response()->json(['message' => "ลบ Our Service สำเร็จ"]);
    }

    public function validateFieldCreate($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'description' => 'required',
                'image' => 'required|mimes:jpeg,png|max:1024',
                'status' => 'required',

        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'description' => 'Description',
                'image' => 'Image',
                'status' => 'Status',
        ]);
    }

    public function validateFieldEdit($request)
    {
        $request->validate([
                'title' => 'required',
                'short_description' => 'required',
                'description' => 'required',
                'image' => 'sometimes|mimes:jpeg,png|max:1024',
                'status' => 'required',

        ], [], [
                'title' => 'Title',
                'short_description' => 'Short Description',
                'description' => 'Description',
                'image' => 'Image',
                'status' => 'Status'
        ]);
    }
}
