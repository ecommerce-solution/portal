<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Mail\RegisterMail;
use App\Mail\ResetPassword;
use App\PasswordReset;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function checkLogin(Request $request)
    {
        $this->ValidateLogin($request);
        $email = $request->input('email');
        $password = $request->input('password');
            $checkLogin = User::where('email', $email)->first();
        if (empty($checkLogin)) {
            return redirect()->back()->withError('Email and Password is incorrect');
        } else {
            if (Hash::check($password, $checkLogin->password)) {
                $userProfile = [
                    "id" => $checkLogin->id,
                    "name" => $checkLogin->name,
                    "email" => $checkLogin->email,
                ];
                $request->session()->put('$userProfile', $userProfile);

                return redirect()->route('homepage');
            } else {
                return redirect()->back()->withError('Email and Password is incorrect');
            }
        }
    }

    public function saveRegister(Request $request)
    {
        $password = bcrypt($request->password);
        $this->validateRegister($request);
        User::create([
            'id_card' => $request->id_card,
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,

        ]);
        Mail::to($request->email)->send(new registerMail($request));

        return redirect()->route('login.index')->withSuccess('Create Success');
    }

    public function forgotPw()
    {
        return view('auth.passwords.forgot');
    }

    public function sendPw(Request $request)
    {
        $this->validateResetpw($request);
        $mytime = time();
        $sha1 = sha1($request->email . $mytime);
        PasswordReset::create([
            'email' => $request->email,
            'token' => $sha1,
        ]);
        Mail::to($request->email)->send(new ResetPassword($sha1, $request->email));

        return redirect()->route('login')->withSuccess('ส่งสำเร็จ กรุณาตรวจสอบอีเมล์');
    }

    public function validateRegister($registerData)
    {
        $validatedData = $registerData->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'id_card' => 'required',
        ], [], [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'id_card' => 'Staff id card',
        ]);

        return $validatedData;
    }

    public function validateResetpw($request)
    {
        $this->validate($request,
            $rules = ['email' => 'required|exists:users'],
            $messages = [],
            $attributes = ['email' => 'Email',]
        );
    }

    public function validateLogin($request)
    {
        $rule = [
            'email' => 'required',
            'password' => 'required',
        ];
        $messenger = [];
        $attribute = [
            'email' => 'Email',
            'password' => 'Password',
        ];
        $this->validate($request, $rule, $messenger, $attribute);
    }

    public function reSendPw(Request $request)
    {
        $timeNow = Carbon::now();
        $email = $request->input('email');
        $token = $request->input('token');
        $check = PasswordReset::query()
            ->where('email', $email)
            ->where('token', $token)
            ->first();
        if (empty($check)) {
            return redirect()->route('forgot-password.index')->withError('Not found');
        }
        $temp = $timeNow->diffInHours($check->created_at);

        if ($temp >= 24) {
            return redirect()->route('forgot-password.index')->withError('This link is expire and please send forgot password');
        } else {
            return view('auth.passwords.reset', compact('email', 'token'));
        }
    }

    public function updatePw(Request $request)
    {
        $this->validateNewPassword($request);
        $email = $request->input('email');
        $newpassword = Hash::make($request->input('password'));
        $token = $request->input('token');
        User::where('email', $email)->update(['password' => $newpassword]);
        PasswordReset::where('token', $token)->where('email', $email)->delete([$token, $email]);

        return redirect()->route('login')->withSuccess('Reset Password Success');
    }

    public function validateNewPassword($request)
    {
        $this->validate($request,
            $rules = ['password' => 'required|confirmed|min:6'],
            $messages = [],
            $attributes = ['password' => 'Password',]
        );
    }

}
