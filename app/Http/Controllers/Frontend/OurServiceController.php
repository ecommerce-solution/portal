<?php

namespace App\Http\Controllers\Frontend;

use App\ContactUs;
use App\OurService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OurServiceController extends Controller
{
    public function  index()
    {
        $ourServices = OurService::orderBy('updated_at', 'desc')
                ->where('status','OPEN')
                ->paginate(6);
        $contactUses = ContactUs::get();
        return view('frontend.our-services.index',compact('contactUses','ourServices'));
    }
    public function detail(OurService $ourService)
    {
        $contactUses = ContactUs::get();

        return view('frontend.our-services.detail', compact('ourService', 'contactUses'));
    }
}
