<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Career;
use App\Client;

use App\Contact;
use App\ContactUs;
use App\OurService;
use Illuminate\Http\Request;

use App\Banner;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FrontEndController extends Controller
{

    public function index()
    {
        $aboutUses = About::first();
        $clients = Client::get();
        $contactUses = ContactUs::get();
        $banners = Banner::orderBy('created_at', 'desc')->get();
        $ourServices = OurService::get()->where('status','OPEN');
        return view('frontend.index.index', compact('aboutUses', 'clients', 'banners', 'contactUses','ourServices'));

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['tel']  = str_replace(" ", "", "$request->tel");


        $this->validateStore($data)->validate();

        $data = [
                'name' => $data['name'],
                'email' => $data['email'],
                'tel' => $data['tel'],
                'company' => $data['equest->company'],
                'message' => $data['message'],
                'status' => 'Waiting',
        ];
        Contact::Create($data);

        return redirect()->route('ecommerce.success')->withSuccess('Send Success');

    }

    public function validateStore(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'tel' => 'required|numeric|digits_between:9,10',
            'company' => 'required',
            'message' => 'required',
        ], [], [
            'name' => 'Name',
            'email' => 'Email',
            'tel' => 'Phone',
            'company' => 'Company',
            'message' => 'Message',
        ]);
    }

    public function success()
    {
        $contactUses = ContactUs::get();

        return view('frontend.newpage',compact( 'contactUses'));
    }

}
