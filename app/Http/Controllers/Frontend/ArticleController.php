<?php

namespace App\Http\Controllers\Frontend;

use App\Article;
use App\Category;
use App\ContactUs;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index($slug = null)
    {
        $category = null;
        $categories = $this->getAllCategories();
        $query_articles = Article::where('status', 'published')
            ->where('publish_date', '<=', Carbon::now())
            ->with('category')
            ->orderBy('publish_date', 'desc');

        if ($slug != null) {
            $category = Category::query()->where('slug', $slug)->first();
            if ($category == null) {
                return redirect()->route('ecommerce.article');
            }
        }

        if ($category != null) {
            $query_articles->where('category_id', $category->id);
        }

        $articles = $query_articles->paginate(5);
        $article_ids = $articles->pluck('id')->toArray();

        $query_relate_articles = Article::where('status', 'published')
            ->where('publish_date', '<=', Carbon::now())
            ->orderBy('publish_date', 'desc');

        if ($category != null) {
            $query_relate_articles->where('category_id', $category->id);
        }

        $relateArticles = $query_relate_articles->whereNotIn('id', $article_ids)->limit(4)->get();
        $contactUses = ContactUs::get();

        return view('frontend.articles.index', compact('articles', 'contactUses', 'categories', 'relateArticles'));
    }

    public function content(Article $article)
    {
        $contactUses = ContactUs::get();
        $categories = $this->getAllCategories();

        return view('frontend.articles.content', compact('article', 'contactUses', 'categories'));
    }

    protected function getAllCategories()
    {
        return Category::all();
    }
}
