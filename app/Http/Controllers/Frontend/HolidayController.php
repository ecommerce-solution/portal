<?php

namespace App\Http\Controllers\Frontend;

use App\Holiday;
use App\ContactUs;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
    public function index()
    {
        $holidays = Holiday::get();
        $contactUses = ContactUs::get();

        return view('frontend.holidays.index',compact('contactUses','holidays'));
    }
}
