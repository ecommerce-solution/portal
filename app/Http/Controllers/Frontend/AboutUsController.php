<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    public function index()
    {
        $contactUses = ContactUs::get();
        $aboutUses = About::first();
        return view('frontend.about-us.index', compact('contactUses', 'aboutUses'));
    }
}
