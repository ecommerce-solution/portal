<?php

namespace App\Http\Controllers\Frontend;

use App\Career;
use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    public function index()
    {
        $contactUses = ContactUs::get();
        $careers = Career::where('status', 'Open')->get();

        return view('frontend.careers.index', compact('contactUses', 'careers'));
    }

    public function detail(Career $career)
    {
        $contactUses = ContactUs::get();

        return view('frontend.careers.detail', compact('contactUses', 'career'));
    }
}
