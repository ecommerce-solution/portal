<?php

namespace App\Http\Controllers\Frontend\Merchant;

use App\OrderDetail;
use App\Product;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Stock;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('product_category')
            ->get();
        $categories = ProductCategory::all();
        $users = User::all();
        return view('frontend.merchant.general', compact('products', 'categories', 'users'));
    }

    public function addOrder(Request $request)
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 'ERROR']);
        }

        DB::transaction(function () use ($request) {
            $order = new Order;
            $order->user_id = $request->user_id;
            $order->user_name = $request->user_name;
            $order->total_price = $request->total_price;
            $order->status = "unpaid";
            $order->save();

            for ($i = 0; $i < count($request->product_id); $i++) {
                $orderDetail = new OrderDetail;
                $orderDetail->order_id = $order->id;
                $orderDetail->product_id = $request->product_id[$i];
                $orderDetail->product_name = $request->product_name[$i];
                $orderDetail->price_per_unit = $request->price_per_unit[$i];
                $orderDetail->product_qty = $request->product_qty[$i];
                $orderDetail->total_price = $request->product_total[$i];
                $order->orderDetails()->save($orderDetail);

                $stock = new Stock;
                $stock->order_id = $order->id;
                $stock->product_id = $request->product_id[$i];
                $stock->user_id = $request->user_id;
                $stock->type = 'decrease';
                $stock->quantity = $request->product_qty[$i];
                $stock->total = $request->product_amount[$i] - $request->product_qty[$i];
                $stock->save();

                Product::where('id',
                    $request->product_id[$i])->update(['qty' => $request->product_amount[$i] - $request->product_qty[$i]]);

            };

        });
        return response()->json();
    }

}
