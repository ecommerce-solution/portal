<?php

namespace App\Http\Controllers\Frontend;

use App\ContactUs;
use App\OurBusiness;
use App\Http\Controllers\Controller;

class OurBusinessController extends Controller
{
    public function  index()
    {
        $ourBusinesses = OurBusiness::orderBy('updated_at', 'desc')
                ->where('status', 'published')
                ->paginate(6);
        $contactUses = ContactUs::get();
        return view('frontend.our-businesses.index',compact('contactUses','ourBusinesses'));
    }
    public function detail(OurBusiness $ourBusiness)
    {
        $contactUses = ContactUs::get();

        return view('frontend.our-businesses.detail', compact('ourBusiness', 'contactUses'));
    }
}
