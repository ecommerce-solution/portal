<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\ApplicationForm;
use App\Career;
use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobSkillTraining;
use Illuminate\Support\Facades\Storage;

class ApplicationFormController extends Controller
{
    public function index()
    {
        $contactUses = ContactUs::get();
        $careers = Career::where('status', 'Open')->get();

        return view('frontend.application-form.index', compact('contactUses', 'careers'));
    }

    public function save(Request $request)
    {
        $this->validateCareer($request);
        $contactUses = ContactUs::get();
        $check = Career::where('status', 'Open')
            ->where('id', $request->position)
            ->first();

        if (empty($check)) {
            return redirect()->route('ecommerce.career');
        }

        $attach = Storage::disk('uploads')->put('attach/pdf', $request->file('attach'));
        $input = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'date' => Carbon::createFromFormat('d/m/Y', $request->date),
            'phone' => $request->phone,
            'email' => $request->email,
            'university' => $request->university,
            'degree' => $request->degree,
            'subject' => $request->major,
            'gpa' => $request->gpa,
            'career_id' => $request->position,
            'expected_salary' => $request->salary,
            'status' => 'Waiting',
            'attach' => $attach,
        ];
        $applicationForm = ApplicationForm::Create($input);
        if (count($request->company) > 10) {
            return redirect()->route('ecommerce.career');
        }
        if (empty($request->checkbox)) {
            for ($i = 0; $i < count($request->company); $i++) {
                $work_experience = [
                    "company_name" => $request->company[$i],
                    "job_title" => $request->job_title[$i],
                    "date_start" => Carbon::createFromFormat('d/m/Y', $request->start_date[$i]),
                    "date_end" => Carbon::createFromFormat('d/m/Y', $request->end_date[$i]),
                    "job_description" => $request->responsibilities[$i],
                    "application_form_id" => $applicationForm->id,
                ];
                JobSkillTraining::Create($work_experience);
            }
        }

        return redirect()->route('ecommerce.success',compact('contactUses'))->withSuccess('Send Success');

    }

    public function validateCareer($request)
    {
        $input = [
            'firstname' => 'required',
            'lastname' => 'required',
            'date' => 'required|date_format:"d/m/Y"',
            'phone' => 'required|numeric|digits_between:1,10',
            'email' => 'required|email',
            'university' => 'required',
            'degree' => 'required',
            'major' => 'required',
            'gpa' => 'required',
            'position' => 'required',
            'salary' => 'required|numeric|digits_between:0,7|min:1',
            'attach' => 'required|mimes:pdf|max:1024',
        ];

        if (empty($request->checkbox)) {
            $input += [
                'company.*' => 'required',
                'job_title.*' => 'required',
                'start_date.*' => 'required|date_format:"d/m/Y"|before_or_equal:end_date.*',
                'end_date.*' => 'required|date_format:"d/m/Y"|after_or_equal:start_date.*',
                'responsibilities.*' => 'required',
            ];
        }

        $custom = [
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'date' => 'Birthdate',
            'phone' => 'Contact Number',
            'email' => 'Email',
            'university' => 'University',
            'degree' => 'Degree',
            'major' => 'Major/Subject',
            'gpa' => 'GPA',
            'position' => 'Position',
            'salary' => 'Salary',
            'attach' => 'Attach',
            'company.*' => 'Company',
            'job_title.*' => 'Job Title',
            'start_date.*' => 'Date Start',
            'end_date.*' => "Date End",
            'responsibilities.*' => 'Responsibilities',
        ];

        $request->validate($input, [], $custom);
    }
}
