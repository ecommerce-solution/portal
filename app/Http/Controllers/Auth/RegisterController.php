<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = 'admin/login';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $data['id_card'] = str_replace(" ", "", "$request->id_card");
        $data['phone_number'] = str_replace("-", "", "$request->phone_number");
        $data['bank_account_no'] = str_replace("-", "", "$request->bank_account_no");
        $this->validator($data)->validate();
        event(new Registered($user = $this->create($data)));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->withSuccess('สร้าง User สำเร็จ');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'id_card' => 'required|string|min:13|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'nickname' => 'required|string|max:255|unique:users',
            'birth_date' => 'nullable|date_format:d/m/Y',
            'phone_number' => 'nullable|numeric|digits_between:1,10',
            'blood_type' => 'nullable|string|max:255',
            'bank_account_no' => 'nullable|string|max:255',
            'bank_branch' => 'nullable|string|max:255',
            'start_work_date' => 'nullable|date_format:d/m/Y',
        ], [], [
            'email' => 'Email',
            'password' => 'Password',
            'id_card' => 'ID Card',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'nickname' => 'Nickname',
            'birth_date' => 'Birth Date',
            'phone_number' => 'Phone Number',
            'blood_type' => 'Blood Type',
            'bank_account_no' => 'Bank Account',
            'bank_branch' => 'Bank Branch',
            'start_work_date' => 'Start Work',
        ]);
    }

    public function create(array $data)
    {
        $create = [
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_card' => $data['id_card'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'nickname' => $data['nickname'],
            'phone_number' => $data['phone_number'],
            'bank_account_no' => $data['bank_account_no'],
            'bank_branch' => $data['bank_branch'],
            'role_id' => '3',
        ];

        if (!empty($data['birth_date'])) {
            $birth_date = Carbon::createFromFormat('d/m/Y', $data['birth_date']);
            $create += ['birth_date' => $birth_date];
        }
        if (!empty($data['start_work_date'])) {
            $start_work_date = Carbon::createFromFormat('d/m/Y', $data['start_work_date']);
            $create += ['start_work_date' => $start_work_date];
        }
        if (empty($data['blood_type'])) {
            $create += ['blood_type' => null];
        }else {
            $create += ['blood_type' => $data['blood_type']];
        }

        return User::create($create);
    }
}
