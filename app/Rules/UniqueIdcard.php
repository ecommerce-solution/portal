<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;

class UniqueIdcard implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user_id = $this->user;
        $id_card = str_replace(" ", "", $value);
        $query = User::query()
            ->where('id','!=',$user_id)
            ->where('id_card', $id_card )->first();

        if ($query){
            return false;
        }
        else
        {
            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The id card has already been taken.';
    }
}
