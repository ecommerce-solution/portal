<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MinidCard implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_card = str_replace(" ", "", $value);

        if (strlen($id_card ) < 13){
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The id card must be at least 13 characters.';
    }
}
