<?php

Route::group([
    'as' => 'ecommerce.',
    'namespace' => 'Frontend'
], function () {
    Route::get('/', 'FrontEndController@index')->name('list');
    Route::post('store', 'FrontEndController@store')->name('store');
    Route::get('success', 'FrontEndController@success')->name('success');
    Route::get('application-form', 'ApplicationFormController@index')->name('career-form');
    Route::post('save', 'ApplicationFormController@save')->name('save');
    Route::get('about-us', 'AboutUsController@index')->name('about-us');

    Route::group([
        'prefix' => 'article',
    ], function () {
        Route::get('{slug?}/', 'ArticleController@index')->name('article');
        Route::get('{article}/content', 'ArticleController@content')->name('content');
    });

    Route::group([
        'prefix' => 'holiday',
    ], function () {
        Route::get('/', 'HolidayController@index')->name('holiday');
    });

    Route::group([
        'prefix' => 'career',
        'as' => 'career.',
    ], function () {
        Route::get('/', 'CareerController@index')->name('index');
        Route::get('{career}/detail', 'CareerController@detail')->name('detail');
    });

    Route::group([
        'prefix' => 'our-business',
        'as' => 'our-business.',
    ], function () {
        Route::get('/', 'OurBusinessController@index')->name('index');
        Route::get('{ourBusiness}/detail', 'OurBusinessController@detail')->name('detail');
    });

    Route::group([
        'prefix' => 'our-service',
        'as' => 'our-service.',
    ], function () {
        Route::get('/', 'OurServiceController@index')->name('index');
        Route::get('{ourService}/detail', 'OurServiceController@detail')->name('detail');
    });
});


Route::group([
    'prefix' => 'shop',
    'as' => 'merchant.',
    'namespace' => 'Frontend\Merchant'
], function () {
    Route::get('/','ProductController@index');
    Route::post('/payment','ProductController@addOrder');
});

