<?php

// Authentication Routes...
$this->get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('admin/login', 'Auth\LoginController@login');
$this->get('admin/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('admin/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('admin/register', 'Auth\RegisterController@register');

Route::get('/admin', function () {
    return redirect()->route('login');
});

Route::group([
    'prefix' => 'admin/forgot-password',
    'as' => 'forgot-password.',
    'namespace' => 'Backend',
], function () {
    Route::get('/', 'AuthController@forgotPw')->name('index');
    Route::post('send', 'AuthController@sendPw')->name('send');
});

Route::group([
    'prefix' => 'admin/reset-password',
    'as' => 'reset-password.',
    'namespace' => 'Backend',
], function () {
    Route::get('/', 'AuthController@resendPw')->name('index');
    Route::post('update', 'AuthController@updatePw')->name('update');
});

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth'],
    'namespace' => 'Backend',
], function () {

    Route::group([
        'prefix' => 'users',
        'as' => 'users.',
    ], function () {
        Route::get('/', 'UserController@list')->name('list')->middleware(['admin']);
        Route::get('create', 'UserController@create')->name('create')->middleware(['admin']);
        Route::post('store', 'UserController@store')->name('store')->middleware(['admin']);
        Route::delete('{user}/destroy', 'UserController@destroy')->name('destroy')->middleware(['admin']);
        Route::get('{user}/edit', 'UserController@edit')->name('edit');
        Route::post('{user}/update', 'UserController@update')->name('update');
    });

    Route::group([
        'middleware' => ['admin'],
        'prefix' => 'roles',
        'as' => 'roles.',
    ], function () {
        Route::get('/', 'RoleController@list')->name('list');
        Route::get('create', 'RoleController@create')->name('create');
        Route::post('store', 'RoleController@store')->name('store');
        Route::delete('{role}/destroy', 'RoleController@destroy')->name('destroy');
        Route::get('{role}/edit', 'RoleController@edit')->name('edit');
        Route::post('{role}/update', 'RoleController@update')->name('update');
    });

    Route::group([
        'prefix' => 'homepage'
    ],function (){
        Route::get('/', 'HomeController@index')->name('homepage');
    });


    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'banners',
        'as' => 'banners.',
    ], function () {
        Route::get('/', 'BannerController@list')->name('list');
        Route::get('create', 'BannerController@create')->name('create');
        Route::post('store', 'BannerController@store')->name('store');
        Route::get('{banner}/edit', 'BannerController@edit')->name('edit');
        Route::post('{banner}/update', 'BannerController@update')->name('update');
        Route::delete('{banner}/destroy', 'BannerController@destroy')->name('destroy');

    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'contacts',
        'as' => 'contacts.',
    ], function () {
        Route::get('/', 'ContactFormController@list')->name('list');
        Route::get('{contact}/update', 'ContactFormController@update')->name('update');
        Route::get('{contact}/detail', 'ContactFormController@detail')->name('detail');
        Route::post('{contact}/update', 'ContactFormController@update')->name('update');
        Route::delete('{contact}/destroy', 'ContactFormController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'about-us',
        'as' => 'about-us.',
    ], function () {
        Route::get('/', 'AboutUsController@getAbout')->name('index');
        Route::post('save', 'AboutUsController@postAbout')->name('save');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'categories',
        'as' => 'categories.',
    ], function () {
        Route::get('/', 'CategoryController@list')->name('list');
        Route::get('create', 'CategoryController@create')->name('create');
        Route::post('store', 'CategoryController@store')->name('store');
        Route::get('{category}/edit', 'CategoryController@edit')->name('edit');
        Route::post('{category}/update', 'CategoryController@update')->name('update');
        Route::delete('{category}/destroy', 'CategoryController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'articles',
        'as' => 'articles.',
    ], function () {
        Route::get('/', 'ArticleController@list')->name('list');
        Route::get('create', 'ArticleController@create')->name('create');
        Route::post('store', 'ArticleController@store')->name('store');
        Route::get('{article}/edit', 'ArticleController@edit')->name('edit');
        Route::post('{article}/update', 'ArticleController@update')->name('update');
        Route::delete('{article}/destroy', 'ArticleController@destroy')->name('destroy');

    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'holidays',
        'as' => 'holidays.',
    ], function () {
        Route::get('/', 'HolidayController@list')->name('list');
        Route::get('create', 'HolidayController@create')->name('create');
        Route::post('store', 'HolidayController@store')->name('store');
        Route::post('{holiday}/update', 'HolidayController@update')->name('update');
        Route::get('{holiday}/edit', 'HolidayController@edit')->name('edit');
        Route::delete('{holiday}/destroy', 'HolidayController@destroy')->name('destroy');

    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'careers',
        'as' => 'careers.',
    ], function () {
        Route::get('/', 'CareerController@list')->name('list');
        Route::get('create', 'CareerController@create')->name('create');
        Route::post('store', 'CareerController@store')->name('store');
        Route::get('{career}/edit', 'CareerController@edit')->name('edit');
        Route::post('{career}/update', 'CareerController@update')->name('update');
        Route::delete('{career}/destroy', 'CareerController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'application-form',
        'as' => 'application-form.',
    ], function () {
        Route::get('/', 'ApplicationFormController@list')->name('list');
        Route::post('{applicationForm}/update', 'ApplicationFormController@update')->name('update');
        Route::delete('{applicationForm}/destroy', 'ApplicationFormController@destroy')->name('destroy');
        Route::get('{applicationForm}/detail', 'ApplicationFormController@detail')->name('detail');
        Route::get('export', 'ApplicationFormController@export')->name('export');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'contact-us',
        'as' => 'contact-us.',
    ], function () {
        Route::get('/', 'ContactUsController@create')->name('create');
        Route::post('save', 'ContactUsController@postContact')->name('save');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'job-skill-training',
        'as' => 'job-skill-training.',
    ], function () {
        Route::get('/', 'JobskillandTrainingController@list')->name('list');
    });

    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'clients',
        'as' => 'clients.',
    ], function () {
        Route::get('/', 'ClientController@index')->name('list');
        Route::view('create', 'backend.clients.create')->name('create');
        Route::post('store', 'ClientController@store')->name('store');
        Route::get('{client}/edit', 'ClientController@edit')->name('edit');
        Route::post('{client}/update', 'ClientController@update')->name('update');
        Route::delete('{client}/destroy', 'ClientController@destroy')->name('destroy');
    });
    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'our-businesses',
        'as' => 'our-businesses.',
    ], function () {
        Route::get('/', 'OurBusinessController@index')->name('list');
        Route::get('create', 'OurBusinessController@create')->name('create');
        Route::post('store', 'OurBusinessController@store')->name('store');
        Route::get('{ourBusiness}/edit', 'OurBusinessController@edit')->name('edit');
        Route::post('{ourBusiness}/update', 'OurBusinessController@update')->name('update');
        Route::delete('{ourBusiness}/destroy', 'OurBusinessController@destroy')->name('destroy');
    });
    Route::group([
        'middleware' => ['writer'],
        'prefix' => 'our-services',
        'as' => 'our-services.',
    ], function () {
        Route::get('/', 'OurServiceController@index')->name('list');
        Route::get('create', 'OurServiceController@create')->name('create');
        Route::post('store', 'OurServiceController@store')->name('store');
        Route::get('{ourService}/edit', 'OurServiceController@edit')->name('edit');
        Route::post('{ourService}/update', 'OurServiceController@update')->name('update');
        Route::delete('{ourService}/destroy', 'OurServiceController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['merchant'],
        'prefix' => 'products',
        'as' => 'products.',
        'namespace' => 'Merchant',
    ], function () {
        Route::get('/', 'ProductController@list')->name('list');
        Route::get('create', 'ProductController@create')->name('create');
        Route::post('store', 'ProductController@store')->name('store');
        Route::get('{product}/edit', 'ProductController@edit')->name('edit');
        Route::post('{product}/update', 'ProductController@update')->name('update');
        Route::delete('{product}/destroy', 'ProductController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['merchant'],
        'prefix' => 'product-categories',
        'as' => 'product-categories.',
        'namespace' => 'Merchant',
    ], function () {
        Route::get('/', 'ProductCategoryController@list')->name('list');
        Route::get('create', 'ProductCategoryController@create')->name('create');
        Route::post('store', 'ProductCategoryController@store')->name('store');
        Route::get('{productCategory}/edit', 'ProductCategoryController@edit')->name('edit');
        Route::post('{productCategory}/update', 'ProductCategoryController@update')->name('update');
        Route::delete('{productCategory}/destroy', 'ProductCategoryController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['merchant'],
        'prefix' => 'orders',
        'as' => 'orders.',
        'namespace' => 'Merchant',
    ], function () {
        Route::get('/', 'OrderController@list')->name('list');
        Route::get('{order}/detail', 'OrderController@detail')->name('detail');
        Route::post('{order}/update', 'OrderController@update')->name('update');
        Route::delete('{order}/destroy', 'OrderController@destroy')->name('destroy');
    });

    Route::group([
        'middleware' => ['merchant'],
        'prefix' => 'reports',
        'as' => 'reports.',
        'namespace' => 'Merchant',
    ],function (){
        Route::get('/','ReportController@selectProduct')->name('selectProduct');
        Route::get('summary','ReportController@list')->name('summary');
        Route::get('printSummary','ReportController@printSummary')->name('printSummary');
        Route::get('selectOrder','ReportController@selectOrder')->name('selectOrder');
        Route::get('sendProduct','ReportController@sendProduct')->name('sendProduct');
        Route::get('productSelected','ReportController@productSelected')->name('productSelected');
        Route::get('export-summary','ReportController@exportSummary')->name('exportSummary');
        Route::get('export-order','ReportController@exportOrder')->name('exportOrder');
        Route::get('selectUser','ReportController@selectUser')->name('selectUser');
        Route::get('userSelected','ReportController@userSelected')->name('userSelected');
        Route::get('stock','ReportController@stock')->name('stock');
        Route::get('userReport','UserReportController@list')->name('user');
        Route::get('productReport','ReportController@productReport')->name('product');
        Route::get('orderReport','ReportController@orderReport')->name('order');
    });
    Route::group([
            'middleware' => ['auth'],
            'prefix' => 'purchase-histories',
            'as' => 'purchase-histories.',
            'namespace' => 'Merchant',
    ],function (){
        Route::get('/','PurchaseHistoryController@list')->name('list');
        Route::get('{order}/detail','PurchaseHistoryController@detail')->name('detail');
    });

//    Route::group([
//        'prefix' => 'balances',
//        'as' => 'balances.',
//        'namespace' => 'Merchant',
//    ],function (){
//        Route::get('/','BalanceController@list')->name('list');
//        Route::get('{user_id}/detail','BalanceController@detail')->name('detail');
//        Route::post('/update', 'BalanceController@update')->name('update');
//    });

});
